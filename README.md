# scorpion-core

> ScorpionCore.js

[![NPM](https://img.shields.io/npm/v/scorpion-core.svg)](https://www.npmjs.com/package/scorpion-core) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save scorpion-core
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'scorpion-core'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [PatrickCavalcanti](https://github.com/PatrickCavalcanti)
