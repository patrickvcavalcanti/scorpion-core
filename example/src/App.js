import React, { Component } from 'react'

import { Provider } from 'react-redux';
import { createMuiTheme } from '@material-ui/core/styles';

import { MuiThemeProvider } from '@material-ui/core/styles';

import store from './store'
import Testes from "./Testes";
import TesteComp from "./TesteComp";
import DinamicForms from "./DinamicForms";
import OkReport from "./OkReport";

const themeMaterial = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      main: '#333333',
    },
    secondary: {
      main: '#2e7d32',
    },
  },

});
export default class App extends Component {

  render() {

    return (  
      <MuiThemeProvider theme={themeMaterial}>
        <Provider store={store}>
          {/* <Testes />  */}
          {/* <TesteComp /> */}
          {/* <DinamicForms /> */}
          <OkReport />
        </Provider>
      </MuiThemeProvider>
    )
  }
}
