import { compose } from 'redux'
import { connect } from 'react-redux'

import { reduxForm } from 'redux-form';

const mapStateToProps = (state, props) => {
    let initialValues = {
        componenteRoot: {
            uid: Math.random().toString(36).substring(7),
            label: "Arvore de Componentes",
            componentes: []
        }
    }
    

    return { initialValues };
};


export default compose(

    connect(mapStateToProps, {}),
    reduxForm({
        form: 'TesteVai',
    }),
);