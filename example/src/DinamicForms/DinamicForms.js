import React, { Component } from 'react'
import { FormDeveloper } from 'scorpion-core'
import { Button } from '@material-ui/core';
export default class DinamicForms extends Component {
    salvar(props) {
        console.log('salvar props', props);
        

    }
    render() {
        let metasAdd = [
            <div key={"meta1"}>meta 1</div>,
            <div key={"meta2"}>meta 2</div>,
        ]
        const { handleSubmit } = this.props
        return (
            <div>
                <Button onClick={handleSubmit(this.salvar)}>Salvar</Button>
                <FormDeveloper {...this.props} metasAdd={metasAdd} />
            </div>
        )
    }
}
