import DinamicForms from './DinamicForms'
import enhance from './DinamicForms.enhancer'

export default enhance(DinamicForms)
