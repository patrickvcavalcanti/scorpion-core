import { compose } from 'redux'
import { connect } from 'react-redux'

import { reduxForm } from 'redux-form';

const mapStateToProps = (state, props) => {
    let initialValues = {
        code_conteudo: `function relatorio(dados,meta) {
            console.log("dados", dados)
            console.log("meta", meta)
       
            let l = dados;
            let conteudo1 = [];
        
            conteudo1.push(painel(      [
                [
                    { style: "cellTitulo", text: "UID" }, 
                    { style: "cellValor", text: l.uid }
                ],                
            ], { widths: [70, '*'] }))
           
       
            return conteudo1
        
        }
       
        function gerarRelatorio(dados,meta,imports) {
            const { painel,fieldset } = imports
            console.log("dados", dados)
            console.log("meta", meta)
       
            let l = dados;
            let conteudo1 = [];
        
            conteudo1.push(painel(      [
                [
                    { style: "cellTitulo", text: "UID" }, 
                    { style: "cellValor", text: l.uid }
                ],                
            ], { widths: [70, '*'] }))

            if(meta && meta.chats && meta.chats.GaugeChart){
                conteudo1.push({image: meta.chats.GaugeChart})
            }

            if(meta && meta.chats && meta.chats.LineChart){
                conteudo1.push({image: meta.chats.LineChart})
            }
            if(meta && meta.chats && meta.chats.BarChart){
                conteudo1.push({image: meta.chats.BarChart})
            }
            if(meta && meta.chats && meta.chats.PieChart){
                conteudo1.push({image: meta.chats.PieChart})
            }
       
            return conteudo1
        
        }
        function getGraficos(dados,meta) {
            // console.log("dados", dados)
            // console.log("meta", meta)
          
       
            
        let graficos = [
            {name: "LineChart",
                type: "LineChart", props: {},
                columns: ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                series: [
                    { name: "teste1", values: [4000, 3000, 2000, 2780, 1890, 2390, 3490], props: { type: "monotone", stroke: "#8884d8" } },
                    { name: "teste2", values: [2400, 1398, 9800, 3908, 4800, 3800, 4300], props: { type: "monotone", stroke: "#82ca9d" } },
                ]
            },
            {  name: "BarChart",
                type: "BarChart", props: {},
                columns: ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                series: [
                    { name: "teste1", values: [4000, 3000, 2000, 2780, 1890, 2390, 3490], props: { type: "monotone", fill: "#8884d8" } },
                    { name: "teste2", values: [2400, 1398, 9800, 3908, 4800, 3800, 4300], props: { type: "monotone", fill: "#82ca9d" } },
                ]
            },
            {
                type: "PieChart",
                name: "PieChart",
                props: { cx: 200, cy: 200, outerRadius: 80, fill: "#8884d8", label: function(item) {
                    return item.name +" : "+ item.value ;
                } },
                values: [
                    { name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
                    { name: 'Group C', value: 300 }, { name: 'Group D', value: 200 },
                    { name: 'Group E', value: 278 }, { name: 'Group F', value: 189 },
                ]
            },

            {
                type: "GaugeChart",
                name: "GaugeChart",
                values: [
                    { name: 'Group A', value: 90 },
                    { name: 'Group B', value: 2},
                ]
            },
            
            ];
            return graficos
        
        }`
    }

    return { initialValues };
};


export default compose(

    connect(mapStateToProps, {}),
    reduxForm({ form: 'OkReportForm', }),
);