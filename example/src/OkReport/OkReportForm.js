import React, { Component } from 'react'
import { RelatorioDeveloperForm } from 'scorpion-core'
export default class OkReportForm extends Component {

    render() {
        let jsonGerar = { dados: { uid: "uidTeste", nome: "nome", descricao: "descricao" } }
        let metasAdd = [
            <div key={"meta1"}>meta 1</div>,
            <div key={"meta2"}>meta 2</div>,
        ]

        let metaRelatorio = {
            textoCabecalho1: "OK Desenvolvimento de Software",
            textoCabecalho2: "Dourados-MS - Fone.: (67) 3038-0338",
            textoRodape1: "rodape ",
            textoRodape2: "Emitido po Scorpion Core",
        }
        return (<div style={{height: 800 }}><RelatorioDeveloperForm {...this.props} jsonGerar={jsonGerar} metasAdd={metasAdd} metaRelatorio={metaRelatorio} /></div>)
    }
}
