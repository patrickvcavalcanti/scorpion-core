import OkReportForm from './OkReportForm'
import enhance from './OkReportForm.enhancer'

export default enhance(OkReportForm)
