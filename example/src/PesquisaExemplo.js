import React, { Component } from 'react'

import { reduxForm } from 'redux-form';
import { PesquisaScorpion, FiltroStringScorpion, FiltroCompoboxScorpion } from 'scorpion-core'
class PesquisaExemplo extends Component {

    colunas = () => {
        const styleCondicional = (item) => {
            if (item.ativo !== undefined && !item.ativo) {
                return { color: "#F00" }
            }


            return { color: "#000" }
        };

        let colunas = [
            { label: "Nome", getValue: item => item.nome, props2label: { style: { width: 100 } }, style2valueCondicao: styleCondicional },
            { label: "Safra", getValue: item => item.safra, style2valueCondicao: styleCondicional },
            { label: "Cultura", getValue: item => item.cultura, style2valueCondicao: styleCondicional },

        ];
        return colunas;
    }
   

    render() {
        let itens = [
            { nome: "nome1", safra: "safra1", cultura: "cultura1", ativo: true },
            { nome: "nome2", safra: "safra2", cultura: "cultura2", ativo: false },
            { nome: "nome3", safra: "safra3", cultura: "cultura4", ativo: false },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
            { nome: "nome4", safra: "safra4", cultura: "cultura5", ativo: true },
       


        ]


        return (
            <PesquisaScorpion
                titulo={"Pesquisa de Exemplo"}
                columns={this.colunas()}
                data={itens}
                getID={(item, index) => item.nome1 ? item.nome1 : index}
                porPagina={10}
                loadingPesquisa={false}
                selecionar={(itemSelecionado) => console.log('itemSelecionado', itemSelecionado)}
                multiSelecao={true}
                selecionados={(selecionados) => console.log("Itens selecionados ", selecionados)}
                {...this.props}>
                <FiltroStringScorpion name="filtroNome" label="Nome"
                    strComparaLike={item => item.nome}
                    sugestoes={itens.map(item => item.nome)} />



                <FiltroCompoboxScorpion name="filtroativo" label="ativo" itens={["Sim", "Não"]} placeholder="Exibir somente ativo"
                    atende={(obj, filtro) => {
                        if (filtro === "Sim") {
                            if (obj.ativo) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return true;
                        }
                    }}
                />

            </PesquisaScorpion>
        )
    }
}


PesquisaExemplo = reduxForm({ form: 'PesquisaExemplo', })(PesquisaExemplo);
export default PesquisaExemplo


