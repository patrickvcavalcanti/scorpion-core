import React, { Component } from 'react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'

import { Item, ArvoreSelecionavelObj } from 'scorpion-core'

class Testes extends Component {
    render() {
        const colunas = [
            { label: 'Nome', getValue: (item) => item.nome, align: 'left', width: '40%' },
            { label: 'Descricao', getValue: (item) => item.descricao, align: 'right', width: '20%' },

        ];

        let lista = [{ nome: "nom", descricao: "d" }, { nome: "nom2", descricao: "d2" }];
        return (
            <div>

                <Item >
                    <Field name="permissoesGeral" label="Permissões Gerais" component={ArvoreSelecionavelObj} opcoes={["Sistema.executar", "outro.teste", "Sistema.imprime"]} />
                </Item>

            </div>
        )
    }
}
const mapStateToProps = (state, props) => {
    return {};
};

export default compose(

    reduxForm({
        form: 'TestForm',
    }),
    connect(mapStateToProps, {}),

)(Testes)