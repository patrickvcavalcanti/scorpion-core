import React, { Component } from 'react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Button } from '@material-ui/core'

import { Input, Item, InputAuto, InputDate, InputDecimal, InputMulti, InputPicker, SwitchItem, Spinner } from 'scorpion-core'
import { ArvoreSelecionavel, TabelaScorpion, DialogPesquisa} from 'scorpion-core'
import PesquisaExemplo from './PesquisaExemplo';



class Testes extends Component {
    render() {
        const colunas = [
            { label: 'Nome', getValue: (item) => item.nome, align: 'left', width: '40%' },
            { label: 'Descricao', getValue: (item) => item.descricao, align: 'right', width: '20%' },

        ];

        let lista = [
            { nome: "nom", descricao: "d" }, 
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },
            { nome: "nom2", descricao: "d2" },

        ];
        return (
            <div>
                {/* < Item>
                    <Field name="texto" placeholder={"Texto"} component={Input} />
                </Item>
                < Item>
                    <Field name="data" placeholder={"Data"} component={InputDate} />
                </Item>
                < Item>
                    <Field name="decimal" placeholder={"Decimal"} component={InputDecimal} />
                </Item>
                < Item>
                    <Field name="muiltTesto" placeholder={"Texto Longo"} component={InputMulti} />
                </Item>
                < Item>
                    <Field name="InputPicker" placeholder={"Compobox"} itens={["patrick", "joao", "patricia"]} toStringItem={(item) => item} component={InputPicker} />
                </Item>
                < Item>
                    <Field name="nomeProdutor" placeholder="Nome Produtor" data={["patrick", "joao", "patricia"]} component={InputAuto} />
                </Item>
                < Item>
                    <Field name="SwitchItem" placeholder="Ativo" component={SwitchItem} />
                </Item>
                < Item style={{ backgroundColor: "#f00" }}>
                    <Spinner />
                </Item>
                <Item >
                    <Field name="permissoesGeral" label="Permissões Gerais" component={ArvoreSelecionavel} opcoes={["Sistema.executar", "outro.teste", "Sistema.imprime"]} />
                </Item>*/}
                <Item style={{ backgroundColor: "#ff0" }}>
                    <TabelaScorpion style={{ backgroundColor: "#0f0", }} paginacao={5} colunas={colunas} data={lista} mensagemVazio="Nenhum talhão adicionado." loading={true}></TabelaScorpion>
                </Item>
            
                {/* <Item >
                    <Button onClick={() => {
                        console.log(' this.dialog', this.dialog);

                        this.dialog.show()
                    }}>Pesquisa</Button>
                </Item> */}
                <DialogPesquisa forwardedRef={node => this.dialog = node} mensagemSair={"Deseja cancelar"} >    <PesquisaExemplo /></DialogPesquisa>
            </div>
        )
    }
}
const mapStateToProps = (state, props) => {
    return {};
};

export default compose(

    reduxForm({
        form: 'TestForm',
    }),
    connect(mapStateToProps, {}),

)(Testes)