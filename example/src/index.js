import React from 'react'
import ReactDOM from 'react-dom'

import './index.css'
import App from './App'
import { gerarDocumento as pd2 } from './template/TemplatePadrao2'



import { OkReportGerador } from 'scorpion-core';
OkReportGerador.addTemplate({ nome: "pd2", gerar: pd2 })
ReactDOM.render(<App />, document.getElementById('root'))
