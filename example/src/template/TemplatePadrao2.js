

import logo64 from './logo';

const stylesTemplate = {
    template: {
        relatorioTitulo: {
            fontSize: 20,
            bold: true,
        },
        cabecalhoTexto1: {
            fontSize: 18,
            bold: true,
        },
        cabecalhoTexto2: {
            fontSize: 12,
            bold: false,

        },
        rodapeTexto: {
            fontSize: 10,
        }
    },
    cellTitulo: {
        margin: [1, 1, 1, 1],
        fontSize: 9,
        bold: true,
        fillColor: '#dcdcdc',
    },
    cellValor: {
        margin: [1, 1, 1, 1],
        fontSize: 9,
        fillColor: '#f0f0f0',
    },
    cellLimpo: {
        margin: [1, 1, 1, 1],
        fontSize: 9,
    },
    fieldsetTitulo: {
        margin: [2, 2, 2, 2],
        fontSize: 11,
        bold: true,
        fillColor: '#285137',
        color: 'white'
    }
}

export function gerarDocumento(relatorioCustom, conteudo, meta) {
    console.log('gerarDocumento',gerarDocumento);
    
    let tituloRelatorio = "Titulo Relatorio"
    let textoCabecalho1 = ""
    let textoCabecalho2 = ""
    let textoRodape1 = ""
    let textoRodape2 = ""

    let stylesUsar = stylesTemplate;

    const { styles, nome } = relatorioCustom
    if (styles) {
        stylesUsar = { ...stylesUsar, ...styles }
    }
    if (nome)
        tituloRelatorio = nome;
        tituloRelatorio = tituloRelatorio+"pd2";
    let imageLogo = logo64;
    if (meta) {
        if (meta.logo) {
            imageLogo = `data:image/png;base64,${meta.logo}`;
        }
        textoCabecalho1 = meta.textoCabecalho1
        textoCabecalho2 = meta.textoCabecalho2
        textoRodape1 = meta.textoRodape1 || meta.infoVersao
        textoRodape2 = meta.textoRodape2
    }

    const documentImprimir = templatePadrao(conteudo,
        {
            icone: imageLogo,
            tituloRelatorio,
            textoCabecalho1,
            textoCabecalho2,
            textoRodape1,
            textoRodape2,

        }, stylesUsar
    );
    return documentImprimir
}


function cabecalhoPadrao(templateDados, currentPage, pageCount, pageSize, style) {
    return [{
        margin: [25, 10, 25, 0],
        table: {
            widths: [100, '*'],
            body: [
                [
                    {
                        border: [false, false, false, true],
                        alignment: 'center',
                        image: templateDados.icone,
                        width: 90,
                        height: 90,
                    },
                    {
                        border: [false, false, false, true],
                        table: {
                            body: [
                                [{
                                    text: templateDados.tituloRelatorio,
                                    style: style.template.relatorioTitulo,
                                }],
                                [{
                                    text: templateDados.textoCabecalho1,
                                    style: style.template.cabecalhoTexto1
                                }],
                                [{
                                    text: templateDados.textoCabecalho2,
                                    style: style.template.cabecalhoTexto2
                                }],
                            ]
                        }, layout: 'noBorders'
                    }
                ]
            ]
        }
    }]
}

export function templatePadrao(conteudo, templateDados, styles) {
    var top = 120;
    const documentDefinition = {
        pageMargins: [25, top, 25, 55],
        pageSize: 'A4',

        header: function (currentPage, pageCount, pageSize) {
            return cabecalhoPadrao(templateDados, currentPage, pageCount, pageSize, styles)
        },

        footer: function (currentPage, pageCount) {
            return [{
                margin: [25, 10, 25, 0],
                table: {
                    widths: ['*', "auto", 60],
                    body: [
                        [
                            {
                                border: [false, true, false, false],
                                text: templateDados.textoRodape1,
                                style: styles.template.rodapeTexto
                            },
                            {
                                border: [false, true, false, false],
                                text: templateDados.textoRodape2,
                                style: styles.template.rodapeTexto
                            },
                            {
                                border: [false, true, false, false],
                                alignment: 'right',
                                text: 'Pág.: ' + currentPage.toString() + '/' + pageCount,
                                style: styles.template.rodapeTexto
                            }

                        ]
                    ],

                }
            }]
        },
        content: conteudo,
        styles: styles,

    };

    return documentDefinition;
}