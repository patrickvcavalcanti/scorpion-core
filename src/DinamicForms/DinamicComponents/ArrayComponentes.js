import React, { Component } from 'react'

import InputComp from './components/InputComp'
import InputCompMask from './components/InputCompMask'
import PainelHorizontalComp from './components/PainelHorizontalComp'
import ComboBoxComp from './components/ComboBoxComp'
import InputAutocompletComp from './components/InputAutocompletComp'
import InputCompMultLines from './components/InputCompMultLines'
import InputDateComp from './components/InputDateComp'
import InputDateInterval from './components/InputDateInterval'
import InputDateTimeComp from './components/InputDateTimeComp'
import InputDecimalComp from './components/InputDecimalComp'
import LabelComp from './components/LabelComp'
import MultiSelect from './components/MultiSelect'
import PainelMinimizar from './components/PainelMinimizar'
import SwitchComp from './components/SwitchComp'
import TabelaItens from './components/TabelaItens'
import SequencialIndexComp from './components/SequencialIndexComp'
import ListaString from './components/ListaString'
import CompCodigo from './components/CompCodigo'
import PainelAbasComp from './components/PainelAbas'
import ImagemCortar from './components/ImagemCortar'
import MapaArea from './components/CompMapArea';
import PainelFotos from './components/PainelFotos'
import CompImagem from './components/CompExibeFoto';


export const componentesConhecidos = [
    PainelHorizontalComp,
    InputComp,
    ComboBoxComp,
    InputAutocompletComp,
    InputCompMultLines,
    InputDateComp,
    InputDateTimeComp,
    InputCompMask,
    InputDateInterval,
    InputDecimalComp,
    LabelComp,
    MultiSelect,
    PainelMinimizar,
    SwitchComp,
    TabelaItens,
    SequencialIndexComp,
    ListaString,
    CompCodigo,
    PainelAbasComp,
    ImagemCortar,
    MapaArea,
    PainelFotos,
    CompImagem,

]
export function addComponente(componente) {
    componentesConhecidos.push(componente)
}

export function getComponenteView(item, propsview) {
    for (let index = 0; index < componentesConhecidos.length; index++) {
        const element = componentesConhecidos[index];
        if (element.typeComponente === item.typeComponente) {
            return element.component(item, propsview)
        }

    }
    return <div>Componente Desconhecido: {item.typeComponente} , Field: {item.field}</div>

}

export function getEditores(typeComponente) {

    for (let index = 0; index < componentesConhecidos.length; index++) {
        const element = componentesConhecidos[index];
        if (element.typeComponente === typeComponente) {
            return element.configView.editores;
        }

    }

}

export function getModelo(typeComponente) {

    for (let index = 0; index < componentesConhecidos.length; index++) {
        const element = componentesConhecidos[index];
        if (element.typeComponente === typeComponente) {
            return element.configView.modelo;
        }

    }

}

export function getCompRelEspelho(typeComponente) {

    for (let index = 0; index < componentesConhecidos.length; index++) {
        const element = componentesConhecidos[index];
        if (element.typeComponente === typeComponente) {
            return element.componentRelEspelho;
        }

    }

}

export function getDinamicComponente(typeComponente) {
    for (let index = 0; index < componentesConhecidos.length; index++) {
        const element = componentesConhecidos[index];
        if (element.typeComponente === typeComponente) {
            return element;
        }

    }
}


export default componentesConhecidos