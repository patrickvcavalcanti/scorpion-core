import React from 'react'
import { Field } from 'redux-form';
import { Item, InputPicker } from '../../../components';
import { required } from '../function';
import { painel } from '../../../OkReport';

import EditorLista from '../editors/EditorLista';


function componentRelEspelho(comp, dados) {
    
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })

}

function component(item) {
    let disabled = item.desativado;
    if (item.requerido) {
        return (<Item key={item.sha} style={{ flex: item.flex || 1 }}>
                <Field name={item.field} placeholder={item.label} disabled={disabled} itens={item.opcoes} toStringItem={(item) => item} component={InputPicker} validade={[required]} {...item.propsadd}/>
        </Item>)
    }

    return (<Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} disabled={disabled} itens={item.opcoes} toStringItem={(item) => item} component={InputPicker} {...item.propsadd}/>
    </Item>)
}

const modelo = {
    field: "atividade",
    label: "Atividade Preferida",
    typeComponente: "ComboBox",
    opcoes: ["Atividade Física", "Estudos", "Trabalho"],
    colunaPesquisa: false,
    requerido: false,
    desativado: false
}

const comp = {
    typeComponente: "ComboBox",
    configView: {
        nome: "ComboBox",
        modelo,
        editores: {
            opcoes: EditorLista
        },
    },
    propsField: [
        { field: "field", label: "Field", typeComponente: "Input", },
        { field: "label", label: "Label", typeComponente: "Input", },
        { field: "opcoes", label: "Opções", typeComponente: "ListaString", },
        { field: "requerido", label: "Requerido", typeComponente: "SwitchItem", },
    ],
    componentRelEspelho,
    component
}

export default comp;