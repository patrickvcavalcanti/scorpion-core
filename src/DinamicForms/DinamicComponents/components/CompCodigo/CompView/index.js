import React, { Component } from 'react'

import View from 'react-flexbox'
import {DialogPesquisa, EditorCodeField, Input } from '../../../../../components';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import MoreHoriz from '@material-ui/icons/MoreHoriz';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';

import produce from "immer"


export default class CompView extends Component {
    constructor(props) {
        super(props);

    }


    render() {

        const { placeholder, input: { value, onChange } } = this.props

        return (
            <View style={{ height: 250 }}>
                <EditorCodeField
                    label={placeholder}
                    input={{ value, onChange }}
                    meta={{ touched: "", error: "" }}

                    fullWidth
                />
                <Button variant="contained" color="primary" onClick={() => this.dialog.show()}>
                    <MoreHoriz />
                </Button>
                <DialogPesquisa forwardedRef={node => this.dialog = node}>

                    <EditorCodeField
                        input={{ value, onChange }}
                        meta={{ touched: "", error: "" }}
                        fullWidth
                    />

                </DialogPesquisa>

            </View>
        )
    }
}
