import React from 'react'
import { Field } from 'redux-form';
import { Item } from '../../../../components';
import CompView from './CompView'
function componentRelEspelho(comp, dados) {
    return ""

}

function component(item, ptops) {
    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} item={item} component={CompView}  {...item.propsadd} />
    </Item>
}

const modelo = {
    typeComponente: "CompCodigo",
    field: "codigo",
    label: "Comp Codigo",

}

const comp = {
    typeComponente: "CompCodigo",
    configView: {
        nome: "CompCodigo",
        modelo
    },
    componentRelEspelho,
    component
}

export default comp;