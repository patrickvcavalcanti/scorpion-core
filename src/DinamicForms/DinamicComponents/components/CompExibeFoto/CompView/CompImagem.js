import React, { Component } from "react";
import View from 'react-flexbox';

class CompImagem extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount(){
    }

    render() {
        const {
            input: { value, onChange },
            item
        } = this.props;
        let foto = item.imagem ? item.imagem.foto : null;
        // console.log('Foto ', foto);

        let c = <div style={{
            maxWidth: 200,
            maxHeight: 200,
        }}>

            <img style={{
                maxWidth: 200,
                maxHeight: 200,
                width: 'auto',
                height: 'auto',
            }} src={"data:image/jpeg;base64," + foto} /> 
        </div> 
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1}}>
                {c}
            </View>
        );
    }
}

export default CompImagem;