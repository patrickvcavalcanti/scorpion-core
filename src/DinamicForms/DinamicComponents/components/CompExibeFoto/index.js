import React from 'react'
import { Field } from 'redux-form';
import { Item } from '../../../../components';
import CompImagem from './CompView/CompImagem';
import EditorImagem from '../../editors/EditorImagem'

function componentRelEspelho(comp, dados) {
    return ""

}

function component(item, ptops) {
    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} item={item} component={CompImagem}  {...item.propsadd} />
    </Item>
}
 
const modelo = {
    typeComponente: "CompImagem",
    // field: "imagem",
    label: "Componente de Imagem",
    imagem: ''
    // largura: 480,

}

const comp = {
    typeComponente: "CompImagem",
    configView: {
        nome: "Componente de Imagem",
        modelo,
        editores: {
            imagem: EditorImagem
        }
    },
    componentRelEspelho,
    component
}

export default comp;