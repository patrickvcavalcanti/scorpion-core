import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose, withProps } from 'recompose'
import View from 'react-flexbox'
import { Button } from '@material-ui/core';
import { MyLocation } from '@material-ui/icons';
const {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
    Polygon
} = require("react-google-maps");
// const google = window.google;

const MyMapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAEhmWG2jNKFUcbApO1bg-alG8TPOrDJqQ&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />
    }),
    withScriptjs,
    withGoogleMap
)(props => (

    <GoogleMap
        style={{ flex: "1" }}
        defaultMapTypeId={google.maps.MapTypeId.SATELLITE}
        defaultZoom={16}
        // defaultCenter={{ lat: -22.2277664, lng: -54.7897099 }} 
        defaultCenter={props.centro}
        onClick={e => props.onPress(e)}
        ref={props.innerRef}>
        {console.log("Props, ", props)}
        {props.listCordenada.map((marcar, index2) =>
            <Marker
                icon={{
                    url: 'http://mt.google.com/vt/icon?psize=27&font=fonts/Roboto-Bold.ttf&color=ff135C13&name=icons/spotlight/spotlight-waypoint-a.png&ax=43&ay=50&text=•&scale=1'
                }}
                key={index2}
                position={marcar}
                onDragEnd={(e) => {
                    props.updadeIndex(index2, { lat: e.latLng.lat(), lng: e.latLng.lng() });
                }}

                draggable
            />
        )}
        {props.listCordenada && props.listCordenada.length ?
            <Polygon
                options={{
                    strokeColor: '#006400',
                    fillColor: '#7d899e',
                }}
                path={props.listCordenada}
                strokeColor="#0F0"
                fillColor="rgba(0,255,0,0.5)"
            /> : ""}

        {/* <Button onClick={() => props.centro = }>Refresh</Button> */}
    </GoogleMap>
));

class MapaArea extends Component {
    constructor(props) {
        super(props)

        this.updateValue = this.updateValue.bind(this);
        this.arrumaValue = this.arrumaValue.bind(this);
        this.limpar = this.limpar.bind(this);
        this.desfazer = this.desfazer.bind(this);
        this.updadeIndex = this.updadeIndex.bind(this);
        this.onPress = this.onPress.bind(this);
        this.getAtualLocal = this.getAtualLocal.bind(this);
        this.setAtualLocal = this.setAtualLocal.bind(this);
        this.centralizaMapa = this.centralizaMapa.bind(this);

        this.state = { localAtual: null, areaHa: 0 };
    }

    componentDidMount() {
        this.getAtualLocal();
    }

    updateValue(value) {
        if (value) {
            //de list para string
            let str = "";
            value.forEach(item => str += item.lat + ":" + item.lng + ";")
            this.props.input.onChange(str);
        } else {
            this.props.input.onChange("");
        }
    }
    arrumaValue(value) {
        if (value) {
            //de string para lista
            let lista = [];
            var lacations = value.split(';');
            lacations.forEach(element => {
                if (element) {
                    let split = element.split(':');
                    let lat = split[0];
                    let log = split[1];

                    let cordinate = { lat: parseFloat(lat), lng: parseFloat(log) }
                    lista.push(cordinate);
                }
            });
            return lista;
        }
        return [];
    }

    updadeIndex(index2, coordinate) {
        let newLista = this.arrumaValue(this.props.input.value).slice();
        newLista[index2] = coordinate;

        this.updateValue(newLista);
        this.calcularArea()
    }

    limpar() {
        this.updateValue([]);
        this.calcularArea()
    }
    desfazer() {
        let newLista = this.arrumaValue(this.props.input.value).slice();
        newLista.splice(newLista.length - 1, 1);
        this.updateValue(newLista);
        this.calcularArea()
    }
    onPress(e) {


        let coordinate = { lat: e.latLng.lat(), lng: e.latLng.lng() };
        let newLista = this.arrumaValue(this.props.input.value).slice();

        newLista.push(coordinate);
        this.updateValue(newLista);
        this.calcularArea()
    }

    calcularArea() {
        if (google.maps.geometry) {
            let listCordenada = this.arrumaValue(this.props.input.value);
            var a = [];
            for (var i = 0; i < listCordenada.length; i++) {
                var uno = listCordenada[i];
                a[i] = new google.maps.LatLng(uno.lat, uno.lng);
            }
            let area = google.maps.geometry.spherical.computeArea(a, 6371000);

            let areaF = area / 10000;

            // console.log("Value ", this.props.input.value);
        }
    }

    getCentro() {

        let listCordenada = this.arrumaValue(this.props.input.value);
        if (!listCordenada.length) {
            return null;
        }
        let lowx,
            highx,
            lowy,
            highy,
            lats = [],
            lngs = [];
        for (var i = 0; i < listCordenada.length; i++) {
            lngs.push(listCordenada[i].lng);
            lats.push(listCordenada[i].lat);
        }
        lats.sort();
        lngs.sort();
        lowx = lats[0];
        highx = lats[listCordenada.length - 1];
        lowy = lngs[0];
        highy = lngs[listCordenada.length - 1];
        let center_x = lowx + ((highx - lowx) / 2);
        let center_y = lowy + ((highy - lowy) / 2);
        return { lat: center_x, lng: center_y }

    }
    getAtualLocal() {
        if ("geolocation" in navigator) {

            // check if geolocation is supported/enabled on current browser
            navigator.geolocation.getCurrentPosition(
                this.setAtualLocal,
                function error(error_message) {
                    // for when getting location results in an error
                    console.error('An error has occured while retrieving location', error_message)
                }
            );
        } else {
            // geolocation is not supported
            // get your location some other way
            console.log('geolocation is not enabled on this browser')

        }
    }

    setAtualLocal(position) {
        // console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude);
        let local = { lat: position.coords.latitude, lng: position.coords.longitude }

        this.setState({ localAtual: local })

    }

    centralizaMapa() {
        // this.getAtualLocal();
        // Object.assign(this.refMap.props['defaultCenter'], this.state.localAtual);
        // this.refMap.props.onCenterChanged(this.state.localAtual);
        // console.log("Position ", this.refMap.props.onCenterChanged());
        
        // this.refMap.props.fitBounds(this.state.localAtual);
    }

    render() {
        const {
            input: { value },
            item

        } = this.props;


        let listCordenada = this.arrumaValue(value);

        let centro = this.getCentro();

        if (!centro) {
            if (this.state.localAtual) {
                centro = this.state.localAtual;

            } else {
                centro = { lat: -32.2291487, lng: -64.8020967 };
            }
        }

        if (!this.props.areaHaAtual && value) {
            this.calcularArea();
        }
        return (
            <View style={{ flex: "1", flexDirection: 'column', height: '400px' }}>
                <MyMapComponent innerRef={(node) => this.refMap = node} centro={centro} listCordenada={listCordenada} updadeIndex={this.updadeIndex} onPress={this.onPress} />
                <View style={{
                    flexDirection: 'row',
                    marginVertical: 20,
                    backgroundColor: 'transparent',
                    position: 'absolute',
                    bottom: 30,
                    right: 55,
                }}>
                    {/* <Button style={{
                        backgroundColor: 'rgba(255,255,255,0.7)',
                        paddingHorizontal: 18,
                        paddingVertical: 12,
                        borderRadius: 20,
                        alignItems: 'center',
                        marginHorizontal: 10,
                    }} onClick={() => this.centralizaMapa()} label="Centralizar"><MyLocation /></Button> */}
                    <Button style={{
                        backgroundColor: 'rgba(255,255,255,0.7)',
                        paddingHorizontal: 18,
                        paddingVertical: 12,
                        borderRadius: 20,
                        alignItems: 'center',
                        marginHorizontal: 10,
                    }} onClick={() => this.desfazer()}>Desfazer</Button>
                    <Button style={{
                        backgroundColor: 'rgba(255,255,255,0.7)',
                        paddingHorizontal: 18,
                        paddingVertical: 12,
                        borderRadius: 20,
                        alignItems: 'center',
                        marginHorizontal: 10,
                    }} onClick={() => this.limpar()}>Limpar</Button>

                </View>
            </View>
        )
    }
}

const propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
};

MapaArea.propTypes = propTypes;

export default MapaArea;