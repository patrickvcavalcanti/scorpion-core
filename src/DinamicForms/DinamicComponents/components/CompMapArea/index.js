import React from 'react'
import { Field } from 'redux-form';
import { Item } from '../../../../components';
import MapaArea from './CompView/MapaArea';

function componentRelEspelho(comp, dados) {
    return ""

}

function component(item, ptops) {
    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} item={item} component={MapaArea}  {...item.propsadd} />
    </Item>
}

const modelo = {
    typeComponente: "MapaArea",
    field: "mapa",
    label: "Mapa",

}

const comp = {
    typeComponente: "MapaArea",
    configView: {
        nome: "Componente Mapa",
        modelo
    },
    componentRelEspelho,
    component
}

export default comp;