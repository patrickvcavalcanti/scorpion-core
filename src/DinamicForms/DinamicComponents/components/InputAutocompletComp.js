import React from 'react'
import { Field } from 'redux-form';
import { Item, InputAuto } from '../../../components';
import { required } from '../function';
import { painel } from  '../../../OkReport';

import EditorLista from '../editors/EditorLista';


function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })

}

function component(item) {
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field key={item.field} name={item.field} placeholder={item.label} data={item.opcoes} component={InputAuto} validate={[required]} {...item.propsadd}/>
        </Item>
    }

    return (<Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field key={item.field} name={item.field} placeholder={item.label} data={item.opcoes} component={InputAuto} {...item.propsadd}/>
    </Item>)
}

const modelo = { field: "tipo", label: "Selecione um Tipo", typeComponente: "InputAuto", opcoes: ["Tipo1", "Tipo2", "Tipo3"] }

const comp = {
    typeComponente: "InputAuto",
    configView: {
        nome: "Campo Autocomplete",
        modelo,
        editores: {
            opcoes: EditorLista
        },
    },
    componentRelEspelho,
    component
}

export default comp;