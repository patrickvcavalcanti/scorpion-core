import React from 'react'
import { Field } from 'redux-form';
import { Item, Input } from '../../../components';
import { required } from '../function';
import { painel } from '../../../OkReport';
export function requiredTeste(value) {

    if (value) {
        return undefined
    } else {

        console.log('value', value);
        return 'Requerido'
    }

}


function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados ? dados[comp.field] : "" }]
    ], { widths: ['*'] })

}

function component(item, props) {

    let disabled = item.desativado
    if (props && props.verificaPermissao && item.permissao_editar) {
        disabled = !props.verificaPermissao(item.permissao_editar);
    }
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field name={item.field} placeholder={item.label} type={item.type} component={Input} validate={[requiredTeste]} {...item.propsadd} disabled={disabled} />
        </Item>
    }

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} type={item.type} component={Input}  {...item.propsadd} disabled={disabled} />
    </Item>
}

const modelo = {
    typeComponente: "Input",
    field: "nome",
    label: "Nome",
    type: "text",
    requerido: false,
    colunaPesquisa: false,
    filtroPesquisa: false,
    desativado: false,
}

const InputComp = {
    typeComponente: "Input",
    configView: {
        nome: "Campo Texto",
        modelo
    },
    componentRelEspelho,
    component
}

export default InputComp;