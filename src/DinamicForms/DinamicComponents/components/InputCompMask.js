import React from 'react';
import { Field } from 'redux-form';
import MaskedInput from 'react-text-mask';
import PropTypes from 'prop-types';
import { Item, InputMask } from '../../../components';
// import InputLabel from '@material-ui/core/InputLabel';
// import FormControl from '@material-ui/core/FormControl';
export function requiredTeste(value) {

    if (value) {
        return undefined
    } else {

        // console.log('value', value);
        return 'Requerido'
    }

}

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

function component(item, props) {
    let disabled = item.desativado;
    if (props && props.verificaPermissao && item.permissao_editar) {
        disabled = !props.verificaPermissao(item.permissao_editar);
    }
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field name={item.field} placeholder={item.label} mask={item.mascara} component={InputMask} validate={[requiredTeste]} {...item.propsadd} disabled={disabled} style={{width: '100%'}} />
        </Item>
    }

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} mask={item.mascara} component={InputMask} {...item.propsadd} disabled={disabled} style={{width: '100%'}} />
    </Item>
}

const modelo = {
    field: "nome",
    label: "Telefone",
    observacao: "Para preencher o campo máscara: 9: 0-9 a: A-Z, a-z *: A-Z, a-z, 0-9",
    mascara: "(999) 99999-9999",
    requerido: false,
    colunaPesquisa: false,
    filtroPesquisa: false,
    desativado: false,
}


const InputCompMask = {
    configView: {
        nome: "Campo Texto com Máscara",
        modelo
    },
    // componentRelEspelho,
    component
}

export default InputCompMask;


































// import React from 'react'
// import { Field } from 'redux-form';
// import { Item, InputMask } from '../../../components';
// import { required } from '../function';
// import { painel } from '../../../OkReport';
// export function requiredTeste(value) {

//     if (value) {
//         return undefined
//     } else {

//         console.log('value', value);
//         return 'Requerido'
//     }

// }


// function componentRelEspelho(comp, dados) {
//     return painel([
//         [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados ? dados[comp.field] : "" }]
//     ], { widths: ['*'] })

// }


// function component(item, props) {

//     let disabled = false
//     if (props && props.verificaPermissao && item.permissao_editar) {
//         disabled = !props.verificaPermissao(item.permissao_editar);
//     }
//     if (item.requerido) {
//         return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
//             <Field name={item.field} placeholder={item.label} mask={item.mascara} value={item.value} {...item.propsadd} component={InputMask} />
//         </Item>
//     }

//     return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
//         <Field name={item.field} placeholder={item.label} mask={item.mascara} value={item.value} {...item.propsadd} component={InputMask} />
//     </Item>
// }

// const modelo = {
//     typeComponente: "InputMask",
//     field: "nome",
//     label: "Nome",
//     value: '059.302.031-64',
//     mascara: "[/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/,'-', /\d/, /\d/]",
//     requerido: false,
//     colunaPesquisa: false,
//     filtroPesquisa: false,
//     desativado: false,
// }

// const InputCompMask = {
//     typeComponente: "InputMask",
//     configView: {
//         nome: "Campo Texto com mascara",
//         modelo
//     },
//     componentRelEspelho,
//     component
// }

// export default InputCompMask;