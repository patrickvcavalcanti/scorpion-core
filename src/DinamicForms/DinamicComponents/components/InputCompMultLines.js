import React from 'react'
import { Field } from 'redux-form';
import { Item, InputMulti } from '../../../components';
import { required } from '../function';
import { painel } from '../../../OkReport';




function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })

}

function component(item) {
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field rows={item.linhas || "4"} name={item.field} placeholder={item.label} disabled={item.desativado} component={InputMulti} validate={[required]} {...item.propsadd} />
        </Item>
    }

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field rows={item.linhas || "4"} name={item.field} disabled={item.desativado} placeholder={item.label} component={InputMulti} {...item.propsadd} />
    </Item>
}

const modelo = {
    typeComponente: "TextArea",
    field: "campoTexto",
    label: "Área de Texto",
    linhas: "4",
    requerido: false,
    colunaPesquisa: false,
    filtroPesquisa: false,
    desativado: false,

}

const InputCompMultLines = {
    typeComponente: "TextArea",
    configView: {
        nome: "Campo Texto Multiplas Linhas",
        descricao: "Campo de texto com multiplas linha",
        modelo
    },
    propsField: [
        { field: "field", label: "Field", typeComponente: "Input", },
        { field: "label", label: "Label", typeComponente: "Input", },
        { field: "linhas", label: "Qt Linhas", typeComponente: "InputDecimal", },
    ],

    componentRelEspelho,
    component
}

export default InputCompMultLines;