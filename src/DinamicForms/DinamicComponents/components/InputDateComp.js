import React from 'react'
import { Field } from 'redux-form';
import { Item, InputDate } from '../../../components';
import { required } from '../function';
import { painel } from  '../../../OkReport';

function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })

}

function component(item) {
    // console.log('Item date ', item);
    
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field name={item.field} placeholder={item.label} component={InputDate} validate={[required]} {...item.propsadd} disabled={item.desativado} automatico={item.automatico}/>
        </Item>
    }

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} component={InputDate} {...item.propsadd} disabled={item.desativado} automatico={item.automatico}/>
    </Item>
}

const modelo = {
    typeComponente: "InputDate",
    field: "data",
    label: "Data",
    requerido: false,
    desativado: false,
    automatico: false,
    colunaPesquisa: false,
    filtroPesquisa: false,
}

const InputDateComp = {
    typeComponente: "InputDate",
    configView: {
        nome: "Campo Data",
        modelo,
        renderPropriedades:(props)=>"props"

    },
    componentRelEspelho,
    component
}

export default InputDateComp;