import React from 'react'
import { Field } from 'redux-form';
import { Item, InputDate } from '../../../components';
import { required } from '../function';
import { painel } from  '../../../OkReport';
import { Divider } from '@material-ui/core';

function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })

}

function component(item) {
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field name={item.fieldInicio} placeholder={item.labelInicio} component={InputDate} validate={[required]} {...item.propsadd} style={{marginRight: 10}}/>
            <Field name={item.fieldFinal} placeholder={item.labelFinal} component={InputDate} validate={[required]} {...item.propsadd}/>
        </Item>
    }

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.fieldInicio} placeholder={item.labelInicio} component={InputDate} {...item.propsadd} style={{ flex: 0.5, marginRight: 10}}/>
        <Field name={item.fieldFinal} placeholder={item.labelFinal} component={InputDate} {...item.propsadd} style={{ flex: 0.5}}/>
    </Item>
}

const modelo = {
    // typeComponente: "InputDate",
    fieldInicio: "dataInicio",
    fieldFinal: "dataFinal",
    labelInicio: "Início",
    labelFinal: "Fim",
    requerido: false,
    colunaPesquisa: false,
    filtroPesquisa: false,
   
}

const InputDateInterval = {
    // typeComponente: "InputDate",
    configView: {
        nome: "Intervalo entre datas",
        modelo,
        renderPropriedades:(props)=>"props"

    },
    componentRelEspelho,
    component
}

export default InputDateInterval;