import React from 'react'
import { Field } from 'redux-form';
import { Item, InputDateTime} from '../../../components';
import { required } from '../function';
import { painel } from  '../../../OkReport';

function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })

}

function component(item) {
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field name={item.field} placeholder={item.label} component={InputDateTime} validate={[required]} {...item.propsadd} disabled={item.desativado} automatico={item.automatico}/>
        </Item>
    }

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} component={InputDateTime} {...item.propsadd} disabled={item.desativado} automatico={item.automatico}/>
    </Item>
}

const modelo = {
    typeComponente: "InputDateWithTime",
    field: "dataHora",
    label: "Data com Hora",
    requerido: false,
    desativado: false,
    automatico: false,
    colunaPesquisa: false,
    filtroPesquisa: false,
   
}

const InputDateComp = {
    typeComponente: "InputDateWithTime",
    configView: {
        nome: "Campo Data e Hora",
        modelo,
        renderPropriedades:(props)=>"props"

    },
    componentRelEspelho,
    component
}

export default InputDateComp;