import React from 'react'
import { Field } from 'redux-form';
import { Item, InputDecimal } from '../../../components';
import { required } from '../function';
import { painel } from '../../../OkReport';



function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })

}

function component(item,props) {
    let disabled =  item.desativado
    if (props && props.verificaPermissao && item.permissao_editar) {
        disabled = !props.verificaPermissao(item.permissao_editar);
    }
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field name={item.field} placeholder={item.label} component={InputDecimal} validate={[required]} {...item.propsadd} disabled={disabled} />
        </Item>
    }

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} component={InputDecimal} {...item.propsadd}  disabled={disabled}/>
    </Item>
}

const modelo = {
    typeComponente: "InputDecimal",
    field: "valorDecimal",
    label: "Valor Decimal",
    requerido: false,
    colunaPesquisa: false,
    filtroPesquisa: false,
    desativado: false
}

const InputDecimalComp = {
    typeComponente: "InputDecimal",
    configView: {
        nome: "Campo Numérico",
        descricao: "Campo que aceita somente valores numéricos",
        modelo
    },
    componentRelEspelho,
    component
}

export default InputDecimalComp;