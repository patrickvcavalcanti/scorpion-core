import React from 'react'
import { Field } from 'redux-form';
import { Item, Input } from '../../../components';
import { required } from '../function';
import { painel } from '../../../OkReport';
import { Typography } from '@material-ui/core';



function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }]
    ], { widths: ['*'] })

}

function component(item) {
    let t = ["h1", "h2", "h3", "h4", "h5", "h6", "subtitle1", "subtitle2", "body1", "body2", "caption", "button", "overline", "srOnly", "inherit", "display4", "display3", "display2", "display1", "headline", "title", "subheading"]
    let lista = item.label.split("\n");
    let tam = "h3";
    if (t.includes(item.variant)) {
        tam = item.variant;
    }
    return (<Item key={item.sha} style={{ flex: item.flex || 1, flexDirection: "column" }}>

        {lista.map((itemT, index) => <Typography key={index} variant={tam} style={{ display: 'inline-block' }}>
            {itemT}
        </Typography>)}

    </Item>)
}

const modelo = {
    typeComponente: "Output",
    field: "titulo",
    label: "Titulo",
    variant: "h3",

    requerido: false,
    colunaPesquisa: false,
    filtroPesquisa: false
}

const InputComp = {
    typeComponente: "Output",
    configView: {
        nome: "Texto Fixo",
        modelo
    },
    componentRelEspelho,
    component
}

export default InputComp;