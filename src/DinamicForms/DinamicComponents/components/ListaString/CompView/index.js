import React, { Component } from 'react'

import View from 'react-flexbox'
import { DialogPesquisa, Input } from '../../../../../components';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import MoreHoriz from '@material-ui/icons/MoreHoriz';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';

import produce from "immer"


export default class CompView extends Component {
    constructor(props) {
        super(props);
        this.state = { novo: "" }
        this.getValue = this.getValue.bind(this)
    }
    updateItem(obj) {

        this.dialog.hide();
    }
    getValue() {
        const { input: { value } } = this.props
        return value || []
    }

    sobe(indexAtual) {
        const value = this.getValue();
        const { input: { onChange } } = this.props
        if (indexAtual > 0) {
            const nextState = produce(value || [], draftState => {
                this.array_move(draftState, indexAtual, indexAtual - 1);
            })
            onChange(nextState)
        }
    }

    desce(indexAtual) {
        const value = this.getValue();
        const { input: { onChange }, label } = this.props
        if (indexAtual < value.length - 1) {
            const nextState = produce(value || [], draftState => {
                this.array_move(draftState, indexAtual, indexAtual + 1);
            })
            
            onChange(nextState)
        }

    }

    array_move(arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    };

    render() {
        const value = this.getValue();
        const { input: { onChange } } = this.props

        return (
            <View>
                <Input {...this.props} disabled={true} />
                <Button variant="contained" color="primary" onClick={() => this.dialogCrud.show()}>
                    <MoreHoriz />
                </Button>
                <DialogPesquisa forwardedRef={node => this.dialogCrud = node} notClose>
                    <View column style={{ padding: 5 }}>
                        <View column style={{ flex: "0 0 auto" }}>
                            <div style={{ margin: 5, display: 'inherit', alignItems: 'center' }}>
                                <TextField
                                    label={"Novo"}
                                    value={this.state.novo}
                                    onChange={event => this.setState({ novo: event.target.value })}

                                    fullWidth
                                />
                                <Button variant="contained" color="primary" size="small" onClick={() => {
                                    let newArr = value.slice();
                                    newArr.push(this.state.novo.trim())
                                    onChange(newArr)
                                    this.setState({ novo: "" })
                                }}>
                                    ADD
                            </Button>
                            </div>
                            <Typography variant="h6">Valores</Typography>
                            <Divider />
                        </View>
                        <View column style={{ justifyContent: 'flex-start', flex: 1, overflow: 'auto', height: 1 }}>


                            {value.map((item, index) => (
                                // <View key={index} style={{ margin: 5,  alignItems: 'center', height:1  }}>
                                <div key={index} style={{ display: 'flex', margin: 5, minHeight: 38 }} >

                                    <Typography style={{ flex: 1 }}>{item}</Typography>
                                    <Button variant="contained" size="small" onClick={() => {
                                        var newArray = value.slice();
                                        newArray.splice(index, 1);
                                        onChange(newArray)
                                    }}>
                                        Remover
                                    </Button>
                                    <Button size="small" onClick={() => this.sobe(index)}>
                                        <KeyboardArrowUp />
                                    </Button>
                                    <Button size="small" onClick={() => this.desce(index)}>
                                        <KeyboardArrowDown />
                                    </Button>

                                </div>

                                // </View>
                            ))}

                        </View>

                    </View>
                </DialogPesquisa>

            </View>
        )
    }
}
