import React from 'react'
import { Field } from 'redux-form';
import { Item } from '../../../../components';
import CompView from './CompView'
function componentRelEspelho(comp, dados) {
    return ""

}

function component(item, ptops) {
    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={item.field} placeholder={item.label} item={item} component={CompView}  {...item.propsadd} />
    </Item>
}

const modelo = {
    typeComponente: "ListaString",
    field: "listaStr",
    label: "Lista String",

}

const InputComp = {
    typeComponente: "ListaString",
    configView: {
        nome: "Lista Strings",
        modelo
    },
    componentRelEspelho,
    component
}

export default InputComp;