import React from 'react'



import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const Item = (props) => {
    const { input: { value,onChange }, label } = props

    return <FormControlLabel control={<Checkbox checked={value ? true : false} onChange={(evt) => onChange(evt.target.checked)} />} label={label} />
}
export default Item;