import React from 'react'
import { Field } from 'redux-form';
import { Item, Input } from '../../../../components';
import { required } from '../../function';
import { painel, fieldset } from '../../../../OkReport';

import EditorLista from '../../editors/EditorLista';

import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import ItemMulti from './ItemMulti';


function componentRelEspelho(comp, dados) {
    let lista = [];
    let listaTrue = [];


    for (let prop in dados[comp.field]) {
        lista.push({ label: prop, sel: dados[comp.field][prop] })
    }

    lista.forEach(l => {
        if (l.sel) {
            listaTrue.push([{ style: "cellValor", text: l.label }]);
        }
    })

    // console.log("Lista selecionados ", lista)
    // return fieldset(comp.label,)

    return fieldset(comp.label, painel([
        [listaTrue]
    ], { widths: ['*'] }))

}

// handleChange = name => event => {
//     this.setState({ [name]: event.target.checked });
//   };

function component(item) {
    return (<Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <FormControl key={item.field} data={item.opcoes}>
            <FormLabel component="legend">{item.label}</FormLabel>
            {item.opcoes.map((opcao, index) => {
                return <Field key={item.field + index} name={item.field + "." + opcao} label={opcao} component={ItemMulti} />
            })}
        </FormControl>
    </Item>)
}

const modelo = { field: "atividades", label: "Atividades Preferidas", typeComponente: "MultiplaSelecaoCheckBox", opcoes: ["Assistir Filmes ", "Futebol ", "Churrasco "] }

const comp = {
    typeComponente: "MultiplaSelecaoCheckBox",
    configView: {
        nome: "Seletor Múltiplo",
        modelo,
        editores: {
            opcoes: EditorLista
        },
    },
    componentRelEspelho,
    component
}

export default comp;