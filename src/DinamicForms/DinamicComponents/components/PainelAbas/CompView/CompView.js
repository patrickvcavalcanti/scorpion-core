import React, { Component } from 'react'

import View from 'react-flexbox'



import { Button, Typography, AppBar, Tabs, Tab } from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';



import ItemCustomRender from '../../../../developer/RenderCustomForm/components/ItemCustomRender/ItemCustomRender';
import RenderCustomForm from '../../../../developer/RenderCustomForm';

class CompView extends Component {
    constructor(props) {
        super(props)
        this.state = { tab: 0 };

    }

    handleChange = (event, value) => {
        this.setState({ tab: value });
    };
    render() {
        const { item, propsview } = this.props
        const { tab } = this.state
        

        let st = {}



        let componentes = [];
        // console.log("Entrou aqui ", propsview);

        if (propsview && propsview.verificaPermissao) {

            item.componentes.forEach((comp, index) => {
                if (comp.visible !== false) {
                    if (comp.permissao) {
                        if (propsview.verificaPermissao(comp.permissao)) {
                            componentes.push(comp)
                        }
                    } else {
                        componentes.push(comp)
                    }
                }


            })


        } else {
            componentes = item.componentes
        }

        return (
            <View column style={{ flex: 1, display: item.usarFlex === true ? 'flex' : 'block' }}>
                <AppBar position="static">
                    <Tabs value={tab} onChange={this.handleChange}>
                        {componentes.map((comp, index) => <Tab key={index} label={comp.label} />)}
                    </Tabs>
                </AppBar>
                {componentes.map((comp, index) => {
                    // console.log('Comp ', comp.usarFlex);
                    if(tab != index){
                        return "";
                    }
                    return <div key={"pn" + item.uid + "index" + index} style={{display: comp.usarFlex === true ? 'flex' : 'block', flex: '1'}}>
                        {/* <ItemCustomRender */}
                        {/* //     key={index}
                        //     item={comp}
                        //     propsadd={{ style: {} }}
                        //     propsview={propsview}
                        // /> */}
                        <RenderCustomForm key={"render-" + item.uid + "index" + index} componenteRoot={comp} styleContainer={st} propsview={propsview} />
                    </div>
                })}
            </View>

        )
    }
}


export default CompView;
