import React from 'react'

import { Field } from 'redux-form';
import { Item, TabelaScorpion, TabelaOkds } from '../../../../components';
// import RenderCustomForm from '../../developer/RenderCustomForm';
import { painel } from '../../../../OkReport';

import CompView from './CompView'
function componentRelEspelho(comp, dados, getCompRel) {

    const { componentes } = comp;
    let widths = componentes.map(c => "*");
    let dentro = componentes.map(c => getCompRel(c, dados));
    return painel([
        dentro
    ], { widths })

}

function component(item, propsview) {
    // return (<RenderCustomForm key={"render-" + item.uid} componenteRoot={item} styleContainer={st} propsview={propsview} />)

    return <Item key={item.sha} style={{ flex: item.flex || 1,  paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,}}>
        <CompView item={item} propsview={propsview} />
    </Item>
    // return <div style={{height: '100%', padding: 0, flex: item.flex || 1}} key={item.sha}>
    //     <CompView item={item} propsview={propsview} />
    // </div>
}

const modelo = {
    label: "Painel Abas",
    typeComponente: "PainelAbas",
    usarFlex: false,
    componentes: [
        { label: "PN1", typeComponente: "PainelHorizontal", componentes: [], vertical: true, visible: true, usarFlex: false },
        { label: "PN2", typeComponente: "PainelHorizontal", componentes: [], vertical: true, visible: true, usarFlex: false },
    ]
}
const comp = {
    typeComponente: "PainelAbas",
    configView: {
        nome: "Painel Abas",
        modelo,
    },
    propsField: [
        { field: "label", label: "label", typeComponente: "Input", },
    ],
    componentRelEspelho,
    component
}



export default comp;