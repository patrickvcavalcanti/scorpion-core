import React from 'react'
import { Field } from 'redux-form';
import { Item, TabelaScorpion, TabelaOkds } from '../../../../components';
import { painel, fieldset } from '../../../../OkReport';
import ItemCustomRender from '../../../developer/RenderCustomForm/components/ItemCustomRender/ItemCustomRender';

import PainelFotos from './components/PainelFotos';

function componentRelEspelho(comp, dados) {
    let listaColunas = [];

    listaColunas.push(comp.columns.map(c => {
        return [{ style: "cellTitulo", text: c.label }]
    }))

    return (fieldset(comp.label,
        painel([
            [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: "Não implementado" }]
        ], { widths: ['*'] })
    ))

}


function component(item, props) {
    let colunas = [];

    item.componentes.forEach((comp, index) => {
        colunas.push({
            ...comp, getValue: (itemArray, indexArray) => (
                <ItemCustomRender
                    item={{ ...comp, field: item.field + "." + indexArray + "." + comp.field, label: undefined }}
                    propsadd={{ style: {} }}
 
                />)
        })
    });

    let mensagemVazio = "Sem Itens"

    return <Item key={item.sha} style={{ flex: item.flex || 1, display: 'block' }}>
        <Field name={item.field} item={item} component={PainelFotos} propsSuper={props} />
    </Item>
}

const modelo = {
    field: "itens",
    label: "Tabela de Imagens",
    typeComponente: "PainelFotos",
    pegarGeoreferencia: false,
    botaoRemoverAntes: false,
    componentes: []
}

const comp = {
    typeComponente: "PainelFotos",
    configView: {
        nome: "Lista de Imagens",
        modelo,
    },
    componentRelEspelho,
    component
}



export default comp;