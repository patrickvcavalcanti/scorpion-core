import React, { Component } from "react";
import View from 'react-flexbox';
import { Button, Typography, Dialog, Tooltip } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import Dropzone from 'react-dropzone';
import { Redo, Undo, EvStationTwoTone, SettingsVoiceOutlined } from '@material-ui/icons';
// import { DialogConfirma, DialogSeletor, Item } from "scorpion-core";
// import ItemCustomRender from "scorpion-core";
import ReactCrop, { makeAspectCrop } from 'react-image-crop';
import withStyles from '@material-ui/core/styles/withStyles';
import ItemCustomRender from "../../../../developer/RenderCustomForm/components/ItemCustomRender/ItemCustomRender";
import { DialogConfirma, DialogSeletor } from "../../../../../components";

function resizeImage2(image, maxWidth, maxHeight, quality) {
    var canvas = document.createElement('canvas');

    var width = image.width;
    var height = image.height;

    if (width > height) {
        if (width > maxWidth) {
            height = Math.round(height * maxWidth / width);
            width = maxWidth;
        }
    } else {
        if (height > maxHeight) {
            width = Math.round(width * maxHeight / height);
            height = maxHeight;
        }
    }

    canvas.width = width;
    canvas.height = height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(image, 0, 0, width, height);
    return canvas.toDataURL("image/jpeg", quality);
}

function getRenderItemComp(componente, indexL) {
    let value = componente.getValue(componente, indexL);
    let comp = value;

    console.log('Vsalue ', value);
    console.log('Index  ', indexL);
    if (typeof value === 'string' || typeof value === 'number') {
        comp = <Typography style={{ overflow: 'auto', }} {...componente.props}> {value}</Typography>
    }

    return <View key={"comp" + comp.label + indexL}>{comp}</View>
}

const painelFoto = (item, index, startRemover, rotateBase64Image, updadeItem, props) => {
    console.log('Prpos ', item);

    let comps = [];
    let itemP = props.item;
    const { propsSuper, classes } = props;
    itemP.componentes.forEach((comp) => {
        comps.push({
            ...comp, getValue: (itemArray, indexArray) => {
                return <ItemCustomRender
                    item={{ ...comp, field: itemP.field + "." + indexArray + "." + comp.field, label: undefined }}
                    propsadd={{ style: {} }}
                    propsview={{ ...propsSuper, propEditor: { item: itemArray } }}
                />
            }

        })
    });

    let placeContent = ''
    let rotaGoogleMaps = item.georeferencia ? `https://www.google.com.br/maps/dir//${item.georeferencia.lat},${item.georeferencia.lng}` : null
    
    if (comps.length === 0 && !itemP.pegarGeoreferencia) {
        placeContent = 'space-around'
    }

    return <View key={index} style={{
        padding: 10,
        borderStyle: 'solid',
        borderRadius: 8,
        borderWidth: 2,
        placeContent: placeContent
    }}
        className={classes.painelGeral}
    >
        <View className={classes.painelImagem} style={{ flexDirection: "column", alignItems: 'center', justifyContent: 'center', maxHeight: 200, minWidth: 200 }}>
            {/* <View style={{ flexDirection: "column", alignItems: 'center', justifyContent: 'center', maxWidth: 240, maxHeight: 240, minWidth: 240, minHeight: 240 }}> */}
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <img style={{
                    maxWidth: 200,
                    maxHeight: 200,
                    width: 'auto',
                    height: 'auto',
                }} src={"data:image/jpeg;base64," + item.imagem.base64} />
            </View>
            <View style={{display: 'block', flex: 0 }}>
                <Button onClick={() => {
                    rotateBase64Image({ ...item, index }, updadeItem, false);
                }}> <Undo /></Button>
                <Tooltip title="Remover"><Button onClick={() => startRemover({ ...item, index })}><DeleteIcon color="primary" /></Button></Tooltip>
                <Button onClick={() => {
                    rotateBase64Image({ ...item, index }, updadeItem, true);
                }}><Redo /></Button>
            </View>
        </View>
        {rotaGoogleMaps && <View style={{ flexDirection: 'column', marginLeft: 10, height: 19 }}>
            <a href={rotaGoogleMaps} target="_blank">
                <Button variant="contained" color="primary" >{"Rota"}</Button>
            </a>

            {comps.length != 0 &&
                <View style={{ flexDirection: "column" }}>
                    <View style={{ flexDirection: "column", justifyContent: 'flex-start', padding: 5 }}>
                        {comps.map((comp, indexC) => {
                            return (<View column>
                                {comp.label}
                                {getRenderItemComp(comp, index)}
                            </View>)
                        })}
                    </View>
                </View>
            }
        </View>}


        {
            !itemP.pegarGeoreferencia && comps.length != 0 &&
            <View style={{ flexDirection: "column" }}>
                <View style={{ flexDirection: "column", justifyContent: 'flex-start', padding: 5 }}>
                    {comps.map((comp, indexC) => {
                        return (<View column>
                            {comp.label}
                            {getRenderItemComp(comp, index)}
                        </View>)
                    })}
                </View>
            </View>

        }

    </View>
}


class PainelFotos extends Component {
    constructor(props) {
        super(props);

        this.state = {
            localAtual: null,
            image: null,
            files: [],
            src: null,
            crop: {
                x: 10,
                y: 10,
                unit: "%",
                // width: 80,
                // height: 80,
                aspect: 1
            },
            novaImagem: null,
        }
        this.updadeItem = this.updadeItem.bind(this);
        this.processaImagem = this.processaImagem.bind(this);
        this.addFoto = this.addFoto.bind(this);
        this.startRemover = this.startRemover.bind(this);
        this.rotateBase64Image = this.rotateBase64Image.bind(this);
        this.getAtualLocal = this.getAtualLocal.bind(this);
        this.setAtualLocal = this.setAtualLocal.bind(this);
    }

    componentWillReceiveProps(props) {
        // console.log('Props receive ', props);
        const { item } = props

        if (item.pegarGeoreferencia) {
            this.getAtualLocal()
        }

    }

    getAtualLocal() {
        if ("geolocation" in navigator) {

            // check if geolocation is supported/enabled on current browser
            navigator.geolocation.getCurrentPosition(
                this.setAtualLocal,
                function error(error_message) {
                    // for when getting location results in an error
                    console.error('An error has occured while retrieving location', error_message)
                }
            );
        } else {
            // geolocation is not supported
            // get your location some other way
            console.log('geolocation is not enabled on this browser')

        }
    }

    setAtualLocal(position) {
        // console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude);
        let local = { lat: position.coords.latitude, lng: position.coords.longitude }

        this.setState({ localAtual: local })

    }

    onDrop(files) {
        this.setState({
            files
        });
        files.forEach(file => {
            const reader = new FileReader();
            reader.onload = (event) => {
                this.setState({ src: reader.result, image: event.target.result })
                // this.processaImagem(event.target.result, this.addFoto)
            };
            reader.readAsDataURL(file);
        });
        // this.dialog.hide();
    }

    onImageLoaded = (image) => {
        // console.log('onImageLoaded', image)
        // console.log("this ", this);

        this.setState({ image });
    }

    onCropComplete = (crop, pixelCrop) => {
        // console.log('onCropComplete', crop)
        // this.setState({ novoLogo: crop })
        var canvas = document.createElement('canvas');
        canvas.width = pixelCrop.width;
        canvas.height = pixelCrop.height;
        var ctx = canvas.getContext('2d');

        ctx.fillStyle = "#FFFFFF";
        ctx.fillRect(0, 0, this.state.width, this.state.height);

        ctx.drawImage(
            this.state.image,
            pixelCrop.x,
            pixelCrop.y,
            pixelCrop.width,
            pixelCrop.height,
            0,
            0,
            pixelCrop.width,
            pixelCrop.height
        );

        // Base64
        const base64Image = canvas.toDataURL('image/png');
        // console.log('base64Image', base64Image);
        this.setState({ novaImagem: base64Image })
    }

    onCropChange = crop => {
        this.setState({ crop })
    }

    rotateBase64Image(item, updadeItem, direita) {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");

        var image = new Image();
        // console.log("Item ", item);

        image.src = "data:image/png;base64," + item.imagem.base64;

        image.onload = function () {
            canvas.width = image.height;
            canvas.height = image.width;
            if (direita) {
                ctx.rotate(90 * Math.PI / 180);
                ctx.translate(0, -canvas.width);
            } else {
                ctx.rotate(-90 * Math.PI / 180);
                ctx.translate(-canvas.height, 0);
            }
            ctx.drawImage(image, 0, 0);
            let base64Rotate = canvas.toDataURL();
            base64Rotate = base64Rotate.substring(base64Rotate.indexOf(",") + 1, base64Rotate.length)
            item.imagem.base64 = base64Rotate
            updadeItem(item);
            // console.log("Passou ", base64Rotate);
        };

    }

    processaImagem(data, funcaoAdd) {
        var image = new Image();
        image.src = data;
        image.onload = function () {
            var resizedDataUrl = resizeImage2(image, 480, 480, 0.7);
            funcaoAdd(resizedDataUrl);
        };
    }
    addFoto(data) {
        var res = data.split(";base64,");
        let ext = data.slice(11, 15);

        let base64 = res[1];

        // console.log('Local atual ', this.state.localAtual);
        const foto = { imagem: { base64: base64, galeria: true, extencao: ext }, georeferencia: this.state.localAtual, georeferenciaStr: 'Lat: '+(this.state.localAtual ? this.state.localAtual.lat : '-')+', Lng: '+ (this.state.localAtual ? this.state.localAtual.lng : '-')};
        this.updadeItem(foto);
    }


    showAdd() {
        this.dialog.show();
    }

    updadeItem(obj) {
        const { index } = obj;
        let fotos = this.props.input.value;

        var newArray = []
        if (fotos) {
            newArray = fotos.slice();
        }
        if (index != null) {

            newArray[index] = obj;
        } else {
            newArray.push(obj);
        }

        this.updateValue(newArray)
    }
    updateValue(value) {
        this.props.input.onChange(value);
    }
    startRemover(item) {
        this.setState({ editando: item }, () => {
            this.dialogConfirma.show();
        });
    }
    remover() {
        const remover = this.state.editando;
        let { value } = this.props.input;
        var newArray = []
        if (value) {
            newArray = value.slice();
        }

        var index = remover.index;

        if (index > -1) {
            newArray.splice(index, 1);
        }
        this.updateValue(newArray)

    }

    concluir() {
        const { novaImagem, image } = this.state;

        // const { src } = this.novoLogo;
        // console.log('src', src);
        var res = novaImagem ? novaImagem.split(";base64,") : image;
        // console.log('data', data);
        let base64 = res[1];
        // this.props.change(base64)
        let img = novaImagem ? novaImagem : image.src;
        this.processaImagem(img, this.addFoto)
        // this.props.change("logoEmpresa", base64)

        this.setState({
            image: null,
            src: null,
            crop: {
                x: 10,
                y: 10,
                width: 80,
                height: 80,
            },
            novoLogo: null
        })

        this.dialog.hide();
    }

    render() {
        const {
            input: { value, onChange },
            meta: { touched, error },
            item
        } = this.props;
        let fotos = [];
        if (value) fotos = value;
        return (
            <View style={{ flexDirection: "column" }}>
                <View>
                    <Typography variant="h6" >{item.label}</Typography>
                    <Button variant="contained" color="primary" onClick={() => this.showAdd()}>Adicionar</Button>
                </View>
                <View style={{ flexDirection: 'column', }}>

                    {fotos.map((item, index) =>
                        painelFoto(item, index, this.startRemover, this.rotateBase64Image, this.updadeItem, this.props)
                    )}

                    {!fotos.length && <Typography style={{ textAlign: "center" }}
                    >
                        Sem Fotos
                        </Typography>}
                </View>
                <DialogSeletor innerRef={node => this.dialog = node} notClose={true}>
                    <View column style={{
                        justifyContent: 'center'
                    }}>
                        {!this.state.src ? <Dropzone
                            onDrop={this.onDrop.bind(this)}
                            accept="image/jpeg, image/png"
                        >
                            {({ getRootProps, getInputProps }) => (
                                <section>

                                    <View column {...getRootProps()} style={{
                                        height: '100%',
                                        alignItems: "center", justifyContent: 'center'

                                    }}>
                                        <input {...getInputProps()} />
                                        <p>Arraste as fotos para este local.</p>
                                        <Button variant="contained" color="primary" >Selecionar</Button>
                                    </View>
                                </section>
                            )}
                        </Dropzone>
                            :
                            <div style={{
                                height: 150,
                                borderWidth: 2, borderStyle: 'dashed', borderRadius: 5,
                                alignItems: "center", justifyContent: 'center',
                            }}>

                                <ReactCrop
                                    src={this.state.src}
                                    crop={this.state.crop}
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={this.onCropChange}
                                />

                            </div>
                        }
                        <View>
                            <Button onClick={() => this.concluir()}>OK</Button>
                            <Button onClick={() => {

                                this.setState({
                                    image: null,
                                    src: null,
                                    crop: {
                                        x: 10,
                                        y: 10,
                                        unit: "%",
                                        // width: 80,
                                        // height: 80,
                                        aspect: 1
                                    },
                                    novoLogo: null
                                })
                                this.dialog.hide()
                            }}>Cancelar</Button>
                        </View>
                    </View>
                </DialogSeletor>
                <DialogConfirma innerRef={node => this.dialogConfirma = node} titulo={"Remover"} mensagem={"Deseja mesmo remover esta foto?"} onConfirm={() => { this.remover() }} />
            </View>
        );
    }
}

const styles = theme => {
    return {
        painelGeral: {
            [theme.breakpoints.down('sm')]: {
                display: 'block !important',
            }
        },
        painelImagem: {
            [theme.breakpoints.up('sm')]: {
                maxWidth: 200,
            }
        }
    }
}

PainelFotos = withStyles(styles)(PainelFotos)

export default PainelFotos;