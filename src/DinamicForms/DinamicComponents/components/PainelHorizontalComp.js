import React from 'react'

import RenderCustomForm from '../../developer/RenderCustomForm';
import { painel } from '../../../OkReport';

function componentRelEspelho(comp, dados, getCompRel) {

    const { componentes } = comp;
    let widths = componentes.map(c => "*");
    let dentro = componentes.map(c => getCompRel(c, dados));
    return painel([
        dentro
    ], { widths })

}

function component(item, propsview) {
    let st = { flexDirection: 'row' }
    if (item.vertical) {
        st = {}
    }

    if (item.style) {
        st = { ...st, ...item.style, }
    } else {

    }
    if (item.visible !== false) {
        return (<RenderCustomForm key={"render-" + item.uid} componenteRoot={item} styleContainer={st} propsview={propsview} />)
    }else{
        return <div></div>
    }
}

const modelo = { label: "Painel Horizontal", typeComponente: "PainelHorizontal", componentes: [], vertical: false, visible: true, usarFlex: false}
const comp = {
    typeComponente: "PainelHorizontal",
    configView: {
        nome: "Painel Horizontal",
        modelo,
    },
    propsField: [
        { field: "label", label: "label", typeComponente: "Input", },
        { field: "vertical", label: "Vertical", typeComponente: "SwitchItem", },
    ],
    componentRelEspelho,
    component
}



export default comp;