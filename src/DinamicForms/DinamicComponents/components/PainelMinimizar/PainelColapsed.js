import React, { Component } from 'react'

import { ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import View from 'react-flexbox'
import RenderCustomForm from '../../../developer/RenderCustomForm';
export class PainelColapsed extends Component {

    render() {
        const { item, confrender, propsview } = this.props;
        const { label, resumo, disabled_mensagem, disabled, icone } = item;

        return (
            <ExpansionPanel key={this.props.sha} {...this.props} disabled={disabled} >
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon style={{ color: '#000' }} />} style={{ backgroundColor: "#eeeeee", alignItems: 'center' }}>
                    <View style={{ backgroundColor: "transparent", borderRadius: 8, padding: 5, paddingLeft: 15, alignItems: 'center', justifyContent: "flex-start", flex: '1' }}>
                        {icone &&
                            <img height={24} width={24} style={{ marginRight: 15 }} src={icone} />
                        }

                        <Typography style={{
                            fontSize: 20,
                            fontFamily: "Roboto",
                            fontWeight: '300',
                            color: '#000',
                        }}>{label ? label : "Titulo"}</Typography>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: "flex-start", flex: '2' }}>
                        {resumo && <Typography style={{
                            paddingLeft: 35,
                            fontFamily: "Roboto",
                            fontWeight: '300',
                            color: '#b1b1b1',
                        }}>{resumo}</Typography>}
                        {disabled && <Typography style={{
                            paddingLeft: 35,
                            fontFamily: "Roboto",
                            fontWeight: '300',
                            color: '#000',
                        }}>{disabled_mensagem}</Typography>}
                    </View>

                </ExpansionPanelSummary>
                <ExpansionPanelDetails style={{ marginTop: 10 }}>
                    <RenderCustomForm key={"render-" + item.uid} componenteRoot={item} confrender={confrender} propsview={propsview} />
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }
}

const propTypes = {

};

PainelColapsed.propTypes = propTypes;

export default PainelColapsed
