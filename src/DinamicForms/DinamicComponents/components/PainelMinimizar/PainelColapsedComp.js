import React from 'react'
import { fieldset } from '../../../../OkReport';
import PainelColapsed from './PainelColapsed';

function componentRelEspelho(comp, dados, getCompRel) {


    const { componentes } = comp;
    let widths = componentes.map(c => "*");
    let dentro = componentes.map(c => getCompRel(c, dados));
    return fieldset(comp.label, dentro)

}
function component(item,propsview) {
    return (<PainelColapsed sha={item.sha} item={item} propsview={propsview}/>)
}

const modelo = { label: "Painel Minimizavel", typeComponente: "PainelColapsed", componentes: [] }
const comp = {
    typeComponente: "PainelColapsed",
    configView: {
        nome: "Painel Minimizavel",
        modelo,
    },
    componentRelEspelho,
    component
}
export default comp;