import React from 'react'
import { Field } from 'redux-form';
import { Item, Input } from '../../../components';
import { required } from '../function';
import { painel } from '../../../OkReport';


function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados ? dados[comp.field] : "" }]
    ], { widths: ['*'] })

}

function component(item,ptops) {

    return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
        <Field name={"sequencialIndex"} placeholder={item.label} disabled={true} component={Input}  {...item.propsadd} />
    </Item>
}

const modelo = {
    typeComponente: "sequencialIndex",    
    label: "Código",    
    colunaPesquisa: false,
    filtroPesquisa: false,
    
}

const InputComp = {
    typeComponente: "sequencialIndex",
    configView: {
        nome: "Campo Sequencial",
        modelo
    },
    componentRelEspelho,
    component
}

export default InputComp;