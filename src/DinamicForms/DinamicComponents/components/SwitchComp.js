import React from 'react'
import { Field } from 'redux-form';
import { Item, SwitchItem } from '../../../components';
import { required } from '../function';
import { painel } from '../../../OkReport';

function componentRelEspelho(comp, dados) {
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] ? "Sim" : "Não" }]
    ], { widths: ['*'] })

}

function component(item, props) {


    let disabled = item.desativado
    if (props && props.verificaPermissao && item.permissao_editar) {
        disabled = !props.verificaPermissao(item.permissao_editar);
    }
    if (item.requerido) {
        return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
            <Field key={item.field} name={item.field} placeholder={item.label} component={SwitchItem} validate={[required]} disabled={disabled} />
        </Item>
    }

    return (<Item key={item.sha} style={{ flex: item.flex || 1 }}>

        <Field key={item.field} name={item.field} placeholder={item.label} component={SwitchItem} disabled={disabled} />
    </Item>)
}

const modelo = {
    field: "ligado",
    label: "Ligado",
    colunaPesquisa: false,
    desativado: false,
    typeComponente: "SwitchItem"
}

const comp = {
    typeComponente: "SwitchItem",
    configView: {
        nome: "Falso/Verdadeiro",
        modelo,
    },
    componentRelEspelho,
    component
}

export default comp;