import React, { Component } from 'react'

import View from 'react-flexbox'


import { Item, TabelaScorpion, TabelaOkds, DialogConfirma } from '../../../../../components';

import { Button, Typography } from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';



import ItemCustomRender from '../../../../developer/RenderCustomForm/components/ItemCustomRender/ItemCustomRender';

class CompView extends React.Component {
    constructor(props) {
        super(props)
        this.state = { editando: null };
        this.remover = this.remover.bind(this);
        this.startRemover = this.startRemover.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState){
        const {propsSuper} = nextProps;


        if(this.props.input.name && this.props.propsSuper.atualValues && this.props.propsSuper.atualValues[this.props.input.name] !== propsSuper.atualValues[this.props.input.name] && this.props.propsSuper.atualValues[this.props.input.name]){
          return true;
        }
        return true;
    }

    startRemover(item) {
        this.setState({ editando: item }, () => {
            this.dialogConfirma.show();
        });
    }

    remover() {
        const { input: { value, onChange } } = this.props
        const remover = this.state.editando;
        // let { value } = this.props.input;
        var newArray = []
        if (value) {
            newArray = value.slice();
        }

        var index = remover.index;

        if (index > -1) {
            newArray.splice(index, 1);
        }
        // var newArray = value.slice();
        // newArray.splice(remover.index, 1);
        onChange(newArray)
    }

    render() {
        const { input: { value, onChange }, item, propsSuper, paginacao, funcAdicionou } = this.props
        let colunas = [];

        let adicionou = null
        try {
            adicionou = funcAdicionou
        } catch (error) {

        }

        // console.log('Item tabbb  ', item);

        item.componentes.forEach((comp, index) => {
            colunas.push({
                ...comp, getValue: (itemArray, indexArray) => {
                    return <ItemCustomRender
                        item={{ ...comp, field: item.field + "." + indexArray + "." + comp.field, label: undefined }}
                        propsadd={{ style: {} }}
                        propsview={{ ...propsSuper, propEditor: { item: itemArray } }}

                    />
                }

            })
        });

        let mensagemVazio = "Sem Itens"


        let data = [];
        if (value && value.slice) {
            data = value.sort(function (a, b) {
                let objetoAVazio = true;
                Object.keys(a).forEach(function (item) {
                    if (a[item]) {
                        objetoAVazio = false;
                    }
                })

                let objetoBVazio = true;
                Object.keys(b).forEach(function (item) {
                    if (a[item]) {
                        objetoBVazio = false;
                    }
                })

                if (objetoAVazio === false || objetoBVazio === false) {
                    return 1;
                }
                return -1
            })
        }

        return (
            <View column>
                <View>
                    <Typography variant="h6" >{item.label}</Typography>
                    <Button variant="contained" color="primary" onClick={() => {
                        let newArray = [].concat(data.slice());
                        newArray.push({});
                        if (adicionou) {

                            adicionou(this.props, item)
                        }
                        // console.log('Item novo ', newArray);
                        onChange(newArray)
                    }
                    }> <AddIcon />Adicionar</Button>
                </View>


                <TabelaOkds quebraMobile={item.quebraMobile} display={'grid'} exibeCodigo={item.exibirColunaIndice} colunas={colunas} paginacao={paginacao} mensagemVazio={mensagemVazio} data={data} botaoRemoverAntes={item.botaoRemoverAntes} habilitaOrdenacao={item.habilitaOrdenacao} onChange={onChange} removeItem={(item) => {
                    this.startRemover(item);
                }} />
                <DialogConfirma innerRef={node => this.dialogConfirma = node} titulo={"Remover"} mensagem={"Deseja mesmo remover este item?"} onConfirm={() => { this.remover() }} />
            </View>

        )
    }
}


export default CompView;
