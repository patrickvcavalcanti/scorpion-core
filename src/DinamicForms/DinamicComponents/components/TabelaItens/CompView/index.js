import CompView from './CompView'
import enhancer from './CompView.enhancer'

export default enhancer(CompView)