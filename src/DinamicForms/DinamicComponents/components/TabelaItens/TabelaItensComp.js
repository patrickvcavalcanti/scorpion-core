import React from 'react'
import { Field } from 'redux-form';
import { Item, TabelaScorpion, TabelaOkds } from '../../../../components';
import { required } from '../../function';
import { painel, fieldset } from '../../../../OkReport';
import { Button, Typography } from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';

import RenderCustomForm from '../../../developer/RenderCustomForm';
import ItemCustomRender from '../../../developer/RenderCustomForm/components/ItemCustomRender/ItemCustomRender';
import View from 'react-flexbox'

import CompView from './CompView'
import EditorCodigo from '../../editors/EditorCodigo';

function componentRelEspelho(comp, dados) {
    let listaColunas = [];

    listaColunas.push(comp.columns.map(c => {
        return [{ style: "cellTitulo", text: c.label }]
    }))

    return (fieldset(comp.label,
        painel([
            [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: "Não implementado" }]
        ], { widths: ['*'] })
    ))

}


function component(item, props) {
    let colunas = [];
    let funcClickAdd = null;
    item.componentes.forEach((comp, index) => {
        colunas.push({
            ...comp, getValue: (itemArray, indexArray) => (
                <ItemCustomRender
                    item={{ ...comp, field: item.field + "." + indexArray + "." + comp.field, label: undefined }}
                    propsadd={{ style: {} }}

                />)
        })
    });

    let mensagemVazio = "Sem Itens"

    try {
        funcClickAdd = new Function("return " + item.strFuncAdicionou)();
    } catch (error) {
        // console.log('error', error);
    }



    // return <Item key={item.sha} style={{ flex: item.flex || 1 }}>
    //     <Field name={item.field} component={(props) => {
    //         const { input: { value, onChange } } = props

    //         let data = [];
    //         if (value) {
    //             data = value
    //         }
    //         return <View column>
    //             <View>
    //                 <Typography variant="h6" >{item.label}</Typography>
    //                 <Button variant="contained" color="primary" onClick={() => onChange(data.concat([{}]))}> <AddIcon />Adicionar</Button>
    //             </View>
    //             <div >

    //                 <TabelaOkds colunas={colunas} mensagemVazio={mensagemVazio} data={data} />
    //             </div>
    //         </View>
    //     }} />
    // </Item>

    return <Item key={item.sha} style={{ flex: item.flex || 1, display: 'block' }}>

        <Field name={item.field} item={item} component={CompView} propsSuper={props} funcAdicionou={funcClickAdd} paginacao={item.paginacao} />
    </Item>
}

const modelo = {
    field: "itens", label: "Tabela de Itens", typeComponente: "TabelaListaItens",
    exibirColunaIndice: false,
    botaoRemoverAntes: false,
    habilitaOrdenacao: false,
    quebraMobile: true,
    paginacao: "",
    componentes: [
        { field: "nome", label: "Nome", typeComponente: "Input" },
        { field: "descricao", label: "Descrição", typeComponente: "Input", },
    ],
    strFuncAdicionou: `function funcAdicionou(propsGeral) {
        console.log('Props geral ', propsGeral)
    }`,

}
const comp = {
    typeComponente: "TabelaListaItens",
    configView: {
        nome: "Tabela de Itens",
        modelo,
        editores: {
            strFuncAdicionou: EditorCodigo
        },
    },
    componentRelEspelho,
    component
}



export default comp;