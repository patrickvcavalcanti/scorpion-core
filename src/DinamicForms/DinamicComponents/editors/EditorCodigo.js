import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MoreHoriz from '@material-ui/icons/AspectRatio';

import View from 'react-flexbox'
import { DialogPesquisa, EditorCodeField } from '../../../components';


export default class EditorCodigo extends Component {
    constructor(props) {
        super(props);
    }


    render() {

        const { value, onChange } = this.props

        return (
            <View style={{ height: 250 }}>
                <EditorCodeField
                    input={{ value, onChange }}
                    meta={{ touched: "", error: "" }}
                    fullWidth
                />
                <Button variant="contained" color="primary" onClick={() => this.dialog.show()}>
                    <MoreHoriz />
                </Button>
                <DialogPesquisa forwardedRef={node => this.dialog = node}>

                    <EditorCodeField
                        input={{ value, onChange }}
                        meta={{ touched: "", error: "" }}
                        fullWidth
                    />

                </DialogPesquisa>

            </View>

        )
    }
}
