import React, { Component } from 'react'
import { TextField, Button, } from '@material-ui/core';


import View from 'react-flexbox'
import { DialogSeletor, DialogConfirma } from '../../../components';
import MoreHoriz from '@material-ui/icons/MoreHoriz';
import Dropzone from 'react-dropzone'
// import FileUploader from "react-firebase-file-uploader";


function resizeImage2(image, maxWidth, maxHeight, quality) {
    var canvas = document.createElement('canvas');

    var width = image.width;
    var height = image.height;

    if (width > height) {
        if (width > maxWidth) {
            height = Math.round(height * maxWidth / width);
            width = maxWidth;
        }
    } else {
        if (height > maxHeight) {
            width = Math.round(width * maxHeight / height);
            height = maxHeight;
        }
    }

    canvas.width = width;
    canvas.height = height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(image, 0, 0, width, height);
    return canvas.toDataURL("image/jpeg", quality);
}

export default class EditorImagem extends Component {
    constructor(props) {
        super(props);

        this.state = { novo: "" }
        this.updadeItem = this.updadeItem.bind(this);
        this.processaImagem = this.processaImagem.bind(this);
        this.addFoto = this.addFoto.bind(this);
        // this.onDrop = this.onDrop.bind(this);

    }

    // updateItem(obj) {

    //     this.dialog.hide();
    // }



    onDrop(files) {
        this.setState({
            files
        });
        files.forEach(file => {
            const reader = new FileReader();
            reader.onload = (event) => {
                console.log("foto",event.target.result);
                this.processaImagem(event.target.result, this.addFoto)
            };
            reader.readAsDataURL(file);
        });
        this.dialogCrud.hide();
    }

    rotateBase64Image(item, updadeItem, direita) {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");

        var image = new Image();
        image.src = "data:image/png;base64," + item.foto;

        image.onload = function () {
            canvas.width = image.height;
            canvas.height = image.width;
            if (direita) {
                ctx.rotate(90 * Math.PI / 180);
                ctx.translate(0, -canvas.width);
            } else {
                ctx.rotate(-90 * Math.PI / 180);
                ctx.translate(-canvas.height, 0);
            }
            ctx.drawImage(image, 0, 0);
            let base64Rotate = canvas.toDataURL();
            base64Rotate = base64Rotate.substring(base64Rotate.indexOf(",") + 1, base64Rotate.length)
            item.foto = base64Rotate
            updadeItem(item);
        };

    }

    processaImagem(data, funcaoAdd) {
        var image = new Image();
        image.src = data;
        image.onload = function () {
            var resizedDataUrl = resizeImage2(image, 480, 480, 0.7);
            // console.log('', );
            funcaoAdd(resizedDataUrl);
        };
    }

    addFoto(data) {
        var res = data.split(";base64,");
        let base64 = res[1];
        const foto = { foto: base64, galeria: true };
        this.updadeItem(foto);
    }

    showAdd() {
        this.dialog.show();
    }

    updadeItem(obj) {
        // const { index } = obj;

        let foto = obj;
        // var newArray = []
        // if (fotos) {
        //     newArray = fotos.slice();
        // }
        // if (index != null) {

        //     newArray[index] = obj;
        // } else {
        //     newArray.push(obj);
        // }
        this.updateValue(foto)
    }

    updateValue(value) {
        this.props.onChange(value);
    }

    startRemover(item) {
        console.log('startRemover ', item);

        this.setState({ editando: item }, () => {
            this.dialogConfirma.show();
        });
    }

    remover() {
        const remover = this.state.editando;
        // console.log('remover item', remover);
        let { value } = this.props.input;
        var newArray = []
        if (value) {
            newArray = value.slice();
        }

        var index = remover.index;
        // console.log('remover index', index);

        if (index > -1) {
            newArray.splice(index, 1);
        }
        this.updateValue(newArray)

    }

    render() {

        const { value, onChange } = this.props;
        // const columns = [
        //     { id: 'Valor', numeric: false, disablePadding: false, label: 'Valor', getValue: (item) => item },
        // ];
        // console.log('Value ', value);
        return (
            <View>
                <TextField
                    disabled={true}
                    value={value.foto}
                    placeholder={'Selecionar imagem'}
                    fullWidth
                />
                <Button onClick={() => {
                    this.dialogCrud.show()
                }}>
                    <MoreHoriz />
                </Button>
                <DialogSeletor innerRef={node => this.dialogCrud = node}>
                    <div style={{padding: 50}}>
                        <Dropzone
                            onDrop={this.onDrop.bind(this)}
                            accept="image/jpeg, image/png"
                            style={{
                                width: 350, height: 150,
                                borderWidth: 2, borderStyle: 'dashed',
                                borderRadius: 5, textAlign: 'center',
                                paddingTop: 50,
                            }}>
                            {({ getRootProps, getInputProps }) => (
                                <section>
                                    <View column {...getRootProps()} style={{
                                        height: '100%',
                                        alignItems: "center", justifyContent: 'center'

                                    }}>
                                        <input {...getInputProps()} />
                                        <p>Arraste a foto para este local.</p>
                                        <Button variant="contained" color="primary" >Selecionar</Button>
                                    </View>
                                </section>
                            )}


                            {/* <p>Arraste as fotos para este local.</p>
                            <Button variant="contained" color="primary">Selecionar</Button> */}

                        </Dropzone>
                    </div>
                </DialogSeletor>
                <DialogConfirma innerRef={node => this.dialogConfirma = node} titulo={"Remover"} mensagem={"Deseja mesmo remover esta foto?"} onConfirm={() => { this.remover() }} />
            </View>

        )
    }
}
