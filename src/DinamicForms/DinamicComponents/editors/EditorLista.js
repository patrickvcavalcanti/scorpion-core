import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import MoreHoriz from '@material-ui/icons/MoreHoriz';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';

import View from 'react-flexbox'
import { DialogSeletor, Item, DialogPesquisa } from '../../../components';


export default class EditorLista extends Component {
    constructor(props) {
        super(props);

        this.state = { novo: "" }
    }
    updateItem(obj) {

        this.dialog.hide();
    }

    sobe(indexAtual) {
        const { value, onChange, label } = this.props
        if (indexAtual > 0) {

            let newArray = this.array_move(value, indexAtual, indexAtual - 1);
            onChange(newArray)
        }
    }

    desce(indexAtual) {
        const { value, onChange } = this.props
        if (indexAtual < value.length - 1) {
            let newArray = this.array_move(value, indexAtual, indexAtual + 1);
            onChange(newArray)
        }

    }
    sobeNoPercore(no, pai) {
        let indexAtual = -1
        if (pai.componentes) {
            pai.componentes.forEach((f, index) => {
                if (f.sha === no.sha) {
                    indexAtual = index;
                } else {
                    if (indexAtual === -1) {
                        let novoFilho = this.sobeNoPercore(no, f);
                        if (novoFilho) {
                            const data = { ...pai }
                            let newArray = data.componentes.slice();
                            newArray[index] = novoFilho

                            return { ...pai, componentes: newArray };
                        }
                    }

                }
            })
            if (indexAtual > 0) {
                const data = { ...pai }
                let newArray = this.array_move(data.componentes, indexAtual, indexAtual - 1);

                return { ...pai, componentes: newArray };
            }
        }
        return null;

    }
    desceNoPercore(no, pai) {
        let indexAtual = -1
        if (pai.componentes) {
            pai.componentes.forEach((f, index) => {
                if (f.sha === no.sha) {
                    indexAtual = index;
                } else {
                    if (indexAtual === -1) {
                        let novoFilho = this.sobeNoPercore(no, f);
                        if (novoFilho) {
                            const data = { ...pai }
                            let newArray = data.componentes.slice();
                            newArray[index] = novoFilho

                            return { ...pai, componentes: newArray };
                        }
                    }

                }
            })
            if (indexAtual < pai.componentes.length - 1) {
                const data = { ...pai }
                let newArray = this.array_move(data.componentes, indexAtual, indexAtual + 1);

                return { ...pai, componentes: newArray };
            }
        }
        return null;

    }
    array_move(arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    };


    render() {

        const { value, onChange,label } = this.props
        const columns = [
            { id: 'Valor', numeric: false, disablePadding: false, label: 'Valor', getValue: (item) => item },
        ];

        return (
            <View    >
                <TextField
                 style={{ marginBottom: 15 }}
                    label={label}
                    disabled={true}
                    value={value}
                    InputLabelProps={{
                        shrink: true,
                      }}
                    fullWidth
                />
                <Button variant="contained" color="primary" onClick={() => this.dialogCrud.show()}>
                    <MoreHoriz />
                </Button>
                <DialogPesquisa forwardedRef={node => this.dialogCrud = node} notClose>
                    <View column style={{ padding: 5 }}>
                        <View column style={{ flex: "0 0 auto" }}>
                            <div style={{ margin: 5, display: 'inherit', alignItems: 'center' }}>
                                <TextField
                                    label={"Novo"}
                                    value={this.state.novo}
                                    onChange={event => this.setState({ novo: event.target.value })}

                                    fullWidth
                                />
                                <Button variant="contained" color="primary" size="small" onClick={() => {
                                    let newArr = value.slice();
                                    newArr.push(this.state.novo.trim())
                                    onChange(newArr)
                                    this.setState({ novo: "" })
                                }}>
                                    ADD
                            </Button>
                            </div>
                            <Typography variant="h6">Valores</Typography>
                            <Divider />
                        </View>
                        <View column style={{ justifyContent: 'flex-start', flex: 1, overflow: 'auto', height: 1 }}>


                            {value.map((item, index) => (
                                // <View key={index} style={{ margin: 5,  alignItems: 'center', height:1  }}>
                                <div key={index} style={{ display: 'flex', margin: 5, minHeight: 38 }} >

                                    <Typography style={{ flex: 1 }}>{item}</Typography>
                                    <Button variant="contained" size="small" onClick={() => {
                                        var newArray = value.slice();
                                        newArray.splice(index, 1);
                                        onChange(newArray)
                                    }}>
                                        Remover
                                    </Button>
                                    <Button size="small" onClick={() => this.sobe(index)}>
                                        <KeyboardArrowUp />
                                    </Button>
                                    <Button size="small" onClick={() => this.desce(index)}>
                                        <KeyboardArrowDown />
                                    </Button>

                                </div>

                                // </View>
                            ))}

                        </View>

                    </View>
                </DialogPesquisa>

            </View>

        )
    }
}
