export function required(value) {
  return value ? undefined : 'Requerido'
}

export function getLabel(customForm, field) {
  return getLabelComps(customForm.componenteRoot.componentes,field);

}


export function getLabelComps(componentes, field) {
  let retorno = null;
  componentes.forEach(element => {
    if (element.field === field) {
      retorno = element.label;
    }
    if (element.componentes && !retorno) {
      let r = getLabelComps(element.componentes, field)
      if (r) {
        retorno = r;
      }
    }
  });


  return retorno;

}