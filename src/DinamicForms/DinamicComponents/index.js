
export { componentesConhecidos as DinamicComponentes, addComponente, getCompRelEspelho, getComponenteView, getDinamicComponente, } from './ArrayComponentes'
export { default as EditorLista } from './editors/EditorLista'
export { default as EditorCodigo } from './editors/EditorCodigo'
