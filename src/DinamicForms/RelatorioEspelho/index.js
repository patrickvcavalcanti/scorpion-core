import { getCompRelEspelho } from '../DinamicComponents';
import { painel, OkReportGerador } from '../../OkReport';

export function gerarRelatorioEspelho(customForm, dados, dadosCabecalho) {
    const { componentes } = customForm.componenteRoot
    let conteudo = [];
    componentes.forEach(c => {
        conteudo.push(getCompRel(c, dados || {}))
    });
    let nomeRel = "Relatório de " + (customForm.nomeForm || "Espelho")
    OkReportGerador.gerarRelatorioImprimir({ nome: nomeRel }, conteudo, {}, nomeRel)
}

function getCompRel(comp, dados) {
    let c = getCompRelEspelho(comp.typeComponente);
    if (c) {
        return c(comp, dados, getCompRel)
    }
    return painel([
        [{ style: "cellTitulo", text: comp.label }], [{ style: "cellValor", text: dados[comp.field] }]
    ], { widths: ['*'] })
}
