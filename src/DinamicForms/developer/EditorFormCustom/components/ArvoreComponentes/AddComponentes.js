import React, { Component } from 'react'
import { DinamicComponentes } from '../../../../DinamicComponents';
import produce from "immer"

import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

export default class AddComponents extends Component {

    constructor(props) {
        super(props)
        this.state = { anchorEl: null }
    }

    show = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };
    hide = () => {
        this.setState({ anchorEl: null });
    };

    renderItem(c, index) {
        const { noPaiUid, componenteRoot } = this.props;


        return <MenuItem key={index} onClick={() => {
            this.hide();
            let add = { ...c.modelo, uid: Math.random().toString(36).substring(7) };
            addUidEmTudo(add)

            const nextState = produce(componenteRoot, draftState => {
                if (draftState.uid === noPaiUid) {
                    draftState.componentes.push(add);
                } else {

                    let itemSelecionado = this.getNo(draftState.componentes, noPaiUid)
                    itemSelecionado.componentes.push(add);
                }
            })
            this.props.onChange(nextState);
            this.props.selecionouComponente(add.uid);

        }}>{c.nome}</MenuItem>
    }

    getNo(componentes, uid) {
        if (!uid) {
            return null
        }

        for (var i = 0; i < componentes.length; i++) {
            let c = componentes[i];
            if (c.uid === uid) {
                return c;
            } else {
                if (c.componentes) {
                    let r = this.getNo(c.componentes, uid)
                    if (r) {
                        return r;
                    }

                }

            }
        }

        return null
    }

    render() {
        const { anchorEl } = this.state
        let comps = DinamicComponentes.map(c => c.configView).sort((a, b) => a.nome > b.nome ? 1 : a.nome < b.nome ? -1 : 0);       
        return (
            <div>
                <IconButton onClick={this.show}> <AddIcon /> </IconButton>

                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.hide}
                >
                    {comps.map((c, index) => this.renderItem(c, index))}
                </Menu>

            </div>
        )
    }


}

var arrayConstructor = [].constructor;
var objectConstructor = {}.constructor;
const addUidEmTudo = (json) => {
    if (json && json.constructor === objectConstructor)
        for (var prop in json) {
            if (prop === "typeComponente") {
                if (!json.uid) {
                    json.uid = Math.random().toString(36).substring(7);
                }
            }
            let v = json[prop];
            if (v)
                if (v.constructor === arrayConstructor) {
                    v.forEach(vi => addUidEmTudo(vi))
                } else {
                    addUidEmTudo(v)
                }
        }
}

