import React, { Component } from 'react'


import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import MenuIcon from "@material-ui/icons/Menu";
import EditIcon from "@material-ui/icons/Edit";
import { getTreeLeafDataByIndexArray } from "material-ui-tree";

import produce from "immer"
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import TreeComponent from '../TreeComponent';
import AddComponents from './AddComponentes';
import MoveComponentes from './MoveComponentes';
import { DialogPesquisa } from '../../../../../components';
import DialogAddComponente from './DialogAddComponente';

import IconButton from '@material-ui/core/IconButton';
import DialogEditaComp from './DialogEditaComp';

class ArrayComponentes extends Component {
    constructor(props) {
        super(props);
        this.getNo = this.getNo.bind(this)
        this.startEditar = this.startEditar.bind(this)
        this.state = {
            selecionado: "",
            noPaiUid: null,
            compEditar: null,


        }
    }

    static propTypes = {
        classes: PropTypes.object.isRequired
    };


    handleClick2 = (al, nodeAdd2) => {
        this.setState({ showMenu2: !this.state.showMenu2, anchorEl2: al, nodeAdd2 });
    };

    handleClose2 = () => {
        this.setState({ showMenu2: false });
    };



    renderTreeLeafLabel = (leafData, expand) => {
        const { label } = leafData;
        if (!leafData.uid && leafData.sha) {
            leafData.uid = leafData.sha
        }
        return (
            <Button viriant="body1" style={{ justifyContent: "left", flex: 1, fontSize: 12, width: '100%', paddingLeft: 2, paddingRight: 5 }} onClick={() => {
                this.setState({ selecionado: leafData.uid })
                this.props.selecionouComponente(leafData.uid)
            }}>
                {label ? label : "-"}
            </Button>
        );
    };

    getItem(sha, listaComponente) {
        if (!sha) {
            return null
        }
        for (var i = 0; i < listaComponente.length; i++) {
            let c = listaComponente[i];
            if (c.sha === sha) {
                return c;
            } else {
                if (c.componentes) {
                    let r = this.getItem(sha, c.componentes)
                    if (r) {
                        return r;
                    }
                }
            }
        }
        return null
    }


    getTreeLeafActionsData = (leafData, chdIndex, expand) => {
        const { componenteRoot } = this.props

        const { componentes } = leafData;
        let noPaiUid = leafData.uid

        // const AddComponente = <AddComponents noPaiUid={noPaiUid} componenteRoot={componenteRoot}
        //     selecionouComponente={this.props.selecionouComponente}
        //     onChange={this.props.onChange}
        // />
        const AddComponente = <IconButton onClick={() => {
            this.setState({ noPaiUid }, this.dialog.show)

        }}> <AddIcon /> </IconButton>
        compEditar

        const EditaComponente = {
            icon: <EditIcon color="secondary" />,
            hint: "Editar",
            onClick: () => {

                this.setState({ compEditar: leafData }, this.dialogEditar.show)
            }
        }
        const RemoveComponente = {
            icon: <DeleteIcon color="secondary" />,
            hint: "Remover",
            onClick: () => {
                const { componenteRoot } = this.props;
                const nextState = produce(componenteRoot, draftState => {
                    let r = this.getPaiEIndex(draftState, noPaiUid)
                    if (r) {
                        let p = r[0]
                        let index = r[1]
                        p.componentes.splice(index, 1);

                    }
                })
                this.props.onChange(nextState);
            }
        }


        const MoveComponente = <MoveComponentes noPaiUid={noPaiUid} componenteRoot={componenteRoot}
            selecionouComponente={this.props.selecionouComponente}
            onChange={this.props.onChange}
        />

        if (noPaiUid === componenteRoot.uid) {//eh root
            return [AddComponente];

        } else {
            if (componentes) {
                return [AddComponente, RemoveComponente, EditaComponente, MoveComponente];
            }
            return [RemoveComponente, EditaComponente, MoveComponente];
        }
    };



    getNo(componentes, uid) {
        if (!uid) {
            return null
        }
        for (var i = 0; i < componentes.length; i++) {
            let c = componentes[i];          


            if (c.uid === uid) {
                return c;
            } else {
                if (c.componentes) {
                    let r = this.getNo(c.componentes, uid)
                    if (r) {
                        return r;
                    }

                }

            }
        }

        return null
    }

    getPaiEIndex(CompPai, uid) {
        if (!uid) {
            return null
        }
        for (var i = 0; i < CompPai.componentes.length; i++) {
            let c = CompPai.componentes[i];
            if (c.uid === uid) {
                return [CompPai, i];
            } else {
                if (c.componentes) {
                    let r = this.getPaiEIndex(c, uid)
                    if (r) {
                        return r;
                    }

                }

            }
        }

        return null
    }

    startEditar(comp) {
        console.log('startEditar',comp);
        
        this.setState({ compEditar: comp }, this.dialogEditar.show)
    }

    render() {
        const { componenteRoot, selecionouComponente, onChange, titulo } = this.props
        const { classes } = this.props;
        const { noPaiUid, compEditar } = this.state;


        return (<div>
            <TreeComponent
                style={{ flex: 1, overflow: "auto", }}
                actionsAlignRight={true}
                title={titulo}
                data={componenteRoot}
                labelName="label"
                valueName="uid"
                childrenName="componentes"
                renderLabel={this.renderTreeLeafLabel}
                getActionsData={this.getTreeLeafActionsData}
                expandAll={false}
                classes={classes}
            />
            <DialogPesquisa forwardedRef={node => this.dialog = node}>
                <DialogAddComponente
                    noPaiUid={noPaiUid}
                    componenteRoot={componenteRoot}
                    selecionouComponente={selecionouComponente}
                    startEditar={this.startEditar}
                    onChange={onChange}
                />
            </DialogPesquisa>
            <DialogPesquisa forwardedRef={node => this.dialogEditar = node}>
                <DialogEditaComp
                    compEditar={compEditar}
                    componenteRoot={componenteRoot}
                    saveitem={onChange}
                />
            </DialogPesquisa>

        </div>
        );
    }
}


const styles = () => ({
});

export default withStyles(styles, { withTheme: true })(ArrayComponentes);
