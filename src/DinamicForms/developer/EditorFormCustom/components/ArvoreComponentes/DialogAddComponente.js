import React, { Component } from 'react'
import { DinamicComponentes } from '../../../../DinamicComponents';
import produce from "immer"
import View from 'react-flexbox'
import { Card, Button, Divider, Typography, TextField } from '@material-ui/core';
export default class DialogAddComponente extends Component {

    constructor(props) {
        super(props)
        this.renderItemDefault = this.renderItemDefault.bind(this)
        this.addComponente = this.addComponente.bind(this)
        this.getNo = this.getNo.bind(this)
        this.filtra = this.filtra.bind(this)
        this.state = { txtFiltro: '' }
    }
    addComponente(c) {
        const { noPaiUid, componenteRoot } = this.props;
        let add = { ...c.modelo, uid: Math.random().toString(36).substring(7) };
        addUidEmTudo(add)

        const nextState = produce(componenteRoot, draftState => {
            if (draftState.uid === noPaiUid) {
                draftState.componentes.push(add);
            } else {

                let itemSelecionado = this.getNo(draftState.componentes, noPaiUid)
                itemSelecionado.componentes.push(add);
            }
        })
        this.props.onChange(nextState);
        this.props.selecionouComponente(add.uid);
        this.props.startEditar(add);
        this.props.actionclose();


    }
    getNo(componentes, uid) {
        if (!uid) {
            return null
        }
        for (var i = 0; i < componentes.length; i++) {
            let c = componentes[i];
            


            if (c.uid === uid) {
                return c;
            } else {
                if (c.componentes) {
                    let r = this.getNo(c.componentes, uid)
                    if (r) {
                        return r;
                    }

                }

            }
        }

        return null
    }
    filtra() {
        let comps = DinamicComponentes.map(c => c.configView).sort((a, b) => a.nome > b.nome ? 1 : a.nome < b.nome ? -1 : 0);


        return comps.filter(item => {

            let b1 = item.nome.toUpperCase().includes(this.state.txtFiltro.toUpperCase())
            if (!item.descricao) {
                return b1;
            }
            let b2 = item.descricao.toUpperCase().includes(this.state.txtFiltro.toUpperCase())
            return b1 || b2;



        });
    }
    renderItemDefault(item, index) {
        
        let dados = <div>
            <Typography variant="h6" > {item.nome}</Typography>
            {item.descricao && <Typography variant="caption" > {item.descricao}</Typography>}
        </div>
        if (item.renderView) {
            dados = item.renderView();
        }

        return <Card key={index} style={{ padding: 10, marginBottom: 3 }}>
            <View style={{ justifyContent: "flex-start" }}>
                <View style={{ alignItems: "center", flex: "0 0 auto" }}>
                    <div>
                        <Button variant="contained" color="primary" size="small"
                            onClick={() => this.addComponente(item)}>Adicionar</Button>
                    </div>
                </View>
                <View column
                    style={{ padding: 3, borderLeftStyle: 'groove', marginLeft: 10 }}
                >
                    {dados}
                </View>


            </View>
        </Card>
        // return <div key={index}>{c.nome}</div>
    }

    renderFiltro() {
        const { txtFiltro } = this.state
        return <div >
            <div style={{ backgroundColor: "#000", height: 50, }}> < Typography variant="h5" style={{ padding: 8, color: "#fff" }}>Componentes</Typography></div>
            <div style={{ margin: 5, }}>
                <Typography>Filtro</Typography>
                <TextField
                    inputRef={input => {
                        if (input)
                            input.focus();
                    }}
                    value={txtFiltro}
                    onChange={event => this.setState({ txtFiltro: event.target.value })}
                    fullWidth
                />
            </div>
        </div>
    }

    render() {
        

        let comps = this.filtra();
        

        return (
            <View column style={{ justifyContent: "flex-start" }}>
                {this.renderFiltro()}
                <div style={{
                    padding: 5, backgroundColor: "#CCC", height: "100%", maxHeight: "100%",
                    overflow: 'scroll'
                }}>
                    {comps.map((item, index) => {
                        return this.renderItemDefault(item, index)
                    })}
                </div>
            </View>
        )
    }
}

var arrayConstructor = [].constructor;
var objectConstructor = {}.constructor;
const addUidEmTudo = (json) => {
    if (json && json.constructor === objectConstructor)
        for (var prop in json) {
            if (prop === "typeComponente") {
                if (!json.uid) {
                    json.uid = Math.random().toString(36).substring(7);
                }
            }
            let v = json[prop];
            if (v)
                if (v.constructor === arrayConstructor) {
                    v.forEach(vi => addUidEmTudo(vi))
                } else {
                    addUidEmTudo(v)
                }
        }
}

