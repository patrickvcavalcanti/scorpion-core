import React, { Component } from 'react'

import { reduxForm } from 'redux-form';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { DinamicComponentes, getDinamicComponente } from '../../../../DinamicComponents';
import produce from "immer"
import View from 'react-flexbox'
import { Card, Button, Divider, Typography, TextField } from '@material-ui/core';
import ItemCustomRender from '../../../RenderCustomForm/components/ItemCustomRender/ItemCustomRender';
import RenderCustomForm from '../../../RenderCustomForm';

class DialogEditaComp extends Component {

    constructor(props) {
        super(props)
        this.salvar = this.salvar.bind(this)
        this.state = { formRender: null }
    }
    getNo(componentes, uid) {
        if (!uid) {
            return null
        }
        for (var i = 0; i < componentes.length; i++) {
            let c = componentes[i];



            if (c.uid === uid) {
                return c;
            } else {
                if (c.componentes) {
                    let r = this.getNo(c.componentes, uid)
                    if (r) {
                        return r;
                    }

                }

            }
        }

        return null
    }
    salvar(props) {
        const { componenteRoot, compEditar } = this.props
        const nextState = produce(componenteRoot, draftState => {
            let itemSelecionado = this.getNo(draftState.componentes, compEditar.uid)
            for (var p in props) {
                itemSelecionado[p] = props[p]
            }
        })

        this.props.saveitem(nextState);
        this.props.actionclose()
    }

    editouProp(prop, value) {
        const { componenteRoot } = this.props
        const nextState = produce(componenteRoot, draftState => {
            let itemSelecionado = this.getNo(draftState.componentes, this.props.itemSelecionadoUid)
            itemSelecionado[prop] = value;
        })
        this.props.onChange(nextState);
    }
    componentDidMount() {
        const { compEditar } = this.props

        let dComp = getDinamicComponente(compEditar.typeComponente);
        let propsEditar = null;
        if (dComp) {
            if (dComp.propsField) {
                propsEditar = dComp.propsField.slice();
            }
        }
        if (propsEditar) {

            this.setState({ formRender: <RenderCustomForm componenteRoot={{ componentes: propsEditar }} /> })
        } else {
            this.props.actionclose();
        }
    }
    renderFiltro() {
        const { txtFiltro } = this.state
        return <div >
            <div style={{ backgroundColor: "#000", height: 50, }}>
                <View>

                    < Typography variant="h5" style={{ padding: 8, color: "#fff" }}>Editar</Typography>
                    <div>
                        <View>

                            <Button variant="contained" color="secondary" size="small" onClick={this.props.handleSubmit(this.salvar)}>Salvar</Button>
                            <Button color="secondary" size="small" onClick={this.props.actionclose}>Sair</Button>
                        </View>
                    </div>
                </View>
            </div>
            <div style={{ margin: 5, }}>

            </div>
        </div>
    }


    render() {
        const { compEditar } = this.props
        const { formRender } = this.state
        let dComp = getDinamicComponente(compEditar.typeComponente);
        let propsEditar = null;
        if (dComp) {
            if (dComp.propsField) {
                propsEditar = [];
                for (var prop in dComp.propsField) {

                    propsEditar.push({ field: prop, label: prop, typeComponente: dComp.propsField[prop], })

                }
            }
        }

        return (
            <View column style={{ justifyContent: "flex-start" }}>
                {this.renderFiltro()}
                <div>
                    {!propsEditar && <div>Sem propsField</div>}
                    {/* {propsEditar && <RenderCustomForm componenteRoot={{ componentes: propsEditar }} />} */}
                    {formRender}
                </div>
            </View>
        )
    }
}

const mapStateToProps = (state, props) => {
    let initialValues = props.compEditar;

    return { initialValues }
}


export default compose(
    connect(mapStateToProps),
    reduxForm({ form: "EditaCompTeste" }),
)(DialogEditaComp);