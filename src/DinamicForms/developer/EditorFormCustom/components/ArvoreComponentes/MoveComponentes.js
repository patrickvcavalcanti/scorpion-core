import React, { Component } from 'react'

import produce from "immer"

import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import MenuIcon from "@material-ui/icons/Menu";
import { Collapse, Divider } from '@material-ui/core';

export default class MoveComponentes extends Component {
    constructor(props) {
        super(props)
        this.state = { anchorEl: null }
    }

    show = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };
    hide = () => {
        this.setState({ anchorEl: null });
    };

    getPaiEIndex(CompPai, uid) {
        if (!uid) {
            return null
        }
        for (var i = 0; i < CompPai.componentes.length; i++) {
            let c = CompPai.componentes[i];
            if (c.uid === uid) {
                return [CompPai, i];
            } else {
                if (c.componentes) {
                    let r = this.getPaiEIndex(c, uid)
                    if (r) {
                        return r;
                    }

                }

            }
        }

        return null
    }

    inserirEm(uidInserir) {
        const { noPaiUid, componenteRoot } = this.props;
        const nextState = produce(componenteRoot, draftState => {
            let r = this.getPaiEIndex(draftState, noPaiUid)
            let rVai = this.getPaiEIndex(draftState, uidInserir)
            if (r && rVai) {
                let pai = r[0]
                let index = r[1]
                let comp = pai.componentes[index];
                pai.componentes.splice(index, 1);//remove do pai
                let compVai = rVai[0].componentes[rVai[1]];
                compVai.componentes.push(comp)
            }
        })
        this.props.onChange(nextState);
    }
    extrairDoPai() {
        const { noPaiUid, componenteRoot } = this.props;
        const nextState = produce(componenteRoot, draftState => {
            let r = this.getPaiEIndex(draftState, noPaiUid)
            if (r) {
                let pai = r[0]
                let index = r[1]
                let comp = pai.componentes[index];

                let r2 = this.getPaiEIndex(draftState, pai.uid)
                if (r2) {
                    pai.componentes.splice(index, 1);//remove do pai
                    let paiDoPai = r2[0]
                    paiDoPai.componentes.push(comp)

                }


            }
        })
        this.props.onChange(nextState);
    }

    movePara(indexPara) {
        const { noPaiUid, componenteRoot } = this.props;
        const nextState = produce(componenteRoot, draftState => {
            let r = this.getPaiEIndex(draftState, noPaiUid)
            if (r) {
                let p = r[0]
                let index = r[1]
                if (index > 0 && indexPara < p.componentes.length - 1) {
                    this.array_move(p.componentes, index, indexPara);
                }
            }
        })
        this.props.onChange(nextState);
    }


    sobeNo() {
        const { noPaiUid, componenteRoot } = this.props;
        const nextState = produce(componenteRoot, draftState => {
            let r = this.getPaiEIndex(draftState, noPaiUid)
            if (r) {
                let p = r[0]
                let index = r[1]
                if (index > 0) {
                    this.array_move(p.componentes, index, index - 1);
                }
            }
        })
        this.props.onChange(nextState);
    }
    desceNo() {
        const { noPaiUid, componenteRoot } = this.props;
        const nextState = produce(componenteRoot, draftState => {
            let r = this.getPaiEIndex(draftState, noPaiUid)
            if (r) {
                let p = r[0]
                let index = r[1]
                if (index < p.componentes.length - 1) {
                    this.array_move(p.componentes, index, index + 1);
                }
            }
        })
        this.props.onChange(nextState);
    }

    array_move(arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    };




    render() {
        const { anchorEl } = this.state
        const { noPaiUid, componenteRoot } = this.props;
        let r = this.getPaiEIndex(componenteRoot, noPaiUid)
        let irmaosContainers = [];
        if (r) {
            let paiTeste = r[0]
            if (paiTeste && paiTeste.componentes) {
                paiTeste.componentes.forEach((c, index) => {
                    if (c.componentes) {

                        irmaosContainers.push(<MenuItem key={index} onClick={() => {
                            this.inserirEm(c.uid);
                            this.hide();
                        }}>Inserir em: {c.label}</MenuItem>)
                    }
                })
            }

        }

        return (
            <div>
                <IconButton onClick={this.show}> <MenuIcon /> </IconButton>

                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.hide}
                >
                    <MenuItem onClick={() => {
                        this.sobeNo();
                        this.hide();
                    }}>Sobe</MenuItem>
                    <MenuItem onClick={() => {
                        this.desceNo();
                        this.hide();
                    }}>Desce</MenuItem>
                    <MenuItem onClick={() => {
                        this.movePara(0);
                        this.hide();
                    }}>Inicio</MenuItem>
                    <MenuItem onClick={() => {
                        this.extrairDoPai();
                        this.hide();
                    }}>ExtraiDoPainel</MenuItem>
                    <Divider />
                    {irmaosContainers}
                </Menu>


            </div>
        )
    }


}

var arrayConstructor = [].constructor;
var objectConstructor = {}.constructor;
const addUidEmTudo = (json) => {
    if (json && json.constructor === objectConstructor)
        for (var prop in json) {
            if (prop === "typeComponente") {
                if (!json.uid) {
                    json.uid = Math.random().toString(36).substring(7);
                }
            }
            let v = json[prop];
            if (v)
                if (v.constructor === arrayConstructor) {
                    v.forEach(vi => addUidEmTudo(vi))
                } else {
                    addUidEmTudo(v)
                }
        }
}

