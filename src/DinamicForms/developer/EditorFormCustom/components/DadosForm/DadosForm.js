import React, { Component } from 'react'

import { Field } from 'redux-form';

import Button from '@material-ui/core/Button';
import { Item, Input } from '../../../../../components';

import { required } from '../../../../DinamicComponents/function'
import { gerarRelatorioEspelho } from '../../../../RelatorioEspelho';


export default class DadosForm extends Component {
    duplicar(atual) {
        let novo = { componenteRoot: atual.componenteRoot, chaveSalvar: atual.chaveSalvar, permissao: atual.permissao }
        this.props.initialize(novo);
    }

    render() {

        return (
            <div>
                <Item style={{ flex: 1 }}>
                    <Field name="uid" placeholder="UID" component={Input} disabled={true} />
                    {this.props.atualValues && this.props.atualValues.uid && <Button onClick={() => this.duplicar(this.props.atualValues)}>Duplicar</Button>}
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field name="nomeForm" placeholder="Nome Formulário" component={Input} validate={[required]} />
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field name="descricao" placeholder="Descrição" component={Input} validate={[required]} />
                </Item>

                <Item style={{ flex: 1 }}>
                    {this.props.atualValues && <Button variant="contained" color="primary" onClick={() => gerarRelatorioEspelho(this.props.atualValues, this.props.atualValues.DebugValues)}>Gerar Espelho</Button>}
                </Item>


            </div>
        )
    }
}
