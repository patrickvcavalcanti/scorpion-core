import React, { Component } from 'react'
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

import { Field } from 'redux-form';

import View from 'react-flexbox'

import ArvoreComponentes from './ArvoreComponentes/ArvoreComponentes';
import Propriedades from './Propriedades/Propriedades';
import DadosForm from './DadosForm';

import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
class EditorFormCustom extends Component {
    constructor(props) {
        super(props);
        this.state = { selecionado: "" }
    }

    static propTypes = {
        classes: PropTypes.object.isRequired
    };



    getItem(sha, listaComponente) {

        if (!sha) {
            return null
        }


        for (var i = 0; i < listaComponente.length; i++) {
            let c = listaComponente[i];
            if (c.sha === sha) {
                return c;
            } else {
                if (c.componentes) {
                    let r = this.getItem(sha, c.componentes)
                    if (r) {
                        return r;
                    }

                }

            }
        }

        return null


    }


    render() {
        const { initialize, input: { value, onChange }, atualValues } = this.props

        if (!value) {
            return <div>Valor invalido para "componenteRoot"</div>
        }

        return (<View column>

            <DadosForm initialize={initialize} atualValues={atualValues} />
            {/* <ArvoreComponentes componenteRoot={value} onChange={onChange} selecionouComponente={(c) => {
                this.setState({ selecionado: c })
            }} /> */}

            <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h6" >Árvore de Componentes</Typography>
                </ExpansionPanelSummary >
                <ExpansionPanelDetails style={{ flex: 1, padding: 0 }}>
                    <div style={{ flex: 1, }}>
                        <ArvoreComponentes componenteRoot={value} onChange={onChange} selecionouComponente={(c) => {
                            this.setState({ selecionado: c })
                        }} />
                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>
            <Propriedades componenteRoot={value} onChange={onChange} itemSelecionadoUid={this.state.selecionado} />


        </View>

        );
    }
}



const styles = () => ({
    container: {

    },
    icon: {
        fontSize: 18
    },
    leaf: {
        padding: 0,
        fontSize: 12,
    },

});

export default withStyles(styles, { withTheme: true })(EditorFormCustom);
