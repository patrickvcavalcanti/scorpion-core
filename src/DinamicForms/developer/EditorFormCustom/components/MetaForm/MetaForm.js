import React, { Component } from 'react'

import { Field } from 'redux-form';
import { Item, SwitchItem, EditorCodeField, Input } from '../../../../../components';
// import ConvidarPerfil from '../../../../../ConsultaPerfisAdmin/componentes/ConvidarPerfil';


export default class MetaForm extends Component {
    render() {
        const { metasAdd } = this.props;
        return (
            <div>
                <Item style={{ flex: 1 }}>
                    <Field name="rota" placeholder="Rota do Formulario" component={Input} />
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field name="consultaPrimeiro" placeholder="Consulta Primeiro?" component={SwitchItem} />
                </Item>

                <Item style={{ flex: 1 }}>
                    <Field style={{ marginRight: 5 }} name="menu.nome" placeholder="Texto menu" component={Input} />
                    <Field name="iconeStr" placeholder="Ícone Menu" component={Input} />
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field name="menu.ordem" placeholder="Ordem menu" component={Input} />
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field style={{ marginRight: 5 }} name="menu.subDe" placeholder="Submenu De" component={Input} />
                    <Field name="iconePaiStr" placeholder="Ícone Pai" component={Input} />
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field name="menu.ordemPai" placeholder="Ordem do pai" component={Input} />
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field name="scrollInterno" placeholder="Scroll em Componente Interno" component={SwitchItem} />
                </Item>
                <Item style={{ flex: 1 }}>
                    <Field name="chaveSalvar" placeholder="Chave Salvar (VAZIO p/ padrão)(padrão é o id so usuário)" component={Input} />
                </Item>

                {metasAdd}

                <Item style={{ flex: 1, height: 500 }}>
                    <Field name="aoIniciarNovo" label="Função ao Novo" component={EditorCodeField} start={`function aoIniciarNovo(obj,props){}`} />
                </Item>
                <Item style={{ flex: 1, height: 500 }}>
                    <Field name="antesDeSalvar" label="Função antes de Salvar" component={EditorCodeField} start={`function antesDeSalvar(obj,props){}`} />
                </Item>


                {/* <ConvidarPerfil label="Users Editar" />
                <ConvidarPerfil label="Users Preenxer" name={"viewers"} />  */}



            </div>
        )
    }
}
