import React, { Component } from 'react'
import Typography from "@material-ui/core/Typography";
import View from 'react-flexbox'
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import produce from "immer"

// import EditorJson from '../../../../modules/ComponentsConhecidos/editors/EditorJson';

import { getEditores, getModelo } from '../../../../DinamicComponents/ArrayComponentes';
import PropsGlobal from './PropsGlobal';


var arrayConstructor = [].constructor;
var objectConstructor = {}.constructor;
var booleanConstructor = true.constructor;

export default class Propriedades extends Component {
    constructor(props) {
        super(props)
        this.editouProp = this.editouProp.bind(this);
    }

    editouProp(prop, value) {
        const { componenteRoot } = this.props
        const nextState = produce(componenteRoot, draftState => {
            let itemSelecionado = this.getNo(draftState.componentes, this.props.itemSelecionadoUid)
            itemSelecionado[prop] = value;
        })
        this.props.onChange(nextState);
    }

    renderItem(item, index, editores) {
        if (editores) {
            let editorUsar = editores[item.prop];
            if (editorUsar) {
                let elemento = React.createElement(editorUsar, {
                    value: item.value,
                    onChange: value1 => this.editouProp(item.prop, value1)
                })
                return (
                    <div key={index} style={{ margin: 5 }}>
                        <Typography >{item.prop}</Typography>
                        {elemento}
                    </div>
                )
            }
        }

        if (item.value && (item.value.constructor === objectConstructor || item.value.constructor === arrayConstructor)) {
            return (
                <div key={index} style={{ margin: 5 }}>
                    <Typography >{item.prop}</Typography>
                    {/* <EditorJson value={item.value} onChange={value => this.editouProp(item.prop, value)} /> */}

                </div>
            )
        }

        if (item.value !== undefined && typeof item.value == typeof true) {

            return (<div key={index} style={{ margin: 5 }}>
                <Typography >{item.prop}</Typography>
                <Switch checked={item.value} onChange={(event, b1) => this.editouProp(item.prop, b1)} />
            </div>
            )
        }
        return (
            <div key={index} style={{ margin: 5 }}>
                <Typography >{item.prop}</Typography>
                <TextField
                    multiline
                    value={item.value}
                    onChange={event => this.editouProp(item.prop, event.target.value)}
                    fullWidth
                />
            </div>
        )

    }


    getNo(componentes, uid) {
        if (!uid) {
            return null
        }

        for (var i = 0; i < componentes.length; i++) {
            let c = componentes[i];
            if (c.uid === uid) {
                return c;
            } else {
                if (c.componentes) {
                    let r = this.getNo(c.componentes, uid)
                    if (r) {
                        return r;
                    }
                }
            }
        }

        return null
    }

    render() {
        const { itemSelecionadoUid, componenteRoot } = this.props;

        let propriedades = [];
        let editores = {};
        let itemSelecionado = this.getNo(componenteRoot.componentes, itemSelecionadoUid)       

        if (itemSelecionado) {
            let modelo = getModelo(itemSelecionado.typeComponente);
            for (var prop in modelo) {
                if ((prop !== "sha") && (prop !== "componentes") && (prop !== "typeComponente")) {
                    let v = itemSelecionado[prop];
                    if (v === undefined) {
                        v = modelo[prop];
                    }
                    propriedades.push({ prop: prop, value: v })
                }
            }
            editores = getEditores(itemSelecionado.typeComponente);
            
        }

        return (
            <div style={{ flex: 1, }}>
               
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h6" >Propriedades: {itemSelecionado? itemSelecionado.typeComponente: ''}</Typography>
                    </ExpansionPanelSummary >
                    <ExpansionPanelDetails style={{ flex: 1, padding: 0 }}>
                        <div style={{ flex: 1, }}>
                        {propriedades.map((item, index) => this.renderItem(item, index, editores))}
                        </div>

                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography variant="h6" >Propriedades Global</Typography>
                    </ExpansionPanelSummary >
                    <ExpansionPanelDetails style={{ flex: 1, padding: 0 }}>
                        <div style={{ flex: 1, }}>
                            <PropsGlobal itemSelecionado={itemSelecionado} editouProp={this.editouProp} editores={editores} />
                        </div>

                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        )
    }
}
