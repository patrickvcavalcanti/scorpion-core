import React, { Component } from 'react'

import Typography from "@material-ui/core/Typography";
import View from 'react-flexbox'
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';

import produce from "immer"
import { EditorCodigo } from '../../../../DinamicComponents';


export default class PropsGlobal extends Component {
    render() {
        const { itemSelecionado } = this.props;
        if (!itemSelecionado) {
            return ""
        }

        return (
            <div>
                <div style={{ margin: 5 }}>
                    <Typography >Permissão Ver</Typography>
                    <TextField
                        multiline
                        value={itemSelecionado.permissao || ""}
                        onChange={event => this.props.editouProp("permissao", event.target.value)}
                        fullWidth
                    />
                </div>
                <div style={{ margin: 5 }}>
                    <Typography >Permissão Editar</Typography>
                    <TextField
                        multiline
                        value={itemSelecionado.permissao_editar || ""}
                        onChange={event => this.props.editouProp("permissao_editar", event.target.value)}
                        fullWidth
                    />
                </div>
                <div style={{ margin: 5 }}>
                    <Typography >Flex</Typography>
                    <TextField
                        type="number"
                        value={itemSelecionado.flex || ""}
                        onChange={event => this.props.editouProp("flex", event.target.value)}
                        fullWidth
                    />
                </div>
                <div style={{ margin: 5 }}>
                    <Typography >Coluna Pesquisa</Typography>
                    <Switch checked={itemSelecionado.colunaPesquisa} onChange={(event, b1) => this.props.editouProp("colunaPesquisa", b1)} />
                </div>
                <div style={{ margin: 5 }}>
                    <Typography >Filtro Pesquisa</Typography>
                    <Switch checked={itemSelecionado.filtroPesquisa} onChange={(event, b1) => this.props.editouProp("filtroPesquisa", b1)} />
                </div>
                <div style={{ margin: 5 }}>
                    <Typography >Override Props</Typography>
                    <EditorCodigo value={itemSelecionado.overrideProps || "function overrideProps(props) { return {} }"
                    } onChange={(v) => this.props.editouProp("overrideProps", v)} />
                </div>



            </div>
        )
    }
}
function overrideProps(props) {
    return {}
}