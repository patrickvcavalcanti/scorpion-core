import React, { Component } from 'react'

import { compose } from 'redux'
import { connect } from 'react-redux'

import { reduxForm, formValueSelector, getFormValues, formValues } from 'redux-form';

import View from 'react-flexbox'
import EditorFormCustom from '../EditorFormCustom';
import MetaForm from '../EditorFormCustom/components/MetaForm';
import RenderCustomForm from '../RenderCustomForm';
// import CrudCustomForm from '../CrudCustomForm';

import { Field } from 'redux-form';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';




class CustonFormDeveloperForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: 0,
            customForm: {},
        }
    }

    handleChange = (event, value) => {
        this.setState({ tab: value });
    };

    render() {
        const { initialize, customForm, atualValues, metasAdd } = this.props;
        const { tab } = this.state;
        let styleContainer = {};
        if (customForm &&customForm.scrollInterno)
            styleContainer = { height: "100%" }

        return (
            <View style={{ flexDirection: 'row', flex: 1 }}>
                <div style={{ width: 425, padding: 7 }}>
                    <Field name="componenteRoot" initialize={initialize} atualValues={atualValues} component={EditorFormCustom} />
                </div>
                <View column style={{ flex: 1, display: 'block' }}> 
                    <AppBar position="static">
                        <Tabs value={tab} onChange={this.handleChange}>
                            <Tab label="Tela" /> 
                            <Tab label="Meta" />
                        </Tabs>
                    </AppBar>
                    {tab === 0 && <Field name="componenteRoot" preField="valores." confrender={{ tipoDebug: true }} propsview={this.props} styleContainer={styleContainer} component={RenderCustomForm} />}
                    {tab === 1 && <MetaForm customForm={customForm} initialize={initialize} metasAdd={metasAdd} />}
                </View>

            </View >
        )
    }

}

const mapStateToProps = (state, props) => {
    let atualValues = getFormValues(props.form)(state)
    // console.log("atualValues", atualValues);

    return { atualValues };
};
export default compose(
    connect(mapStateToProps, {})
)(CustonFormDeveloperForm);