import React, { Component } from 'react'

import View from 'react-flexbox'
import ItemCustomRender from './components/ItemCustomRender/ItemCustomRender';
import { withStyles } from '@material-ui/core';
class RenderCustomForm extends Component {
    render() {
        const { styleContainer, componenteRoot, confrender, propsview, classes } = this.props;
        let listacomponentes = [];

        if (componenteRoot) {
            listacomponentes = componenteRoot.componentes;
        } else {
            if (this.props.input && this.props.input.value) {

                listacomponentes = this.props.input.value.componentes;
            }
        }
        let style = { flexDirection: 'column' }
        if (styleContainer) {
            style = { ...style, ...styleContainer }
        }
        
        
        return (
            <View style={style} className={classes.st}>
                {listacomponentes.map((item1, index1) => 
                <ItemCustomRender key={index1} item={item1} index={index1} confrender={confrender} propsview={propsview} />)}
            </View>
        )
    }
}

const styles = theme => {
    return {
        st: {
            [theme.breakpoints.down('sm')]: {
                display: "block !important",
            },
        }
    }
}

RenderCustomForm = withStyles(styles)(RenderCustomForm);

export default RenderCustomForm;