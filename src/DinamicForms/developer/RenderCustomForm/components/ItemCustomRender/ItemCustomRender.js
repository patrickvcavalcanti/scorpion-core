import React, { Component } from 'react'
import { getComponenteView } from '../../../../DinamicComponents';

const ComponenteView = ({ item, field, confrender, propsadd, propsview }) => {
    let propsItemUsar = {...item, propsadd };

    if (field) { //eh debug
        propsItemUsar = {...propsItemUsar, requerido: false, }
    }

    if (item.overrideProps) {
        let overridePropsVar = {};
        try {
            let func = new Function("return " + item.overrideProps)();
            overridePropsVar = func({ propsItemUsar, propsview, confrender })
        } catch (error) {
            console.error('Exceção override ', error);
        }
        propsItemUsar = {...propsItemUsar, ...overridePropsVar }


    }
    if (propsview && propsview.preField) {
        propsItemUsar = {...propsItemUsar, field: propsview.preField + propsItemUsar.field }
    }

    // console.log('passou aqui ', item);


    let CompRetorno = getComponenteView(propsItemUsar, propsview)
    const { componentes } = propsItemUsar;

    return React.cloneElement(CompRetorno, { confrender, propsadd });

}

export default class ItemCustomRender extends Component {


    render() {
        const { item, confrender, propsadd, propsview } = this.props;
        if (propsview && propsview.verificaPermissao && item.permissao) {
            if (!propsview.verificaPermissao(item.permissao)) {
                return "";
            }
        }


        if (confrender && confrender.tipoDebug) {
            return ( <
                ComponenteView item = { item }
                field = { "DebugValues." + item.field }
                confrender = { confrender }
                propsadd = { propsadd }
                propsview = { propsview }
                />
            )
        }
        return ( <
            ComponenteView item = { item }
            propsadd = { propsadd }
            propsview = { propsview }
            />
        )
    }
}