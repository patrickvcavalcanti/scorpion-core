import React, { Component, forwardRef } from 'react'
import View from 'react-flexbox'
import { Field } from 'redux-form'
import { EditorCodeField, Item, SwitchItem, JsonEditor, InputPicker, Input } from '../../../components'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { compose } from 'redux'
import { connect } from 'react-redux'

import { getFormValues } from 'redux-form';
import { Typography, Button } from '@material-ui/core';
import { geraERetornaRelatorio, templates, imprimirDados, gerarRelatorioImprimir } from '../../OkReportGerador';
import { painel, fieldset } from '../../OkReportUtil';


import { required } from '../../../DinamicForms/DinamicComponents/function';
import Switch from '@material-ui/core/Switch';
import 'regenerator-runtime/runtime'
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
    BarChart, Bar, Cell,
    PieChart, Pie,
    ReferenceLine
} from 'recharts';
import ReactDOM from 'react-dom';
import { supportType } from 'brace/mode/html';
// import FusionCharts from "fusioncharts";
// import Charts from "fusioncharts/fusioncharts.charts";
// import ReactFC from 'react-fusioncharts';
import ReactSpeedometer from "react-d3-speedometer";
import GaugeChart from 'react-advanced-gauge-chart'

// const GaugeChart = require('react-advanced-gauge-chart')

// Charts(FusionCharts);
// ReactFC.fcRoot(FusionCharts, Charts);

const dataSource = {
    chart: {
        caption: "Nordstorm's Customer Satisfaction Score for 2017",
        lowerlimit: "0",
        upperlimit: "100",
        showvalue: "1",
        numbersuffix: "%",
        theme: "fusion",
        showtooltip: "0"
    },
    colorrange: {
        color: [
            {
                minvalue: "0",
                maxvalue: "50",
                code: "#F2726F"
            },
            {
                minvalue: "50",
                maxvalue: "75",
                code: "#FFC533"
            },
            {
                minvalue: "75",
                maxvalue: "100",
                code: "#62B58F"
            }
        ]
    },
    dials: {
        dial: [
            {
                value: "81"
            }
        ]
    }
};

const chartConfigs = {
    type: "angulargauge",
    width: "100%",
    height: "100%",
    dataFormat: "json",
    dataSource: dataSource
}

export const svgToPng = (svg, width, height) => {

    return new Promise((resolve, reject) => {

        let canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext('2d');

        // Set background to white
        ctx.fillStyle = '#ffffff';
        ctx.fillRect(0, 0, width, height);

        let xml = new XMLSerializer().serializeToString(svg);
        let dataUrl = 'data:image/svg+xml;utf8,' + encodeURIComponent(xml);
        let img = new Image(width, height);

        img.onload = () => {
            ctx.drawImage(img, 0, 0);
            let imageData = canvas.toDataURL('image/png', 1.0);
            resolve(imageData)
        }

        img.onerror = () => reject();

        img.src = dataUrl;
    });
};

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

class RelatorioDeveloperForm extends Component {
    constructor(props) {
        super(props);
        this.updateRell = this.updateRell.bind(this)
        this.atualiza = this.atualiza.bind(this)
        this.criaFuncoes1 = this.criaFuncoes1.bind(this)
        this.state = {
            tab: 0,
            docRel: null,
            autoupdate: false,
            showPreview: true,
        }

    }

    gerar(props) {
        if (props.jsonGerar) {
            imprimirDados(props.atualValues, props.jsonGerar.dados, props.jsonGerar.meta)
        }
    }
    convertChart1 = async (ref, WIDTH, HEIGHT, retorno) => {

        console.log('Ref ', ref);
        if (ref && ref.container) {
            let svg = ref.container.children[0];
            let pngData = await svgToPng(svg, WIDTH, HEIGHT);
            retorno(pngData)
        } else if (ref && ref.gaugeDiv) {
            let svg = ref.gaugeDiv.children[0];
            let pngData = await svgToPng(svg, WIDTH, HEIGHT);
            retorno(pngData)
        } else if (ref && ref.chartRef) {
            let svg = ref.chartRef.current.children[0].children[0];
            // console.log('Chart ', ref.chartRef.current.children[0].children[0]);
            let pngData = await svgToPng(svg, WIDTH, HEIGHT);
            // console.log('Png::::>>>  ', pngData);
            retorno(pngData)
        }
    };
    criaGrafico(chart, name) {
        const { convertChart1 } = this;
        let comp = null;

        if (chart.type == "BarChart") {
            comp = this.getBarChart(chart)
        }

        if (chart.type == "LineChart") {
            comp = this.getLineChart(chart)
        }

        if (chart.type == "PieChart") {
            comp = this.getPieChart(chart)
        }

        if (chart.type == "GaugeChart") {
            comp = this.getGaugeChart(chart)
        }


        return new Promise((resolve, reject) => {
            if (comp) {
                console.log('Comp ', comp);
                const WIDTH = comp.props.width;
                const HEIGHT = comp.props.height;


                // let compComRef = React.cloneElement(comp, {
                //     ref: ref => this.convertChart1(ref, WIDTH, HEIGHT, (r) => {
                //         let rr = {}
                //         rr[name] = r
                //         resolve(rr);
                //     }),
                //     isAnimationActive: false
                // })
                let compComRef = React.cloneElement(comp, {
                    ref: ref => setTimeout(function () {
                        convertChart1(ref, WIDTH, HEIGHT, (r) => {
                            let rr = {};
                            rr[name] = r;
                            resolve(rr);
                        })
                    }, 500),
                    isAnimationActive: false
                })

                const helperDiv = document.createElement('tmp');
                ReactDOM.render(compComRef, helperDiv);

            } else {
                resolve(null);
            }
        })



    }

    exportChart = (props, retorno, reject) => {
        const conteudo = props.atualValues.code_conteudo;

        let classCodigo = this.criaFuncoes1(conteudo);

        if (classCodigo.getGraficos) {
            const { meta, dados } = props.jsonGerar;
            let chs = classCodigo.getGraficos(dados, meta)


            let metaAsync = {};



            Promise.all(chs.map((c, index) => this.criaGrafico(c, c.name))).then(function (values) {
                values.forEach(element => {
                    metaAsync = { ...metaAsync, ...element };
                });
                retorno(metaAsync)
            })
                .catch(function (err) {
                    reject(err);
                });
        } else {
            retorno({})
        }
    }
    getLineChart(grafico) {
        let dataNovo = [];
        grafico.columns.forEach((col, index) => {
            let dado = { name: col };
            grafico.series && grafico.series.forEach(serie => {
                if (serie.values[index])
                    dado[serie.name] = serie.values[index];
            });
            dataNovo.push(dado)
        });

        return (<LineChart
            name={"lc"}
            width={500}
            height={250}
            data={dataNovo}
            isAnimationActive={false}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            {grafico.series.map((serie, index) => {
                return <Line isAnimationActive={false} dataKey={serie.name} {...serie.props} />
            })}
        </LineChart>)
    }
    getBarChart(grafico) {
        let dataNovo = [];
        let domain = grafico.domain ? grafico.domain : []
        let ticks = grafico.ticks ? grafico.ticks : []
        let width = grafico.width ? grafico.width : null;
        let height = grafico.height ? grafico.height : null;
        grafico.columns.forEach((col, index) => {
            let dado = { name: col };
            grafico.series && grafico.series.forEach(serie => {
                if (serie.values[index])
                    dado[serie.name] = serie.values[index];
            });
            dataNovo.push(dado)
        });


        return (<BarChart
            name={"bc"}
            width={width || 500}
            height={height || 300}
            data={dataNovo}
            isAnimationActive={false}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis domain={domain} ticks={ticks || null}/>
            <ReferenceLine y={4000} stroke="red" strokeDasharray="3 3" label={{value: 'Meta', position: 'left'}} />
            {grafico.series.map((serie, index) => {
                return <Bar isAnimationActive={false} dataKey={serie.name} {...serie.props} />
            })}
        </BarChart>)
    }

    getPieChart(grafico) {
        return (<PieChart width={400} height={400}>
            <Pie dataKey="value" isAnimationActive={false} data={grafico.values} {...grafico.props} />
        </PieChart>)
    }

    getGaugeChart(grafico) {
        let data = grafico.values;
        let config = {
            type: 'bar',
            series: [{
                values: [4, 5, 3, 4, 5, 3, 5, 4, 11]
            }]
        }

        return (
            <ReactSpeedometer
                maxValue={100}
                minValue={0}
                value={100}
                // needleColor="gray"
                // startColor="red"
                // segments={20}
                // endColor="green"
                customSegmentStops={[0, 35, 60, 100]}
                maxSegmentLabels={5}
                segments={3}
                width={300}
                height={300}
                // segmentColors={['#41fc03', '#fcb503', '#fcf403', '#fc2c03', '#fc2003', '#fc2c03']}
            />
        )
    }

    atualiza(props) {
        if (props.jsonGerar && props.jsonGerar.dados) {
            this.exportChart(props, (metaAsync) => {
                // console.log('Meta Async ', metaAsync);
                this.atualizaDo(props, metaAsync)
            }, reject => {
                console.log('Error export chart ', reject);
            })
        }
    }

    criaFuncoes1(code) {
        var arrStr = code.split('function ');
        let classe = {};
        arrStr.forEach(m => {
            if (m) {
                let nome = m.split('(')[0].trim();
                let func = " return function " + m;
                let funJs = new Function(func)
                funJs.bind(this)
                classe[nome] = funJs().bind(this);
            }

        })
        return classe;
    }

    testeStr2Js(conteudo) {


        this.criaFuncoes1(conteudo);

    }
    atualizaDo(props, chats) {

        if (props.jsonGerar && props.jsonGerar.dados) {
            const conteudo = props.atualValues.code_conteudo;
            const { meta, dados } = props.jsonGerar;

            let metaUsar = {}
            if (meta) {
                metaUsar = meta;
            }

            metaUsar.chats = chats

            try {
                let classCodigo = this.criaFuncoes1(conteudo);
                var strJsonConteudo = "";
                if (classCodigo.gerarRelatorio) {
                    console.log("Gerando do gerarRelatorio");


                    let imports = { painel, fieldset };
                    strJsonConteudo = classCodigo.gerarRelatorio(dados, metaUsar, imports);
                } else {
                    console.log("Gerando do 'relatorio(dados,meta)'");
                    let conteudoAjustado = "relatorio(dados,meta);"
                    conteudoAjustado += conteudo;
                    strJsonConteudo = eval(conteudoAjustado);
                }
                let docRel = geraERetornaRelatorio(props.atualValues, strJsonConteudo, meta || this.props.metaRelatorio)
                console.log("docRel", docRel);

                // let docRel = retornaDocumentoTemplateOkReport(strJsonConteudo, styles, nome || "Relatorio de x")
                if (docRel) {
                    docRel.getDataUrl(this.updateRell, docRel);
                }
            } catch (error) {
                console.log('error', error);
            }
        }
    }
    realtime(props) {
        if (this.state.autoupdate) {
            this.atualiza(props)
        }
    }
    componentWillReceiveProps(props) {
        this.realtime(props);
    }

    updateRell(rel) {
        this.setState({ docRel: rel })
    }

    render() {
        const { metasAdd } = this.props
        const { showPreview } = this.state

        let templatesExibir = templates.map(t => t.nome);
        templatesExibir.splice(0, 0, "");

        return (
            <View column style={{ flex: 1, height: "98%" }}>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    {/* <Button onClick={() => this.gerar(this.props)}>gerar</Button> */}
                    <Item style={{ flex: 1 }}>
                        <Field name="uid" placeholder="UID" component={Input} disabled={true} />
                    </Item>
                    <Item style={{ flex: 1 }}>
                        <Field name="nome" placeholder="Nome" component={Input} validate={[required]} />
                    </Item>

                    <Item style={{ flex: 1 }}>
                        <Field name="tipoModelo" placeholder="Tipo Modelo Relatório" component={Input} validate={[required]} />
                    </Item>
                    <Item>
                        <Field name="descricao" placeholder="Descrição :)" component={Input} />
                    </Item>{showPreview &&
                        <Item style={{ flex: 0 }}>
                            <View style={{ justifyContent: "flex-start", alignItems: "center", flex: '1', padding: 5 }}>
                                <Switch checked={this.state.autoupdate} onChange={(b) => this.setState({ autoupdate: !this.state.autoupdate })} />
                                <Typography >AutoUpdate</Typography>
                            </View>

                            <Button variant="contained" color="primary" size="small" onClick={() => this.atualiza(this.props)}>Atualizar</Button>

                        </Item>}
                    <Item style={{ flex: 0 }}>
                        <View style={{ justifyContent: "flex-start", alignItems: "center", flex: '1', padding: 5 }}>
                            <Switch checked={showPreview} onChange={(b) => this.setState({ showPreview: !showPreview, autoupdate: false })} />
                            <Typography >Show Preview</Typography>
                        </View>
                    </Item>
                </div>

                <View>

                    <View column style={{ flex: 1 }}>
                        <AppBar position="static">
                            <Tabs value={this.state.tab} onChange={(event, value) => this.setState({ tab: value })}>
                                <Tab label="Código" />
                                <Tab label="Style" />
                                <Tab label="Meta" />
                            </Tabs>
                        </AppBar>
                        {this.state.tab === 0 && <View column style={{ flex: 1 }}>
                            <Field name="code_conteudo" component={EditorCodeField} />
                        </View>}
                        {this.state.tab === 1 && <View column style={{ flex: 1 }}>
                            <Field name="styles" component={JsonEditor} />
                        </View>}
                        {this.state.tab === 2 && <View column style={{ flex: 1, display: 'block' }}>
                            <Item>
                                <Field name="template" placeholder="Template" itens={templatesExibir} toStringItem={(t) => t} component={InputPicker} />
                            </Item>
                            {metasAdd}
                        </View>}
                    </View>
                    {showPreview && (this.props.jsonGerar ?
                        <View column style={{ flex: 1 }}>
                            <embed width="100%" height="100%" name="plugin" id="plugin" src={this.state.docRel} type="application/pdf"></embed>
                        </View>
                        :
                        <View column style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Typography>Envia dados JSON para emitir o PDF</Typography>
                        </View>

                    )}
                </View >

            </View>
        )
    }
}


const mapStateToProps = (state, props) => {
    let atualValues = getFormValues(props.form)(state)
    return { atualValues };
};
export default compose(
    connect(mapStateToProps, {})
)(RelatorioDeveloperForm);