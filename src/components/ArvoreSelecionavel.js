import React, { Component } from 'react'
import TreeComponent from './TreeComponent';
import FormControlLabel from '@material-ui/core/Checkbox';
import Checkbox from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';

import produce from "immer"

export default class ArvoreSelecionavel extends Component {
    constructor(props) {
        super(props);
    }


    pegaFilho(pai, chave) {
        let filhoRe = null;
        pai.filhos.forEach(f => {
            if (f.label === chave) {

                filhoRe = f;
            }
        })
        return filhoRe;
    }
    montaArvore(pai, str, strPai) {
        let arStr = str.split('.');

        if (strPai) {
            strPai = strPai + "."
        }
        if (arStr.length === 1) {
            let f = this.pegaFilho(pai, arStr[0])


            if (!f) {

                f = { label: arStr[0], filhos: [], chave: strPai + arStr[0] };
                pai.filhos.push(f);
            }
        } else {
            let subString = str.substring(arStr[0].length + 1)

            let f = this.pegaFilho(pai, arStr[0])
            // console.log("f", f);

            if (!f) {
                f = { label: arStr[0], filhos: [], chave: strPai + arStr[0] };
                pai.filhos.push(f);
            }

            this.montaArvore(f, subString, strPai + arStr[0]);
        }
    }

    populaOutrosChecks(leafData) {
        let checks = [];
        leafData.filhos.map(c => {
            checks.push(c)
        })
        return checks;
    }

    populaChecked(leafData, ch, sel) {
        const { opcoes } = this.props;
        let chave = leafData.chave
        let newArray = [];
        if (this.getAtual()) {
            newArray = this.getAtual().slice();
        }
        opcoes.forEach(p => {
            if (p.startsWith(chave)) {
                if (sel) {
                    var index = newArray.indexOf(p);
                    if (index > -1) {
                        newArray.splice(index, 1);
                    }
                } else {
                    newArray.push(p)
                }
            }
        })

        this.updateValor(newArray);
    }

    isChecked(chave) {
        const { opcoes } = this.props
        let checked = [];
        if (this.getAtual()) {
            checked = this.getAtual()
        }
        let r = -1;


        opcoes.forEach(p => {
            if (p.startsWith(chave)) {
                const currentIndex = checked.indexOf(p);
                if (currentIndex === -1) {
                    if (r === -1) {
                        r = false;
                    } else {
                        if (r !== null) {
                            if (r) {
                                r = null;
                            }

                        }
                    }


                } else {
                    if (r === -1) {
                        r = true;
                    } else {
                        if (r !== null) {
                            if (!r) {
                                r = null;
                            }
                        }
                    }

                }
            }
        })



        return r;
    }
    renderTreeLeafLabel = (leafData, expand) => {
        const { label, chave } = leafData;
        return (<div>
            <FormControlLabel
                control={
                    <Checkbox
                        indeterminate={this.isChecked(chave) === null}
                        checked={this.isChecked(chave) ? true : false}
                        onChange={() => this.populaChecked(leafData, this.getAtual(), this.isChecked(chave))}
                    />
                }

            />
            {label}

        </div>
        );
    };

    getAtual() {
        const { input: { value } } = this.props
        return value
    }
    updateValor(newV) {
        const { input: { onChange } } = this.props
        onChange(newV)
    }

    render() {
        const { input: { value }, opcoes, label,expandAll } = this.props

        let arvore = { label: label, filhos: [], chave: "" };
        opcoes.forEach(str => {
            this.montaArvore(arvore, str, "")
        })

        return (
            <div>
                <TreeComponent
                    style={{ flex: 1, overflow: "auto", }}
                    actionsAlignRight={true}
                    title={label}
                    data={arvore}
                    labelName="label"
                    valueName="label"
                    childrenName="filhos"
                    renderLabel={this.renderTreeLeafLabel}
                    expandAll={expandAll==undefined?true:expandAll}
                />
            </div>
        )
    }
}