import React, { Component } from 'react'
import TreeComponent from './TreeComponent';

import produce from "immer"
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export default class ArvoreSelecionavel extends Component {
    constructor(props) {
        super(props);
        this.isSelecionado = this.isSelecionado.bind(this);
        this.marcaDesmarcaPercore = this.marcaDesmarcaPercore.bind(this);
        this.rendeNoArvore = this.rendeNoArvore.bind(this);
    }
    isSelecionado(chave) {
        const { opcoes, input: { value } } = this.props;
        if (opcoes.includes(chave)) {
            let r = value[chave];
            return r ? true : false;
        } else {//perguntando Pai
            let rPai = -1;
            for (const key in opcoes) {
                if (opcoes.hasOwnProperty(key)) {
                    const ch = opcoes[key];
                    if (ch.startsWith(chave)) {
                        if (rPai === -1) {
                            rPai = this.isSelecionado(ch);
                        } else {
                            let f = this.isSelecionado(ch);
                            if (rPai !== f) {
                                return null;
                            }
                        }
                    }

                }
            }


            return rPai
        }



    }
    seleciona(chave, value) {
        value[chave] = true;
    }
    deseleciona(chave, value) {
        // if (value.hasOwnProperty(chave)) {
        //     delete value[chave];
        // }
        value[chave] = false;
    }

    marcaDesmarcaPercore(chave, selecionado) {
        const { opcoes, input: { value, onChange } } = this.props;
        const nextState = produce({ ...value }, draft => {
            opcoes.forEach(p => {
                if (p.startsWith(chave)) {
                    if (selecionado) {//desmarca

                        this.deseleciona(p, draft)
                    } else {//marca                                            
                        this.seleciona(p, draft)
                    }
                }
            })
        })
        onChange(nextState);
    }

    rendeNoArvore(leafData, expand) {
        const { label, chave } = leafData;
        return (<div >
            <FormControlLabel
                control={
                    <Checkbox
                        indeterminate={this.isSelecionado(chave) === null}
                        checked={this.isSelecionado(chave) ? true : false}
                        onChange={() => this.marcaDesmarcaPercore(chave, this.isSelecionado(chave))}
                        value={chave}
                    />
                }
                label={label}
            />
        </div>
        );
    };

    //#region monta arvore
    montaArvore(pai, str, strPai) {
        let arStr = str.split('.');

        if (strPai) {
            strPai = strPai + "."
        }
        if (arStr.length === 1) {
            let f = this.pegaFilho(pai, arStr[0])


            if (!f) {

                f = { label: arStr[0], filhos: [], chave: strPai + arStr[0] };
                pai.filhos.push(f);
            }
        } else {
            let subString = str.substring(arStr[0].length + 1)

            let f = this.pegaFilho(pai, arStr[0])
            // console.log("f", f);

            if (!f) {
                f = { label: arStr[0], filhos: [], chave: strPai + arStr[0] };
                pai.filhos.push(f);
            }

            this.montaArvore(f, subString, strPai + arStr[0]);
        }
    }
    pegaFilho(pai, chave) {
        let filhoRe = null;
        pai.filhos.forEach(f => {
            if (f.label === chave) {

                filhoRe = f;
            }
        })
        return filhoRe;
    }
    //#endregion
    render() {
        const { opcoes, label } = this.props
        let arvore = { label: label, filhos: [], chave: "" };

        opcoes.forEach(str => {
            this.montaArvore(arvore, str, "")
        })

        return (
            <div>
                <TreeComponent
                    style={{ flex: 1, overflow: "auto", }}
                    actionsAlignRight={true}
                    title={label}
                    data={arvore}
                    labelName="label"
                    valueName="label"
                    childrenName="filhos"
                    renderLabel={this.rendeNoArvore}
                    expandAll={true}
                />
            </div>
        )
    }
}