import React, { Component } from 'react';




import { Button } from '@material-ui/core';



import View from 'react-flexbox';
import Dropzone from 'react-dropzone';

import ReactCrop, { makeAspectCrop } from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Item } from './Item';
import DialogSeletor from './DialogSeletor';



function resizeImage2(image, maxWidth, maxHeight, quality) {
    var canvas = document.createElement('canvas');

    var width = image.width;
    var height = image.height;

    if (width > height) {
        if (width > maxWidth) {
            height = Math.round(height * maxWidth / width);
            width = maxWidth;
        }
    } else {
        if (height > maxHeight) {
            width = Math.round(width * maxHeight / height);
            height = maxHeight;
        }
    }

    canvas.width = width;
    canvas.height = height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(image, 0, 0, width, height);
    return canvas.toDataURL("image/png", quality);
}
export default class CortarImagem extends Component {
    constructor(props) {
        super(props);
        this.concluir = this.concluir.bind(this);
        this.onImageLoaded = this.onImageLoaded.bind(this);
        this.onCropComplete = this.onCropComplete.bind(this);
        this.concluir = this.concluir.bind(this);
        this.state = {
            abaIndex: 0,
            image: null,
            src: null,
            crop: {
                x: 10,
                y: 10,
                width: 80,
                height: 80,
            },
            novoLogo: null
        }

    }
    onDrop(files) {
        if (files && files.length) {
            let file = files[0];
            const reader = new FileReader()
            reader.addEventListener(
                'load',
                () =>
                    this.setState({
                        src: reader.result,
                    }),
                false
            )
            reader.readAsDataURL(file)

        }

        // this.dialog.hide();

    }

    onImageLoaded(image, pixelCrop) {
        console.log('onImageLoaded', image)
        this.setState({ image },()=>this.onCropComplete(null,pixelCrop));
      



    }

    onCropComplete = (crop, pixelCrop) => {
        
        // this.setState({ novoLogo: crop })
        var canvas = document.createElement('canvas');
        canvas.width = pixelCrop.width;
        canvas.height = pixelCrop.height;
        var ctx = canvas.getContext('2d');

        ctx.drawImage(
            this.state.image,
            pixelCrop.x,
            pixelCrop.y,
            pixelCrop.width,
            pixelCrop.height,
            0,
            0,
            pixelCrop.width,
            pixelCrop.height
        );

        // Base64
        const base64Image = canvas.toDataURL('image/png');
        // console.log('base64Image', base64Image);
        this.setState({ novoLogo: base64Image })
    }

    onCropChange = crop => {
        this.setState({ crop })
    }

    processaImagem(data, funcaoAdd) {
        var image = new Image();
        image.src = data;
        image.onload = function () {
            var resizedDataUrl = resizeImage2(image, 480, 480, 0.7);
            funcaoAdd(resizedDataUrl);
        };
    }
    showAdd() {
        this.dialog.show();
    }

    concluir() {
        const { novoLogo } = this.state;
        // const { src } = this.novoLogo;
        // console.log('novoLogo', novoLogo);
        // console.log('src', src);
        var res = novoLogo.split(";base64,");
        // console.log('data', data);
        let base64 = res[1];

        this.props.input.onChange(base64)

        this.setState({
            image: null,
            src: null,
            crop: {
                x: 10,
                y: 10,
                width: 80,
                height: 80,
            },
            novoLogo: null
        })

        this.dialog.hide();
    }
    render() {

        const { input: { value, onChance }, width, height } = this.props
        let imageLogo = null;
        if (value) {
            imageLogo = `data:image/png;base64,${value}`;
        }
        return (

            <View column style={{ justifyContent: 'flex-start', }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', maxWidth: 240, maxHeight: 240, minWidth: 240, minHeight: 240 }}>
                    <img style={{
                        maxWidth: 200,
                        maxHeight: 200,
                        width: 'auto',
                        height: 'auto',
                    }} src={imageLogo} />
                </View>
                <Item style={{ justifyContent: 'center' }}>
                    <Button style={{ height: 38 }} variant="contained" color="primary" onClick={() => this.showAdd()}>Selecionar Logo</Button>
                </Item>
                <DialogSeletor innerRef={node => this.dialog = node}>
                    <View column>
                        {!this.state.src ?

                            <Dropzone
                                onDrop={this.onDrop.bind(this)}
                                accept="image/jpeg, image/png"
                                style={{
                                    width: 350, height: 150,
                                    borderWidth: 2, borderStyle: 'dashed',
                                    borderRadius: 5, textAlign: 'center',
                                    paddingTop: 50
                                }}
                            >
                                {({ getRootProps, getInputProps }) => (
                                    <section>

                                        <View column {...getRootProps()} style={{
                                            height: '100%',
                                            alignItems: "center", justifyContent: 'center'

                                        }}>
                                            <input {...getInputProps()} />
                                            <p>Arraste a logo para este local.</p>
                                            <Button variant="contained" color="primary" >Selecionar</Button>
                                        </View>
                                    </section>
                                )}
                            </Dropzone>


                            :
                            <View style={{
                                width: 500, height: 400,
                                borderWidth: 2, borderStyle: 'dashed', borderRadius: 5,
                                alignItems: "center", justifyContent: 'center'
                            }}>

                                <ReactCrop
                                    src={this.state.src}
                                    crop={this.state.crop}
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={this.onCropChange}
                                />

                            </View>
                        }
                        <View>
                            <Button onClick={() => this.concluir()}>OK</Button>
                            <Button onClick={() => {

                                this.setState({
                                    image: null,
                                    src: null,
                                    crop: {
                                        x: 10,
                                        y: 10,
                                        width: 80,
                                        height: 80,
                                    },
                                    novoLogo: null
                                })
                                this.dialog.hide()
                            }}>Cancelar</Button>
                        </View>


                    </View>
                </DialogSeletor>
            </View>
        )
    }
}
