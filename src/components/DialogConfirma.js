
import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

class DialogConfirma extends React.Component {

  constructor(props) {
    super(props);
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }


  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  show = () => {
    this.setState({ open: true });
  };

  hide = () => {
    this.setState({ open: false });
  };
  render() {
    const { classes, titulo, mensagem, onConfirm, onRecuse } = this.props;



    return (

      <Dialog
        classes={{ paper: classes.dialogPaper }}
        open={this.state.open}
        onClose={this.handleClose}
        disableEnforceFocus
      >
        <DialogTitle id="alert-dialog-title">{titulo}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {mensagem}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => {
            if (onRecuse) {
              onRecuse();
            }
            this.hide()
          }} color="primary">
            Não
            </Button>
          <Button onClick={() => {
            onConfirm();
            this.hide()
          }} color="primary" autoFocus>
            Sim
            </Button>
        </DialogActions>
      </Dialog>);
  }
}
const styles = {
  dialogPaper: {
    maxHeight: '90vh',
    maxWidth: "90vw",
  },
};
DialogConfirma = withStyles(styles)(DialogConfirma);
export default DialogConfirma;
