
import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { TextField } from '@material-ui/core';

class DialogConfirmaEscrito extends React.Component {

    constructor(props) {
        super(props);
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
        this.state = {error: null, open: false};
    }


    state = {
        open: false,
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    show = () => {
        this.setState({ open: true });
    };

    hide = () => {
        this.setState({ open: false });
    };
    render() {
        const { classes, titulo, mensagem, mensagemConfirma, onConfirm } = this.props;


        return (

            <Dialog
                classes={{ paper: classes.dialogPaper }}
                open={this.state.open}
                onClose={this.handleClose}
                disableEnforceFocus
            >
                <DialogTitle id="alert-dialog-title">{titulo}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {'Para Confirmar digite: ' + mensagemConfirma}
                    </DialogContentText>
                    <TextField style={{ flex: 1}}
                        autoFocus
                        error={this.state.error}
                        helperText={this.state.error}
                        margin="dense"
                        id="txtConfirm"
                        // label={'Para Confirmar digite: ' + mensagemConfirma}
                        type="text"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                        this.hide()
                    }} color="primary">
                        Não
                    </Button>
                    <Button onClick={() => {
                        if (document.querySelector('#txtConfirm').value === mensagemConfirma) {
                            this.setState({error: null})
                            onConfirm();
                            this.hide()
                        }else{
                            this.setState({error: 'Inválido'})
                            // console.log('Inválido')
                        }
                    }} color="primary" autoFocus>
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}
const styles = {
    dialogPaper: {
        maxHeight: '90vh',
        maxWidth: "90vw",
    },
};
DialogConfirmaEscrito = withStyles(styles)(DialogConfirmaEscrito);
export default DialogConfirmaEscrito;
