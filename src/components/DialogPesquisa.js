/* eslint-disable import/prefer-default-export */
import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Dialog from '@material-ui/core/Dialog';
import DialogConfirma from './DialogConfirma';
import DialogConfirmaEscrito from './DialogConfirmaEscrito';
class DialogPesquisa extends React.Component {

  constructor(props) {
    super(props);
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);

    this.state = { open: false, }
  }

  show = () => {
    this.setState({ open: true });
  };

  hide = () => {
    this.setState({ open: false });
  };
  showSair = () => {
    const { mensagemSair } = this.props;
    if (mensagemSair) {

      this.dialogConfirma.show();
    } else {
      this.hide();
    }
  };
  render() {
    const { classes, children, superStyle, notClose, mensagemSair, tituloSair } = this.props;
    let childrenWithProps = children;

    if (notClose === undefined) {
      childrenWithProps = React.Children.map(children, child => {

        if (typeof child === 'string') {
          return child
        } else {
          return React.cloneElement(child, { actionclose: () => this.showSair() })
        }
      });
    }

    return (
      <div>

        <Dialog
          classes={{ paper: classes.dialogPaper }}
          open={this.state.open}
          onClose={this.showSair}
          fullScreen
          disableEnforceFocus
        >
          {childrenWithProps}
        </Dialog>
        <DialogConfirma innerRef={node => this.dialogConfirma = node} titulo={tituloSair} mensagem={mensagemSair || "Deseja mesmo sair?"} onConfirm={this.hide} />
      </div>
    );
  }
}
const styles = theme => ({
  dialogPaper: {
    [theme.breakpoints.up('sm')]: {
      maxHeight: "90%",
      maxWidth: "90%",
      height: "100%"
    }
  },
});
// DialogPesquisa = withStyles(styles)(DialogPesquisa);
// export default DialogPesquisa;
const withRefs = ComponentW => {
  class WithFromContext extends Component {
    render() {
      return <ComponentW ref={this.props.forwardedRef}  {...this.props} />
    }
  }
  return WithFromContext
}


DialogPesquisa = withRefs(DialogPesquisa);
DialogPesquisa = withMobileDialog({ breakpoint: 'xs' })(DialogPesquisa);

DialogPesquisa = withStyles(styles)(DialogPesquisa);

export default DialogPesquisa;