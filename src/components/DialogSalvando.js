import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {CircularProgress} from '@material-ui/core'

class DialogSalvando extends Component {
    constructor(props) {
        super(props);
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
    }


    state = {
        open: false,
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    show = () => {
        this.setState({ open: true });
    };

    hide = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, titulo, mensagem, onCancel } = this.props;
        return (
            <Dialog
                classes={{ paper: classes.dialogPaper }}
                open={this.state.open}
                onClose={this.handleClose}
                disableEnforceFocus
            >
                <DialogTitle id="alert-dialog-title">{titulo}</DialogTitle>
                <DialogContent style={{display: 'flex', justifyContent: 'space-evenly'}}>
                    <DialogContentText id="alert-dialog-description">
                        {mensagem}
                    </DialogContentText>
                    <CircularProgress size={20}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                        onCancel();
                        this.hide()
                    }} color="primary" autoFocus>
                        Cancelar
                    </Button>
                </DialogActions>
            </Dialog >
        )
    }
}

const styles = {
    dialogPaper: {
        width: "70vw",
        maxHeight: '90vh',
        maxWidth: "90vw",
    },
};

DialogSalvando = withStyles(styles)(DialogSalvando);
export default DialogSalvando;