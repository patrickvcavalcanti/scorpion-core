import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import Dialog from '@material-ui/core/Dialog';

class DialogSeletor extends React.Component {

  constructor(props) {
    super(props);
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.state = { open: false, }
  }

  show = () => {
    this.setState({ open: true });
  };

  hide = () => {
    this.setState({ open: false });
  };
  render() {
    const { classes, children, notClose } = this.props;
    let childrenWithProps = children;

    if (notClose === undefined) {
      childrenWithProps = React.Children.map(children, child => {

        if (child === " ") {
          return child
        } else {
          if (child.type.name === "View" || child.type.name === "div") {
            return child;
          }
          return React.cloneElement(child, { actionclose: () => this.hide() })
        }
      });
    }

    return (

      <Dialog
        classes={{ paper: classes.dialogPaper }}
        open={this.state.open}
        onClose={this.hide}
        disableEnforceFocus
      >
        {childrenWithProps}
      </Dialog>);
  }
}
const styles = {
  dialogPaper: {
    maxHeight: '90vh',
    maxWidth: "50vw",
    width: 'auto',
    overflow: 'hidden'
  },
};
DialogSeletor = withStyles(styles)(DialogSeletor);
export default DialogSeletor;
