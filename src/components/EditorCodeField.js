import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography';
import AceEditor from 'react-ace';
import 'brace/mode/html';
import 'brace/theme/monokai';
import 'brace/snippets/html';
import 'brace/ext/language_tools';
import 'brace/ext/beautify';

import View from 'react-flexbox'

export default class EditorCodeField extends Component {

    render() {


        const {
            input: { value, onChange },
            meta: { touched, error },
            start,
            label
        } = this.props;
        return (<View column>
            <Typography>{label}</Typography>
            <AceEditor
                style={{ display: "flex", flex: "1" }}
                width="100%"
                mode="javascript"
                theme="monokai"
                name="blah2"

                onChange={onChange}
                fontSize={14}
                showPrintMargin={true}
                showGutter={true}
                highlightActiveLine={true}
                value={value||start}

                setOptions={{
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true,
                    showLineNumbers: true,
                    tabSize: 2,
                }}
                editorProps={{
                    $blockScrolling: Infinity
                }}
                commands={[{   // commands is array of key bindings.
                    name: 'commandName', //name for the key binding.
                    bindKey: "contextmenu", //key combination used for the command.
                    exec: () => { console.log('key-binding used'); }  //function to execute when keys are pressed.
                },

                ]}
            />
        </View>
        )
    }
}
