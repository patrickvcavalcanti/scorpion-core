import React, { Component } from 'react'

import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import { InputPicker } from '../InputPicker';

class FiltroComboboxPesquisaOk extends Component {

    render() {
        const { name, label, placeholder, itens } = this.props;
        return (
            <div>
                <Field 
                    name={name}
                    label={label}
                    placeholder={placeholder} 
                    toStringItem={(item) => item }
                    itens={itens} 
                    component={InputPicker} 
                />
            </div>
        )
    }
}
const propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired

};

FiltroComboboxPesquisaOk.propTypes = propTypes;

export default FiltroComboboxPesquisaOk;