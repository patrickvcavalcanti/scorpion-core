import React from 'react';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import Downshift from 'downshift';
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';

function renderInput(inputProps) {
    const { InputProps, label, placeholder, classes, ref, ...other } = inputProps;

    return (
        <TextField
            InputProps={{
                inputRef: ref,
                classes: {
                    root: classes.inputRoot,
                },
                ...InputProps,
            }}
            label={label}
            // placeholder={placeholder}
            {...other}
        />
    );
}




class FiltroDownshiftMultiple extends React.Component {


    constructor(props) {
        super(props);
        this.atende = this.atende.bind(this);
        this.auxFilterMulti = this.auxFilterMulti.bind(this);
    }

    // state = {
    //     inputValue: '',
    //     selectedItem: [],
    // };
    onChangeState(p) {
        this.props.input.onChange({ ...this.props.input.value, ...p, atende: this.atende })
    }
    auxFilterMulti(b, stringCoparar, objEquals, filter) {
        if (!b) {
            return b;
        }
        let filtroItem = filter.selectedItem;
        // console.log("filtroItem", filtroItem)
        let filtroString = filter.inputValue;
        // console.log("filtroString", filtroString)
        // console.log("stringCoparar", stringCoparar)
        // console.log("objEquals 4", objEquals)



        if (!filtroItem || !filtroItem.length) {
            if (filtroString)
                return stringCoparar.toUpperCase().includes(filtroString.toUpperCase());
        } else {
            if (filtroItem.length) {
                let axo = false;
                for (let i = 0; i < filtroItem.length; i++) {
                    if (filtroItem[i] === objEquals) {
                        axo = true;
                        break;
                    }
                }

                if (!axo) {
                    return false;
                }
            }
        }
        return true;
    }

    atende(b, obj, filter) {

        let stringCoparar = this.props.stringComparar(obj);
        let objEquals = this.props.objEquals(obj);
        return this.auxFilterMulti(b, stringCoparar, objEquals, filter)
    }

    handleKeyDown = event => {
        let { inputValue, selectedItem } = this.props.input.value;
        if (!selectedItem) {
            selectedItem = [];
        }
        if (selectedItem.length && !inputValue.length && keycode(event) === 'backspace') {
            this.onChangeState({ selectedItem: selectedItem.slice(0, selectedItem.length - 1) })


            // this.setState({
            //     selectedItem: selectedItem.slice(0, selectedItem.length - 1),
            // });
        }
    };

    handleInputChange = event => {
        // this.setState({ inputValue: event.target.value });
        this.onChangeState({ inputValue: event.target.value })
    };

    handleChange = item => {
        let { selectedItem } = this.props.input.value;
        if (!selectedItem) {
            selectedItem = [];
        }
        if (selectedItem.indexOf(item) === -1) {
            selectedItem = [...selectedItem, item];
        }

        // this.setState({
        //     inputValue: '',
        //     selectedItem,
        // });
        this.onChangeState({ inputValue: '', selectedItem, })
    };

    handleDelete = item => () => {
        const selectedItem = [...this.props.input.value.selectedItem];
        selectedItem.splice(selectedItem.indexOf(item), 1);

        // this.setState({ selectedItem });
        this.onChangeState({ selectedItem, })
    };

    getSuggestions(inputValue) {
        let count = 0;

        let suggestions = [
            { label: 'Afghanistan' },
            { label: 'Aland Islands' },
            { label: 'Albania' },
            { label: 'Algeria' },
            { label: 'American Samoa' },
            { label: 'Andorra' },
            { label: 'Angola' },
            { label: 'Anguilla' },
            { label: 'Antarctica' },
        ];
        if (this.props.possiveis) {
            suggestions = this.props.possiveis
        }

        return suggestions.filter(suggestion => {
            const keep = (!inputValue || this.getToStringItem(suggestion).toLowerCase().indexOf(inputValue.toLowerCase()) !== -1)
                // && count < 5;

            if (keep) {
                count += 1;
            }

            return keep;
        });
    }

    getToStringItem(item) {
        if (this.props.item2String)
            return this.props.item2String(item);
        if (item.label) {
            return item.label;
        }
        return item;
    }
    renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem }) {
        const isHighlighted = highlightedIndex === index;
        const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;

        return (
            <MenuItem
                {...itemProps}
                key={this.getToStringItem(suggestion)}
                selected={isHighlighted}
                component="div"
                style={{
                    fontWeight: isSelected ? 500 : 400,
                }}
            >
                {this.getToStringItem(suggestion)}
            </MenuItem>
        );
    }

    render() {
        const { classes, label, placeholder } = this.props;
        let { inputValue, selectedItem } = this.props.input.value;
        if (!selectedItem) {
            selectedItem = [];
        }
        return (
            <Downshift inputValue={inputValue} onChange={this.handleChange} selectedItem={selectedItem}>
                {({
                    getInputProps,
                    getItemProps,
                    isOpen,
                    inputValue: inputValue2,
                    selectedItem: selectedItem2,
                    highlightedIndex,
                }) => (
                        <div className={classes.container}>
                            {renderInput({
                                fullWidth: true,
                                classes,
                                label,
                                placeholder,
                                InputProps: getInputProps({
                                    startAdornment: selectedItem.map(item => (
                                        <Chip
                                            key={item}
                                            tabIndex={-1}
                                            label={item}
                                            className={classes.chip}
                                            onDelete={this.handleDelete(item)}
                                        />
                                    )),
                                    onChange: this.handleInputChange,
                                    onKeyDown: this.handleKeyDown,
                                    placeholder,
                                    id: 'integration-downshift-multiple',
                                }),
                            })}
                            {isOpen ? (
                                <Paper className={classes.paper} square>
                                    {this.getSuggestions(inputValue2).map((suggestion, index) =>
                                        this.renderSuggestion({
                                            suggestion,
                                            index,
                                            itemProps: getItemProps({ item: this.getToStringItem(suggestion) }),
                                            highlightedIndex,
                                            selectedItem: selectedItem2,
                                        }),
                                    )}
                                </Paper>
                            ) : null}
                        </div>
                    )}
            </Downshift>
        );
    }
}

FiltroDownshiftMultiple.propTypes = {
    classes: PropTypes.object.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: 250,
    },
    container: {
        flexGrow: 1,
        position: 'relative',
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    },
    chip: {
        margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    },
    inputRoot: {
        flexWrap: 'wrap',
    },
});


FiltroDownshiftMultiple = withStyles(styles)(FiltroDownshiftMultiple);

export default FiltroDownshiftMultiple;
