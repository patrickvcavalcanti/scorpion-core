import React, { Component } from 'react'

import PropTypes from 'prop-types'
import { Field } from 'redux-form';

import Divider from '@material-ui/core/Divider';
import FiltroDownshiftMultiple from './FiltroDownshiftMultiple';

class FiltroStringPesquisaOk extends Component {

    preparaSugestoes(sugestoes) {
        if (!sugestoes) {
            if (this.props.criaSugestaoPorLike) {
                // tem q receber a lista pra isso
                return [...new Set(this.props.criaSugestaoPorLike.map(this.props.strComparaLike))];

            }

            return [];
        }
        return [...new Set(sugestoes)];
    }

    render() {
        const { name, label, placeholder, strComparaLike, sugestoes, strComparaSugestao, style } = this.props;
        return (
            <div   style={style}>
                <Field name={name}
                    label={label}
                    placeholder={placeholder}
                    stringComparar={strComparaLike}
                  
                    // possiveis={[...new Set(this.props.areas_cultivadas.map((item) => item.nomeProdutor))]}
                    possiveis={this.preparaSugestoes(sugestoes)}
                    objEquals={strComparaSugestao ? strComparaSugestao : strComparaLike}

                    component={FiltroDownshiftMultiple} />
                <Divider />
            </div>
        )
    }
}
const propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    strComparaLike: PropTypes.func.isRequired,

    criaSugestaoPorLike: PropTypes.array,
    sugestoes: PropTypes.arrayOf(PropTypes.string),
    strComparaSugestao: PropTypes.func,

};

FiltroStringPesquisaOk.propTypes = propTypes;

export default FiltroStringPesquisaOk;