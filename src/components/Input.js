import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';
// import { render } from 'react-dom';

const propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  secureTextEntry: PropTypes.bool,
  multiline: PropTypes.bool,
  containerStyle: PropTypes.object,
};

const defaultProps = {
  secureTextEntry: false,
  multiline: false,
  containerStyle: {},
};

const Input = props => {

  const {
    input: { value, onChange },
    meta: { touched, error },
    placeholder,
    type,
    secureTextEntry,
    multiline,
    containerStyle,
    disabled,
    ...otherProps
  } = props;

  let error1 = touched && error ? true : false;


  return (
    <TextField
      disabled={disabled}
      error={error1}
      label={placeholder}
      type={type ? type : "text"}
      value={value}
      onChange={event => onChange(event.target.value)}
      helperText={error1 ? error : ""}
      fullWidth
      style={{ marginBottom: 15 }}
      InputLabelProps={{
        shrink: true,
      }}
      {...otherProps}
    />
  );
};

Input.defaultProps = defaultProps;
Input.propTypes = propTypes;

export {Input};
