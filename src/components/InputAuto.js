import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';


import Downshift from 'downshift';
function renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem }) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || '').indexOf(suggestion) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {suggestion}
    </MenuItem>
  );
}

function renderInput(inputProps) {
  const { InputProps, classes, ref, placeholder, helperText, error1, disabled, otherProps, ...other } = inputProps;


  return (
    <TextField
      disabled={disabled ? true : false}
      InputProps={{
        inputRef: ref,
        // classes: {
        //   root: classes.inputRoot,
        // },
        ...InputProps,
      }}
      {...other}
      fullWidth
      style={{ marginBottom: 15 }}
      error={error1}
      label={placeholder}
      helperText={helperText}
      InputLabelProps={{
        shrink: true,
      }}
      {...otherProps}
    />
  );
}
class InputAuto extends Component {
  render() {
    const {
      input: { value, onChange },
      data,
      meta: { touched, error },
      placeholder,
      disabled,
      classes,
      ...otherProps
    } = this.props;
    let error1 = touched && error ? true : false;
    // console.log('data',data);


    return (
      <Downshift
        disabled={disabled ? true : false}
        inputValue={value}
        onChange={(value) => onChange(value)}
      >
        {({ getInputProps, getItemProps, isOpen, inputValue, selectedItem, highlightedIndex }) => (
          <div className={classes.container}>
            {renderInput({
              fullWidth: true,
              classes,
              placeholder,
              error1: error1,
              disabled: disabled,
              helperText: error1 ? error : "",
              InputProps: getInputProps({
                onChange: (value) => onChange(value),
              }),
              otherProps
            })}
            {isOpen ? (
              <Paper className={classes.paper} square>
                {data.map((suggestion, index) =>
                  renderSuggestion({
                    suggestion,
                    index,
                    itemProps: getItemProps({ item: suggestion }),
                    highlightedIndex,
                    selectedItem,
                  }),
                )}
              </Paper>
            ) : null}
          </div>
        )}
      </Downshift>
    )
  }
}
const styles = theme => ({
  root: {
    flexGrow: 1,
    height: 250,
  },
  container: {
    flexGrow: 1,
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    // marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  inputRoot: {

    marginBottom: 15
  },
});
const propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,

};

InputAuto.propTypes = propTypes;
InputAuto = withStyles(styles)(InputAuto)
export { InputAuto }
