import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  secureTextEntry: PropTypes.bool,
  multiline: PropTypes.bool,
  containerStyle: PropTypes.object,
};

const defaultProps = {
  secureTextEntry: false,
  multiline: false,
  containerStyle: {},
};

const formataStr = (v) => {
  let res = v.split('-');
  let dia = res[2];
  let mes = res[1];
  let ano = res[0];
  return dia + "/" + mes + "/" + ano
}

const InputDateTime = (props) => {

  const {
    input: { value, onChange },
    meta: { touched, error },
    placeholder,
    type,
    secureTextEntry,
    multiline,
    containerStyle,
    disabled,
    automatico,
    ...otherProps
  } = props;

  let error1 = touched && error ? true : false;

  let data = value;
  
  

  if (value) {
    let res = value.split('/');
    let dia = res[0];
    let mes = res[1];
    let ano = res[2];
    data = ano + "-" + mes + "-" + dia
  }
  if (data && data.includes("undefined")) {
    data = "";
  }

  if (!data && automatico && value === '') {
    let dtAtual = new Date();
    let dia = dtAtual.getDate();
    let mes = dtAtual.getMonth() + 1;
    let ano = dtAtual.getFullYear();
    let hr = dtAtual.getHours();
    let min = dtAtual.getMinutes();

    data = ano + "-" + (mes < 10 ? "0" + mes : mes) + "-" + (dia < 10 ? "0" + dia : dia)+"T"+(hr < 10 ? "0"+hr : hr)+":"+(min < 10 ? "0"+min: min);
    data = formataStr(data)
    onChange(data)
  }

  return (
    <TextField
      disabled={disabled}
      error={error1}
      label={placeholder}
      type="datetime-local"
      value={data}
      onChange={event => {
        console.log("data ", event.target.value);
        let data = formataStr(event.target.value);
        onChange(data);
        if (props.onSelected) props.onSelected(data);

      }}
      helperText={error1 ? error : ""}
      fullWidth
      style={{ marginBottom: 15 }}
      InputLabelProps={{
        shrink: true,
      }}
      {...otherProps}
    />
  );
};

InputDateTime.defaultProps = defaultProps;
InputDateTime.propTypes = propTypes;

export { InputDateTime };
