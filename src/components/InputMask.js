import React from 'react';
// import MaskedInput from 'react-text-mask';
import InputMask1 from 'react-input-mask';
import MaterialInput from '@material-ui/core/Input';
import PropTypes from 'prop-types';
import { TextField, Input, InputLabel } from '@material-ui/core';

const propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
    secureTextEntry: PropTypes.bool,
    multiline: PropTypes.bool,
    containerStyle: PropTypes.object,
};

const defaultProps = {
    secureTextEntry: false,
    multiline: false,
    containerStyle: {},
};

const InputMask = props => {
    const {
        input: { value, onChange },
        meta: { touched, error },
        placeholder,
        type,
        mask,
        secureTextEntry,
        multiline,
        containerStyle,
        disabled,
        ...otherProps
    } = props;
    let error1 = touched && error ? true : false;

    return (
        <div style={{ width: '100%' }}>
            <InputLabel style={{fontSize: 12}} htmlFor="formatted-text-mask-input">{placeholder}</InputLabel>
            <InputMask1 disabled={disabled} mask={mask} value={value} onChange={onChange} style={{width: '100%'}}>
                {(inputProps) => <TextField {...inputProps} type="text" disabled={disabled} error={error1} helperText={error1 ? error : ""}/>}
            </InputMask1>
        </div>
    );
}

InputMask.defaultProps = defaultProps;
InputMask.propTypes = propTypes;

export { InputMask };
