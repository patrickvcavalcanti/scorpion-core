/* eslint-disable import/prefer-default-export, no-shadow */
import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

const propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  secureTextEntry: PropTypes.bool,
  multiline: PropTypes.bool,
  containerStyle: PropTypes.object,
};

const defaultProps = {
  secureTextEntry: false,
  multiline: false,
  containerStyle: {},
};

const InputMulti = (props) => {

  const {
    input: { value, onChange },
    meta: { touched, error },
    placeholder,
    type,
    secureTextEntry,
    multiline,
    containerStyle,
    rows,
    ...otherProps
  } = props;

  let error1 = touched && error ? true : false;

  return (
        <TextField 
        multiline
          error = {error1}
          rows={rows}
          label={placeholder}
          type={type ? type : "text"}
          value={value}
          onChange={event => onChange(event.target.value)}
          helperText={error1 ? error : ""}
          fullWidth
          style={{marginBottom: 15}}

          InputLabelProps={{
            shrink: true,
          }}
          {...otherProps}
        />
  );
};

InputMulti.defaultProps = defaultProps;
InputMulti.propTypes = propTypes;

export { InputMulti };
