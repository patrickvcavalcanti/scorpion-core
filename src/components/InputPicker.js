/* eslint-disable import/prefer-default-export, no-shadow */
import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';



import View from 'react-flexbox'

const propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,

  secureTextEntry: PropTypes.bool,
  multiline: PropTypes.bool,
  containerStyle: PropTypes.object,
};

const defaultProps = {
  secureTextEntry: false,
  multiline: false,
  containerStyle: {},
};

const InputPicker = (props) => {
  const { errorText } = styles;
  const {
    input: { value, onChange },
    meta: { touched, error },
    placeholder,
    itens,
    secureTextEntry,
    multiline,
    containerStyle,
    disabled,
    toStringItem,
    ...otherProps
  } = props;

  let error1 = touched && error ? true : false;
  let lista = [];
  if (itens) {
    lista = itens;
  }

  let serviceItems = lista.map((item, index) => {
    return <MenuItem key={index} value={index}>{toStringItem(item)}</MenuItem>
  });

  return (
    <View style={{ flexDirection: "column" }}>

      <View style={{ flexDirection: "row", }}>
        {placeholder && <InputLabel style={styles.labelForm}>{placeholder}</InputLabel>}
        {touched && error &&
          <View>
            <Typography style={errorText}>{error}</Typography>
          </View>}
      </View>


      <Select
        disabled={disabled}
        value={lista.map(function (e) { return toStringItem(e); }).indexOf(toStringItem(value))}
        renderValue={value => lista[value] ? toStringItem(lista[value]) : ""}
        onChange={event => {
          onChange(lista[event.target.value]);
          if (props.onSelected) props.onSelected(lista[event.target.value])
        }}
        fullWidth
        style={{ marginBottom: 15 }}
        {...otherProps}
      >
        {serviceItems}
      </Select>


    </View >
  );
};

const styles = {
  inputContainer: {

    // flex: 1,
    flexDirection: "column"
    // alignItems: 'center',
    // borderBottomWidth: 1,
    // borderColor: '#ddd',
    // backgroundColor: "#0f0"
  },
  InputText: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    // fontSize: 18,
    // lineHeight: 18,
    height: 44,
    flex: 2,

  },
  errorText: {
    textAlign: 'right',
    color: '#ff5964',
    fontSize: 12,
  },
  labelForm: {
    fontSize: 12 
  }
};

InputPicker.defaultProps = defaultProps;
InputPicker.propTypes = propTypes;

export { InputPicker };
