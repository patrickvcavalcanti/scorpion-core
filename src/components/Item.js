/* eslint-disable import/prefer-default-export */
import React from 'react';
import PropTypes from 'prop-types';
import View from 'react-flexbox'

const propTypes = {
  // children: PropTypes.node.isRequired,
  style: PropTypes.object,
};

const defaultProps = {
  style: {},
};

const Item = props => {
  let styleGeral = { ...styles.container, ...props.style };
  return (<View style={styleGeral}>
    {props.children}
  </View>)
}

const styles = {
  container: {
    paddingTop: 0,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
    // backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    
    // backgroundColor: "#f00"
    //position: 'relative',
  },
};

Item.defaultProps = defaultProps;
Item.propTypes = propTypes;

export { Item };
