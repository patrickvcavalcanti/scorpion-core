import React, { Component } from 'react'


import Typography from '@material-ui/core/Typography';
import ReactJson from 'react-json-view'

import View from 'react-flexbox'
export default class JsonEditor extends Component {
    render() {
        const {
            input: { value, onChange },
            meta: { touched, error },
            placeholder,

            disabled,
            ...otherProps
        } = this.props;
        let json = {}
        if (value) {
            json = value
        }
        return (<View column>
            <Typography>{placeholder}</Typography>
            <ReactJson src={json} theme="monokai"
                name={false}
                style={{ display: "flex", flex: "1", height: 100, overflow: 'auto' }}
                collapsed={2}
                displayDataTypes={false}
                enableClipboard={false}
                onEdit={v => onChange(v.updated_src)}
                onDelete={v => onChange(v.updated_src)}
                onAdd={v => onChange(v.updated_src)}
                {...otherProps}
            />
        </View>
        )
    }
}
