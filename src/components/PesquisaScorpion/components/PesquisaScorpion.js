import React from 'react';
import './style.css';

import DeleteIcon from "@material-ui/icons/Delete";
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Search from '@material-ui/icons/Search';
import Add from '@material-ui/icons/Add';
import Close from '@material-ui/icons/Close';
import Tune from '@material-ui/icons/Tune';
import TabelaOkds from '../../TabelaOkds';

import View from 'react-flexbox'
import { Typography } from '@material-ui/core';
class PesquisaScorpion extends React.Component {

    constructor(props) {
        super(props);
        this.filtrar = this.filtrar.bind(this);
        this.colAdd = this.colAdd.bind(this);
        this.state = {
            showFiltros: false,
            listaFiltrada: [],
            listaSelecionada: []
        };
    }

    changeFiltros() {
        this.setState({
            showFiltros: !this.state.showFiltros
        });
    }

    componentDidMount() {
        this.filtrar();
    }

    filtrar(props) {

        let lista_pesquisa = [];
        if (this.props.data) {
            lista_pesquisa = this.props.data.slice();
        }

        let listaFiltrada = lista_pesquisa.filter(obj => {
            let b = true;
            for (var f in props) {
                if (!b) {
                    return false;
                }
                if (props[f]) {
                    if (props[f].atende) {
                        b = props[f].atende(b, obj, props[f]);
                    } else {
                        // b = this.props[f + "Atende"](b, obj, props[f]);
                        if (this.props.children) {
                            this.props.children.forEach(child => {
                                let filtro = child.props;
                                if (filtro) {
                                    if (filtro.name === f) {


                                        if (filtro.atende) {
                                            b = filtro.atende(obj, props[f]);
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            };
            return b;
        });

        this.setState({
            showFiltros: false,
            listaFiltrada: listaFiltrada
        });

    }

    colAdd(linhas, getRenderItemCell, getRenderItemHeader) {

        return <View column style={{ flex: 0, padding: 1, justifyContent: 'flex-start' }}>
            {getRenderItemHeader({ label: "Sel" }, -25)}
            {/* {linhas.map((linha, indexL) => { return getRenderItemCell({ label: " ", getValue: () => <Button style={{ padding: 1, minWidth: 0 }} onClick={() => this.props.removeItem({ ...linha, index: indexL })}><DeleteIcon /></Button> }, linha, indexL) })} */}
            {linhas.map((obj) => {
                return getRenderItemCell({
                    label: " ", getValue: () => <Checkbox style={{ padding: 0, minWidth: 0 }} onClick={(value) => value.target.checked ?
                        this.state.listaSelecionada.push(obj) :
                        this.state.listaSelecionada.splice(this.state.listaSelecionada.indexOf(obj), 1)} />
                }, obj)
            })}
        </View>;

    }

    render() {

        const { titulo, columns, data, getID, porPagina, loadingPesquisa, actionclose, handleSubmit, novo, classes } = this.props;

        let listaExibir = this.state.listaFiltrada;

        // let alturaTabela = '100%';

        // if(this.props.multiSelecao){
        //     alturaTabela = '95%';
        // }

        // console.log("Por página ", Number(porPagina));
        let rowsPorPagina = Number(porPagina);
        // console.log("Por pagina ", porPagina);
        

        return (
            <div className={'containerPesquisa'}>
                <div className={classes.header}>
                    <Typography variant="h6" color="inherit" noWrap style={{ flexGrow: 1, }}>
                        {titulo}
                    </Typography>
                    {this.props.actionclose &&
                        <Button color="inherit" className="pesquisa-fechar-btn" onClick={actionclose}>
                            <Close onClick={actionclose} />
                        </Button>
                    } 
                </div >
                <div className={'bodyPesquisa'}>



                    <div className={this.state.showFiltros ? "filtrosPesquisaMobile" : "filtrosPesquisa"} >
                        <div className={this.state.showFiltros ? "filtrosAcaoMobile" : "filtrosAcao"}>
                            {novo &&
                                <Button style={{ margin: 2, minWidth: 0 }} variant="contained" color="primary" size="small"
                                    onClick={() => novo()}>
                                    <Add /> <div className={classes.hideMobile}>Novo</div>
                                </Button>
                            }
                            <Button style={{ margin: 2, minWidth: 0 }} variant="contained" color="primary" size="small" onClick={handleSubmit(this.filtrar)} >
                                <Search /> <div className={classes.hideMobile}>Filtrar</div>
                            </Button>
                            <Button variant="contained" color="primary" size="small"
                                className={classes.filtroBt}
                                //    style={this.state.showFiltros == true ? { display: 'none' } : {}}
                                onClick={() => this.changeFiltros()}>
                                <Tune />
                            </Button>
                        </div>
                        <div className={this.state.showFiltros ? "filtrosItensMobile" : "filtrosItens"}>
                            {this.props.children ? this.props.children : this.props.filtros}
                        </div>

                    </div>

                    <div className={'resultadoPesquisa'}>
                        <div className={'tabelaPesquisa'}>
                            <TabelaOkds colunas={columns} data={listaExibir} mensagemVazio="Nenhum insumo encontrado."
                                loading={loadingPesquisa} paginacao={rowsPorPagina} clickItem={!this.props.multiSelecao ? this.props.selecionar : null}
                                colAddAntes={this.props.multiSelecao ? this.colAdd : null}
                            />
                        </div>
                        {this.props.multiSelecao ?
                            <div className={'acoesPesquisa'}>
                                <Button style={{ height: 35 }} variant="contained" color="primary" size="small"
                                    onClick={() => this.props.selecionados(this.state.listaSelecionada)}>Continuar</Button>
                            </div> : ''}
                    </div>
                </div >
            </div>

            // <View column style={{ height: 'inherit', overflow: 'hidden', flex: 1, }}>
            //     <View className={classes.header} style={{ flexGrow: 0, padding: 5, paddingLeft: 16, paddingRight: 16, alignItems: 'center' }} >
            //         <Typography variant="h6" color="inherit" noWrap style={{ flexGrow: 1, }}>
            //             {titulo}
            //         </Typography>
            //         {this.props.actionclose &&
            //             <Button color="inherit" className="pesquisa-fechar-btn" onClick={actionclose}>
            //                 <Close onClick={actionclose} />
            //             </Button>
            //         }
            //     </View>
            //     <View style={{ flex: 1, flexGrow: 1 }}>
            //         <View className={classes.filtros} style={this.state.showFiltros ? { maxWidth: 250 } : {}}>
            //             <View style={{ flexGrow: 0, flexWrap: "wrap", justifyContent: "space-between", padding: 3 }} >
            //                 {novo &&
            //                     <Button variant="contained" color="primary" size="small"
            //                         style={{ margin: 2 }}
            //                         onClick={() => novo()}>
            //                         <Add /> <div className={classes.hideMobile}>Novo</div>
            //                     </Button>
            //                 }
            //                 <Button variant="contained" color="primary" size="small" onClick={handleSubmit(this.filtrar)} style={{ margin: 2 }}>
            //                     <Search /> <div className={classes.hideMobile}>Filtrar</div>
            //                 </Button>
            //                 <Button variant="contained" color="primary" size="small" className={classes.filtroBt}
            //                     style={this.state.showFiltros == true ? { display: 'none' } : {}}
            //                     onClick={() => this.changeFiltros()}>
            //                     <Tune />
            //                 </Button>
            //             </View>
            //             <View className={classes.hideFiltros} >
            //                 <div className={classes.hideMobile} style={this.state.showFiltros == true ? { display: 'block' } : {}}>
            //                     {this.props.children ? this.props.children : this.props.filtros}
            //                 </div>
            //             </View>

            //         </View>

            //         <View column style={{ backgroundColor: "#fafafa", justifyContent: 'flex-start', flex: 1, overflow: 'auto', height: '100%' }}>
            //             <View className={"tabelaPesquisa"} style={{height: alturaTabela}}>
            //                 <TabelaOkds colunas={columns} data={listaExibir} mensagemVazio="Nenhum insumo encontrado."
            //                     loading={loadingPesquisa} paginacao={rowsPorPagina} clickItem={!this.props.multiSelecao ? this.props.selecionar : null}
            //                     colAddAntes={this.props.multiSelecao ? this.colAdd : null}
            //                 />
            //             </View>
            //             <View>
            //                 {this.props.multiSelecao ?
            //                     <Button style={{ width: 100, margin: 2, marginLeft: '90%' }} variant="contained" color="primary" size="small"
            //                         onClick={() => this.props.selecionados(this.state.listaSelecionada)}>Continuar</Button> : null}
            //             </View>
            //         </View>
            //     </View>

            // </View >
        );
    }

}
const styles = theme => {

    return {
        header: {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.primary.contrastText,
            padding: 5,
            paddingLeft: 16,
            paddingRight: 16,
            alignItems: 'center',
            display: 'flex',
            // flexGrow: 0, padding: 5, paddingLeft: 16, paddingRight: 16, alignItems: 'center'


            height: 50,
        },
        filtros: {
            flexGrow: 0,
            flexDirection: "column",
            maxWidth: 250,
            [theme.breakpoints.down('sm')]: {
                maxWidth: 74,
            }
        },

        filtroBt: {
            display: 'none',
            margin: 2,
            minWidth: 0,
            [theme.breakpoints.down('xs')]: {
                display: 'flex',
            }
        },
        hideFiltros: {
            flexDirection: 'column',
            flexGrow: 1, padding: 3,
        },
        hideMobile: {

            [theme.breakpoints.down('xs')]: {
                display: 'none',
            }
        },
    }
};

PesquisaScorpion = withStyles(styles)(PesquisaScorpion);

export default PesquisaScorpion;