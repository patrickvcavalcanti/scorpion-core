import PesquisaScorpion from './PesquisaScorpion'
import enhance from './PesquisaScorpion.enhacer'

export default enhance(PesquisaScorpion)