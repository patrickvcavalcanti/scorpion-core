import React from 'react';
import './style.css';
import { Spinner } from '../../Spinner';
import Button from '@material-ui/core/Button';
import Search from '@material-ui/icons/Search';
import Add from '@material-ui/icons/Add';
import Close from '@material-ui/icons/Close';
import Tune from '@material-ui/icons/Tune';
import TabelaScorpion from './../../TabelaScorpion/index';

class PesquisaScorpion extends React.Component {

    constructor(props) {
        super(props);
        this.filtrar = this.filtrar.bind(this);
        this.state = {
            showFiltros: false,
            listaFiltrada: []
        };
    }

    changeFiltros() {
        this.setState({
            showFiltros: !this.state.showFiltros
        });
    }

    componentDidMount() {
        this.filtrar();
    }

    filtrar(props) {

        let lista_pesquisa = [];
        if (this.props.data) {
            lista_pesquisa = this.props.data.slice();
        }

        let listaFiltrada = lista_pesquisa.filter(obj => {
            let b = true;
            for (var f in props) {
                if (!b) {
                    return false;
                }
                if (props[f]) {
                    if (props[f].atende) {
                        b = props[f].atende(b, obj, props[f]);
                    } else {
                        // b = this.props[f + "Atende"](b, obj, props[f]);
                        if (this.props.children) {
                            this.props.children.forEach(child => {
                                let filtro = child.props;
                                if (filtro) {
                                    if (filtro.name === f) {


                                        if (filtro.atende) {
                                            b = filtro.atende(obj, props[f]);
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            };
            return b;
        });

        this.setState({
            showFiltros: false,
            listaFiltrada: listaFiltrada
        });

    }

    render() {

        const { titulo, columns, data, getID, porPagina, loadingPesquisa, actionclose, handleSubmit, novo } = this.props;

        let listaExibir = this.state.listaFiltrada;

        return (
            <div className="pesquisa-container">
                {loadingPesquisa ?
                    <Spinner />
                    :
                    <div className="pesquisa-container-sub">
                        <div className="pesquisa-header">
                            <div className="pesquisa-titulo">{titulo}</div>
                            <div className="pesquisa-fechar">
                                {this.props.actionclose ?
                                    <Button color="inherit" className="pesquisa-fechar-btn" onClick={actionclose}>
                                        <Close onClick={actionclose} />
                                    </Button>
                                    :
                                    <div />
                                }
                            </div>
                        </div>

                        <div className="pesquisa-filtros" style={this.state.showFiltros == true ? { maxWidth: 'none' } : {}}>
                            <div className="pesquisa-botoes">
                                {novo &&
                                    <Button variant="contained" color="primary" size="small" className="pesquisa-botao" onClick={() => novo()}>
                                        <Add /> <div className="responsivo-hidden">Novo</div>
                                    </Button>
                                }
                                <Button variant="contained" color="primary" size="small" className="pesquisa-botao" onClick={handleSubmit(this.filtrar)}>
                                    <Search /> <div className="responsivo-hidden">Filtrar</div>
                                </Button>
                                <Button variant="contained" color="primary" size="small" className="pesquisa-botao responsivo-show" onClick={() => this.changeFiltros()}>
                                    <Tune />
                                </Button>
                            </div>
                            <div className={"responsivo-hidden" + (this.state.showFiltros == true ? "show-filtros" : "")}>
                                {this.props.children ? this.props.children : this.props.filtros}
                            </div>
                        </div>

                        {/* <TabelaPower columns={columns} data={listaExibir} getID={getID} porPagina={porPagina} clickItem={this.props.selecionar} /> */}
                        <div className="pesquisa-container-tabela">
                            <TabelaScorpion colunas={columns} data={listaExibir} mensagemVazio="Nenhum insumo encontrado." loading={true} paginacao={true} clickItem={this.props.selecionar}></TabelaScorpion>
                        </div>
                    </div>
                }
            </div>
        );
    }

}

export default PesquisaScorpion;