/* eslint-disable import/prefer-default-export */
import React from 'react';
import PropTypes from 'prop-types';
import View from 'react-flexbox'
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from '@material-ui/core/styles/withStyles';
const propTypes = {
  size: PropTypes.string,
};

const defaultProps = {
  size: 'large',
};

class Spinner extends React.Component {

  constructor(props) {
    super(props);
  }
  render() {
    const { classes } = this.props;
    return <View style={styles.spinnerStyle}>     
    <CircularProgress className={classes.progress} />
    </View>
  }
}
const styles = {
  spinnerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

Spinner.propTypes = propTypes;
Spinner.defaultProps = defaultProps;
Spinner = withStyles(styles)(Spinner);
export { Spinner };
