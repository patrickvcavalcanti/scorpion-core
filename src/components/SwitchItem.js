import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';

import View from 'react-flexbox'
class SwitchItem extends Component {
    render() {
        const {
            input: { value, onChange },
            meta: { touched, error },
            placeholder,
            disabled
        } = this.props;
        let bool = false;
        if (value) {
            bool = value
        }
        return (<div>
            <View style={{ justifyContent: "flex-start", alignItems: "center", flex: '1' }}>
                <Switch checked={bool} onChange={(b) => onChange(b)} disabled={disabled} />
                <Typography >{placeholder}</Typography>
            </View>
            {touched && error &&

                <Typography style={{
                    color: '#ff5964',
                    fontSize: 12,
                }}>{error}</Typography>
            }
        </div>
        )
    }
}

const propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
};

SwitchItem.propTypes = propTypes;

export { SwitchItem }
