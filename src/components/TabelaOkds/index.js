import React, { Component } from 'react'

import View from 'react-flexbox'
import { Typography, TextField, Button } from '@material-ui/core'

import DeleteIcon from "@material-ui/icons/Delete";
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import withMobileDialog from '@material-ui/core/withMobileDialog';

import TablePagination from '@material-ui/core/TablePagination';
import { TabelaScorpion } from '..';
import produce from "immer"


function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

class Tabela extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listaFiltrada: null,
            page: 0,
            rowsPerPage: 15,
            ordem: 'asc',
            colunaOrdem: -1,
        };
        this.getRenderItemCell = this.getRenderItemCell.bind(this)
        this.getRenderItemHeader = this.getRenderItemHeader.bind(this)
    }
    componentDidMount() {
        const { paginacao } = this.props;
        if (paginacao) {
            this.setState({ rowsPerPage: paginacao });
        }
    }

    updateOrdem(coluna, ordem) {
        this.setState({
            colunaOrdem: coluna,
            ordem
        }, this.ordenar);
    }

    getRenderItemHeader(coluna, index) {
        const { ordem, colunaOrdem } = this.state;
        let novaOrdem = ordem == 'asc' ? 'desc' : 'asc';
        // console.log("Ordem ", novaOrdem);
        return <View key={"Header" + index} style={{
            color: "#929292",
            padding: 5,
            height: 35,
            maxHeight: 35,
            alignItems: 'center'


        }}
            onClick={() => this.updateOrdem(index, novaOrdem)}
        >
            {coluna.label}
            {colunaOrdem == index &&
                <span>
                    {ordem == 'asc' ?
                        <ArrowUpward style={{ fontSize: 16, marginBottom: -2, marginLeft: 4 }} />
                        :
                        <ArrowDownward style={{ fontSize: 16, marginBottom: -2, marginLeft: 4 }} />
                    }
                </span>
            }
        </View>
    }
    getRenderItemCell(coluna, linha, indexL) {
        let value = coluna.getValue(linha, indexL);
        let comp = value;

        if (typeof value === 'string' || typeof value === 'number') {
            comp = <Typography style={{ overflow: 'auto', height: this.props.heightLinha || 50 }} {...coluna.props}> {value}</Typography>
        }


        return <View key={"cell" + coluna.label + indexL}
            onClick={() => {
                if (this.props.clickItem) {
                    this.props.clickItem(linha)
                }

            }}

            style={{
                borderBottom: 1,
                borderBottomColor: '#e0e0e0',
                borderBottomStyle: 'solid',
                padding: 5,
                alignItems: 'center',
                height: this.props.heightLinha || 60,
                maxHeight: this.props.heightLinha || 60,

            }}>{comp}</View>
    }
    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    ordenar() {
        const { colunaOrdem, ordem } = this.state;
        const { data } = this.props;

        let newArray = data.slice();
        newArray = stableSort(newArray, this.getSorting(ordem, colunaOrdem));

        this.setState({
            listaFiltrada: newArray
        });
    }

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };
    getSorting(order, orderBy) {
        return order === 'desc' ? (a, b) => this.desc(a, b, orderBy) : (a, b) => -this.desc(a, b, orderBy);
    }
    desc(a, b, orderBy) {

        if (orderBy === -1) {
            return 0;
        }

        const { colunas } = this.props;
        let bValue = colunas[orderBy].getValue(b);
        let aValue = colunas[orderBy].getValue(a);

        if (typeof bValue === 'string') {
            bValue = bValue.toLowerCase();
        }

        if (typeof aValue === 'string') {
            aValue = aValue.toLowerCase();
        }
        if (!bValue && aValue) {
            return -1;
        }
        if (bValue && !aValue) {
            return 1;
        }

        if (!isNaN(bValue) && !isNaN(aValue)) {
            return parseFloat(bValue) - parseFloat(aValue)
        }

        if (bValue < aValue) {
            return -1;
        }
        if (bValue > aValue) {
            return 1;
        }


        return 0;
    }

    toUp(mover, index) {
        const data = this.props.data;

        const nextState = produce(data || [], draftState => {
            this.array_move(draftState, index, index - 1);
        })

        this.props.onChange(nextState);

    }
    array_move(arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    };

    toDown(mover, index) {
        const data = this.props.data;

        const nextState = produce(data || [], draftState => {
            this.array_move(draftState, index, index + 1);
        })

        this.props.onChange(nextState);
    }

    render() {
        let { rowsPerPage, page, ordem, colunaOrdem } = this.state;
        const { colunas, data, mensagemVazio, loading, paginacao, fullScreen, botaoRemoverAntes, habilitaOrdenacao, exibeCodigo, quebraMobile } = this.props;
        let mobile = fullScreen;

        if(quebraMobile === false){
            mobile = null;
        }

        let linhas = data ? data.slice() : [];

        linhas = stableSort(linhas, this.getSorting(ordem, colunaOrdem));

        if (paginacao) {
            linhas = linhas.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

        }

        // console.log('paginação ', paginacao);


        if (mobile) {
            return <TabelaScorpion flex='1 1 0px' {...this.props} /> 
        } else {
            return (
                <div style={{display: this.props.display ? this.props.display : 'flex' , flexFlow:'column', width: '100%'}}>
                {/* // <div style={{flexFlow:'column', width: '100%'}}> */}

                
                {/* // <View column
                //     style={{ height: '100%' }}
                // > */}
                    {/* <div style={{ display: 'flex', backgroundColor: "#f00" }}>


                        {colunas.map((coluna, index) => {
                            return <View column key={index} style={{ flex: 1, padding: 1, width: 1, backgroundColor: "#00f" }}>
                                {this.getRenderItemHeader(coluna)}
                            </View>
                        })}
                    </div> */}
                     <div style={{overflow: 'auto', display: 'flex', flex:"1 1 0"}}>
                     {/* <div style={{overflow: 'auto', display: 'flex'}}> */}
                    {/* <View style={{ overflow: 'auto', flex: '1 1 0' }}> */}
                        {this.props.colAddAntes && this.props.colAddAntes(linhas, this.getRenderItemCell, this.getRenderItemHeader)}
                        {(this.props.removeItem && botaoRemoverAntes && habilitaOrdenacao) &&
                            <View column style={{ flex: 0, padding: 1, justifyContent: 'flex-start' }}>
                                {this.getRenderItemHeader({ label: " " }, -25)}
                                {linhas.map((linha, indexL) => {
                                    return this.getRenderItemCell({
                                        label: " ", getValue: () =>
                                            <View>
                                                <Button className="buttonResponsivo" variant="fab" color="secondary" onClick={() => this.toUp(linha, indexL)}> <ArrowUpward /></Button>
                                                <Button className="buttonResponsivo" variant="fab" color="secondary" onClick={() => this.toDown(linha, indexL)}><ArrowDownward /></Button>
                                            </View>
                                    }, linha, indexL)
                                })}

                            </View>}
                        {(this.props.removeItem && botaoRemoverAntes) &&
                            <View column style={{ flex: 0, padding: 1, justifyContent: 'flex-start' }}>
                                {this.getRenderItemHeader({ label: " " }, -25)}
                                {linhas.map((linha, indexL) => {
                                    return this.getRenderItemCell({
                                        label: " ", getValue: () =>
                                            <Button style={{ padding: 1, minWidth: 0 }} onClick={() => this.props.removeItem({ ...linha, index: indexL })}><DeleteIcon /></Button>
                                    }, linha, indexL)
                                })}
                            </View>}
                        {exibeCodigo &&
                            <View column style={{ flex: 0, padding: 1, justifyContent: 'flex-start' }}>
                                {this.getRenderItemHeader({ label: "Índice" }, -25)}
                                {linhas.map((linha, indexL) => {
                                    return this.getRenderItemCell({
                                        label: " ", getValue: () =>
                                            <Typography style={{ overflow: 'auto', height: this.props.heightLinha || 50 }}> {indexL + 1}</Typography>
                                    }, linha, indexL)
                                })}
                            </View>
                        }
                        {colunas.map((coluna, index) => {
                            let flex = 1;
                            if (coluna.label === "Titulo" || coluna.label === "titulo") {
                                flex = 3
                            }
                            if (coluna.flex)
                                flex = coluna.flex
                            return <View column key={index} style={{ flex: flex, padding: 1, justifyContent: 'flex-start' }}>
                                {this.getRenderItemHeader(coluna, index)}
                                {linhas.map((linha, indexL) => { return this.getRenderItemCell(coluna, linha, indexL) })}
                            </View>
                        })}
                        {(this.props.removeItem && !botaoRemoverAntes && habilitaOrdenacao) &&
                            <View column style={{ flex: 0, padding: 1, justifyContent: 'flex-start' }}>
                                {this.getRenderItemHeader({ label: " " }, -25)}
                                {linhas.map((linha, indexL) => {
                                    return this.getRenderItemCell({
                                        label: " ", getValue: () =>
                                            <View>
                                                <Button className="buttonResponsivo" variant="fab" color="secondary" onClick={() => this.toUp(linha, indexL)}> <ArrowUpward /></Button>
                                                <Button className="buttonResponsivo" variant="fab" color="secondary" onClick={() => this.toDown(linha, indexL)}><ArrowDownward /></Button>
                                            </View>
                                    }, linha, indexL)
                                })}

                            </View>}
                        {(this.props.removeItem && !botaoRemoverAntes) &&
                            <View column style={{ flex: 0, padding: 1, justifyContent: 'flex-start' }}>
                                {this.getRenderItemHeader({ label: " " }, -25)}
                                {linhas.map((linha, indexL) => {
                                    return this.getRenderItemCell({
                                        label: " ", getValue: () =>
                                            <Button style={{ padding: 1, minWidth: 0 }} onClick={() => this.props.removeItem({ ...linha, index: indexL })}><DeleteIcon /></Button>
                                    }, linha, indexL)
                                })}

                            </View>}
                    {/* </View> */}
                    </div>
                    <div style={{height:54}}>

                    
                    {/* <View style={{ flexGrow: 0, flexShrink: 1, justifyContent: "flex-end" }}> */}
                        {linhas.length && paginacao ?
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 15, 25, 50, 100, 250, 500]}
                                component="div"
                                count={data.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                backIconButtonProps={{
                                    'aria-label': 'Previous Page',
                                }}
                                nextIconButtonProps={{
                                    'aria-label': 'Next Page',
                                }}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            />
                            :
                            ""
                        }
                    {/* </View> */}
                              
                    </div>
                </div>
                // {/* </View> */}
            )
        }
    }
}


Tabela = withMobileDialog({ breakpoint: 'xs' })(Tabela);



export default Tabela;