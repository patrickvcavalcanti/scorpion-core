import React, { Component } from 'react'

import View from 'react-flexbox';

import { Typography, Tooltip, ListItem, Divider, Button } from '@material-ui/core';
import DeleteIcon from "@material-ui/icons/Delete";

import { string } from 'prop-types';
var objectConstructor = {}.constructor;
class LinhaResponsiva extends Component {

    renderCelula(col, indexCol, linha, indexLinha, defaultWidth) {
        const { classes } = this.props;
        let cond = {};
        if (col.style) {
            cond = { ...cond, ...col.style }

        }
        if (col.style2valueCondicao) {
            cond = { ...cond, }
        }
        if (cond.width) {
            if (cond.width === "*") {
                cond = { ...cond, minWidth: col.style.minWidth || 100, maxWidth: 'unset' }
            } else {

                cond = { ...cond, minWidth: cond.width, maxWidth: cond.width }
            }
        }
        let ss = {}
        if (col.styleCondicional) {
            ss = col.styleCondicional(linha)
        }
        let valor = col.getValue(linha, indexLinha);
        let strValor = "";
        // if (typeof valor === "string") {
        //     strValor = valor;
        // }
        if (valor && valor.constructor !== objectConstructor) {
            strValor = valor;
        }
        // console.log(' {col.label}', col.label);

        // console.log('valor', valor);

        return <View key={`c${indexCol}-l${indexLinha}`} style={{ minWidth: defaultWidth, maxWidth: defaultWidth, justifyContent: 'flex-start', padding: 3, ...cond }} className={classes.celula} >
            <Tooltip className={classes.linhaheader} title={col.label} >
                <Typography variant={"subtitle1"} style={{ flex: 1, marginRight: 3, fontSize: 13, fontWeight: 'bold' }}>
                    {col.label}
                </Typography>
            </Tooltip>
            {strValor ?
                <Tooltip title={strValor}>
                    <Typography className={classes.linhatexto} align={col.align} style={{ flex: 2, ...ss }}>
                        {valor ? valor : ""}
                    </Typography>
                </Tooltip>
                : <View style={{ minWidth: defaultWidth, maxWidth: defaultWidth, flex: 2,justifyContent: 'flex-start', padding: 3, ...cond,...ss }}>
                    {valor}
                </View>
            }
        </View >
    }
    render() {
        const { colunas, linha, index, classes, clickItem } = this.props;
        let defaultWidth = "unset";
        colunas.forEach(col => {
            if (col.style && col.style.width) {
                defaultWidth = 100
            }
        });

        if (clickItem) {
            return (
                <ListItem className={classes.linha} dense style={{ paddingLeft: 5, paddingRight: 5 }} onClick={() => clickItem ? clickItem(linha) : ""} button>
                    {colunas.map((col, indexCol) => this.renderCelula(col, indexCol, linha, index, defaultWidth))}
                    {this.props.removeItem && <div style={{alignItems: 'center', justifyContent: 'center', display: 'flex'}}><Button onClick={() => this.props.removeItem({ ...linha, index: index })}><DeleteIcon/></Button></div>}
                </ListItem>
            )
        } else {
            return (
                <ListItem className={classes.linha} dense style={{ paddingLeft: 5, paddingRight: 5 }} >
                    {colunas.map((col, indexCol) => this.renderCelula(col, indexCol, linha, index, defaultWidth))}
                    {this.props.removeItem && <div style={{alignItems: 'center', justifyContent: 'center', display: 'flex'}}><Button onClick={() => this.props.removeItem({ ...linha, index: index })}><DeleteIcon/></Button></div>}
                </ListItem>
            )
        }

    }
}


export default LinhaResponsiva;