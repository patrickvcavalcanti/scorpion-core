import React, { Component } from 'react'

import View from 'react-flexbox';

import withStyles from '@material-ui/core/styles/withStyles';
import { Typography, Divider, ListItem } from '@material-ui/core';
import LinhaResponsiva from './LinhaResponsiva'

import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ArrowDownward from '@material-ui/icons/ArrowDownward';

class TabelaResponsiva extends Component {
    constructor(props) {
        super(props)
        this.renderCelula = this.renderCelula.bind(this)
    }

    renderCelula(col, indexCol,defaultWidth) {

        const { ordem, colunaOrdem, reordenar } = this.props;
        let cond = {};
        if (col.style) {
            cond = { ...cond, ...col.style }

        }
        if (cond.width) {
            if (cond.width === "*") {
                cond = { ...cond, minWidth: col.style.minWidth || 100, maxWidth: 'none' }
            } else {

                cond = { ...cond, minWidth: cond.width, maxWidth: cond.width }
            }
        }
     
       
        return <View key={`c${indexCol}-l-1`} column
            onClick={() => reordenar(indexCol)}
            style={{ cursor: 'pointer', minWidth: defaultWidth, maxWidth: defaultWidth, padding: 3, ...cond, }}
        >
            <Typography
                align={col.align}
                variant={"subtitle1"}
                noWrap
                style={{ ...cond,minHeight: 34 }}
            >
                {col.label}
                {colunaOrdem === indexCol && (ordem == 'asc' ?
                    <ArrowUpward style={{ fontSize: 16, marginBottom: -4, marginLeft: 4 }} />
                    :
                    <ArrowDownward style={{ fontSize: 16, marginBottom: -4, marginLeft: 4 }} />)
                }
            </Typography>
            <Divider />
        </View>

    }

    render() {
        const { colunas, lista, clickItem, classes, removeItem } = this.props;
        let linhasUsar = lista || []

        let defaultWidth = "unset";
        colunas.forEach(col => {
            if (col.style && col.style.width) {
                defaultWidth = 100
            }
        });
        return (
            <View column style={{flex: '1 1 auto', overflow: 'auto'}}>
                <ListItem className={classes.header} dense style={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 0, paddingTop: 0 }}>
                    {colunas.map((col, indexCol) => this.renderCelula(col, indexCol,defaultWidth))}
                </ListItem>
                <View column style={{ justifyContent: 'flex-start', }}> 
                    {linhasUsar.map((linha, index) => <LinhaResponsiva key={index} index={index} linha={linha} colunas={colunas} clickItem={clickItem} classes={classes} removeItem={removeItem} />)}
                </View>
            </View>
        )
    }
}


const styles = theme => {

    return {
        header: {
            [theme.breakpoints.down('sm')]: {
                display: 'none'
            }
        },
        linhaheader: {
            display: 'none',
            [theme.breakpoints.down('sm')]: {
                display: 'flex'
            }
        },
        linhatexto: {
            alignItems: "center",
            flexWrap: 'noWrap',
            [theme.breakpoints.down('sm')]: {
                flexWrap: 'wrap',
                display: 'flex',
            }
        },
        linha: {
            alignItems: 'center',
            [theme.breakpoints.down('sm')]: {
                display: 'block',
                flexWrap: 'wrap',
                border: '0.1em solid grey',
            }
        },
        celula: {

            [theme.breakpoints.down('sm')]: {
               
                width: 'unset  !important',
                minWidth: 'unset  !important',
                maxWidth: 'unset  !important',
            }
        }
    }
};

TabelaResponsiva = withStyles(styles)(TabelaResponsiva);

export default TabelaResponsiva;