import React from 'react';
import '../style.css';

class TabelaScorpionCell extends React.Component {
    state = {
        order: 'asc'
    };

    componentDidMount() {
        
    }

    render() {

        let { children, width, label, align } = this.props;

        label = label || "";
        children = children || "";
        width = width || "auto";
        align = align || "left";

        return (
            <td className="tb-scpn-cell" style={{ width, textAlign: align }}>
                <div className="tb-scpn-cell-label">{ label }</div>
                <div className="tb-scpn-cell-body">{ children }</div>
            </td>
        );
    }
}

export { TabelaScorpionCell };