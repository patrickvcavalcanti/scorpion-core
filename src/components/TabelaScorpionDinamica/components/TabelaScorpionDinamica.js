import React from 'react';
import '../style.css';
import { TabelaScorpionRow } from './TabelaScorpionRow';
import { TabelaScorpionCell } from './TabelaScorpionCell';

class TabelaScorpionDinamica extends React.Component {
    state = {
        order: 'asc'
    };

    componentDidMount() {

    }

    render() {

        const { children } = this.props;

        return (
            <table className="tb-scpn">
                <tbody>
                    { children || "" }
                </tbody>
            </table>
        );
    }
}

export { TabelaScorpionDinamica };