import React from 'react';
import '../style.css';

class TabelaScorpionRow extends React.Component {
    state = {
        order: 'asc'
    };

    componentDidMount() {
        
    }

    render() {

        const { children, header } = this.props;

        return (
            <tr className={"tb-scpn-row " + (header ? "tb-scpn-row-header" : "")} onClick={this.props.click}>
                { children || "" }
            </tr>
        );
    }
}

export { TabelaScorpionRow };