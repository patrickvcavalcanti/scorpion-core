import React from 'react';
import { TabelaScorpionDinamica } from '../../TabelaScorpionDinamica/components/TabelaScorpionDinamica';
import { TabelaScorpionRow } from '../../TabelaScorpionDinamica/components/TabelaScorpionRow';
import { TabelaScorpionCell } from '../../TabelaScorpionDinamica/components/TabelaScorpionCell';
import View from 'react-flexbox';

import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import InputLabel from '@material-ui/core/InputLabel';
import TablePagination from '@material-ui/core/TablePagination';

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

class TabelaScorpion extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listaFiltrada: null,
            ordem: 'asc',
            colunaOrdem: -1,
            page: 0,
            rowsPerPage: 10,
        };
        this.handleClick = this.handleClick.bind(this);
    }
    componentDidMount() {
        this.ordenar()
    }
    componentWillReceiveProps(props) {
        this.setState({
            listaFiltrada: props.data.slice()
        }, this.ordenar);
    }

    getHeader(cols) {
        let colunas = [];
        const { ordem, colunaOrdem } = this.state;
        let novaOrdem = null;

        cols.map((e, index) => {

            novaOrdem = ordem == 'asc' ? 'desc' : 'asc';

            colunas.push(
                <TabelaScorpionCell key={`h${index}`} align={e.align} width={e.width}>
                    <div onClick={() => this.updateOrdem(index, novaOrdem)} style={{ cursor: 'pointer' }}>
                        {e.label}
                        {colunaOrdem == index &&
                            <span>
                                {ordem == 'asc' ?
                                    <ArrowUpward style={{ fontSize: 16, marginBottom: -4, marginLeft: 4 }} />
                                    :
                                    <ArrowDownward style={{ fontSize: 16, marginBottom: -4, marginLeft: 4 }} />
                                }
                            </span>
                        }
                    </div>
                </TabelaScorpionCell>
            )
        });
        return <TabelaScorpionRow key="rh" header={true}>{colunas}</TabelaScorpionRow>
    }

    ordenar() {
        const { colunaOrdem, ordem } = this.state;
        const { data } = this.props;

        let newArray = data.slice();
        newArray = stableSort(newArray, this.getSorting(ordem, colunaOrdem));

        this.setState({
            listaFiltrada: newArray
        });
    }

    updateOrdem(coluna, ordem) {
        this.setState({
            colunaOrdem: coluna,
            ordem
        }, this.ordenar);
    }

    getSorting(order, orderBy) {
        return order === 'desc' ? (a, b) => this.desc(a, b, orderBy) : (a, b) => -this.desc(a, b, orderBy);
    }

    desc(a, b, orderBy) {

        if (orderBy === -1) {
            return 0;
        }

        const { colunas } = this.props;
        let bValue = colunas[orderBy].getValue(b);
        let aValue = colunas[orderBy].getValue(a);

        if (typeof bValue === 'string') {
            bValue = bValue.toLowerCase();
        }

        if (typeof aValue === 'string') {
            aValue = aValue.toLowerCase();
        }

        if (bValue < aValue) {
            return -1;
        }
        if (bValue > aValue) {
            return 1;
        }
        return 0;
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    handleClick = (item) => {
        if (this.props.clickItem) {
            this.props.clickItem(item);
        }
    }

    render() {

        let { colunas, mensagemVazio, paginacao } = this.props;
        let { rowsPerPage, page } = this.state;
        let listaUsar = this.state.listaFiltrada || [];

        if (paginacao) {
            listaUsar = listaUsar.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);
        }

        let heightDinamico = !listaUsar.length ? "auto" : "calc(100% - 110px)";

        return (
            <div style={{ display: 'inline-block', width: '100%', height: 'calc(100% - 110px)' }}>
                <div style={{ display: 'inline-block', width: '100%', height: 'calc(100% + 10px)', overflow: 'auto' }}>
                    <TabelaScorpionDinamica>
                        {colunas ? this.getHeader(colunas) : ""}
                        {
                            listaUsar.map((item, indexItem) => {
                                return (
                                    <TabelaScorpionRow key={`r${indexItem}`} click={() => this.handleClick(item)}>
                                        {colunas.map((col, index) => {
                                            return (
                                                <TabelaScorpionCell key={`c${index}`} label={col.label} align={col.align}>
                                                    {col.getValue(item,indexItem)}
                                                </TabelaScorpionCell>
                                            )
                                        })}
                                    </TabelaScorpionRow>
                                )
                            })
                        }
                    </TabelaScorpionDinamica>
                </div>
                {!listaUsar.length &&
                    <View style={{ flex: 1, justifyContent: 'center', margin: 10 }}>
                        <InputLabel style={{ fontSize: '0.8125rem' }}>
                            {mensagemVazio || "Nenhum resultado."}
                        </InputLabel>
                    </View>
                }

                {listaUsar.length && paginacao ?
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25, 50, 100, 250, 500]}
                        component="div"
                        count={this.state.listaFiltrada.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                            'aria-label': 'Previous Page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'Next Page',
                        }}
                        onChangePage={this.handleChangePage}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    />
                    :
                    ""
                }
            </div>
        );
    }

}

export default TabelaScorpion;