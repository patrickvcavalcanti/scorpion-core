const styles = theme => ({
  paper: {
    padding: 10,

    //  backgroundColor: "#f00"

  },
  caption: {
    fontSize: 16,
    textAlign: 'center'
    // , backgroundColor: "#ff0"
  },
  caixaBorda: {
    borderStyle: 'solid',
    backgroundColor: '#deedea',
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#16756f',
    padding: 5
  },
  treeNode: {
    height: 32,
    paddingLeft: 2,
    paddingRight: 2,
    // , backgroundColor: "#0f0"
  },
  treeIcon: {
    width: 16,
    height: 16,
    marginRight: 3
    // , backgroundColor: "#00f"
  },
  treeIconButton: {
    color: theme.palette.primary.light
    // , backgroundColor: "#f0f"
  },
  treeText: {
    flex: '0 0 auto',
    paddingLeft: 3,
    paddingRight: 10
    // , backgroundColor: "#f00"
  },
  treeTextFlex: {
    flex: 1
  },
  treeTextButton: {
    color: theme.palette.primary.light
  },
  button: {
    minWidth: 'auto',
    minHeight: 'auto',
    width: 'auto',
    height: 'auto',
    fontSize: 12,
    lineHeight: 1,
    padding: 4
    // , backgroundColor: "#0f0"
  }
});

export default styles;
