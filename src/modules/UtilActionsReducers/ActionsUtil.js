
import { TypeSucesso, TypeErro } from './index'

export function actionSyncListObj(typeBase, dispatch, retorno, nomeLista) {
    let onStart = undefined

    if (nomeLista) {
        onStart = () => dispatch({ type: typeBase, payload: { nomeLista, lista: [] } })
    } else {
        onStart = () => dispatch({ type: typeBase })
    }
    return {

        onStart: onStart,
        resolve: (lista => {
            if (nomeLista) {
                dispatch({ type: TypeSucesso(typeBase), payload: { nomeLista, lista } })
            } else {

                dispatch({ type: TypeSucesso(typeBase), payload: lista })
            }
            if (retorno) {
                retorno(lista)
            }

        }),
        reject: (erro => dispatch({ type: TypeErro(typeBase), payload: { nomeLista, error: erro } }))
    }
}

export function actionSincCRUD(promise, typeBase, dispatch, retorno, retornoError) {
    dispatch({ type: typeBase });
    promise
        .then(item => {
            dispatch({ type: TypeSucesso(typeBase), payload: item });
            if (retorno) {
                retorno(item);
            }
        })
        .catch(erro => {
            dispatch({ type: TypeErro(typeBase), payload: erro });
            if(retornoError){
                retornoError(erro);
            }
        });
}
