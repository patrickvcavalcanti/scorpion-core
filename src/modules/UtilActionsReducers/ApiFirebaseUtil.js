

var pathSincronizando = [];

//#region Salva, update,Delete
export function apiUpdateObj(docSalvar, item) {
    return new Promise((resolve, reject) => {

        item.uid = docSalvar.id;
        docSalvar.set(item, { merge: true })
            .then((docRef) => {
                resolve({ item, docSalvar });
            })
            .catch((error) => {
                console.log("salvarItemNaColection error", error);
                reject(error)
            });
    });
}
export function apiSalveInColection(colection, item) {
    return new Promise((resolve, reject) => {
        const { uid } = item;
        let docSalvar = null;
        if (uid) {
            docSalvar = colection.doc(uid);
        } else {
            docSalvar = colection.doc();
            item.uid = docSalvar.id;
        }

        docSalvar.set(item, { merge: true })
            .then((docRef) => {
                resolve({ item, docSalvar });
            })
            .catch((error) => {
                console.log("apiSalveInColection error", error);
                reject(error)
            });
    });
}

export function apiDeleteInColection(colection, item) {
    return new Promise((resolve, reject) => {
        const { uid } = item;
        let docSalvar = null;
        if (uid) {
            docSalvar = colection.doc(uid);
            docSalvar.delete().then(function () {
                resolve({ item, docSalvar });
            }).catch(function (error) {
                console.error("Error removing document: ", error);
                reject(error)
            });
        } else {
            reject("Item sem uid")
        }


    });
}

//#endregion

//#region Sincronisa Snapshot Com Redux

export function apiSyncReducer(instanceFirestore) {
    return function ({ onStart, resolve, reject }) {
        if (pathSincronizando.indexOf(instanceFirestore.path || JSON.stringify(instanceFirestore._query)) == -1) {
            pathSincronizando.push(instanceFirestore.path || JSON.stringify(instanceFirestore._query));
            onStart();
            instanceFirestore.onSnapshot(querySnapshot => {
                if (querySnapshot.docs) {//colections ou query
                    var newArray = []
                    querySnapshot.docs.map((doc) => {
                        newArray.push(doc.data());
                    });
                    resolve(newArray);
                } else {//document
                    if (!querySnapshot.exists) {
                        resolve(undefined);
                    } else {
                        resolve(querySnapshot.data());
                    }
                }
            }, err => {
                console.log(`Encountered error: ${err}`);
                reject(err)
            });
        }
    }

}


export function manterSincronizacaoQueryReducer(query) {
    return function ({ onStart, resolve, reject }) {
        if (pathSincronizando.indexOf(JSON.stringify(query._query)) == -1) {
            pathSincronizando.push(JSON.stringify(query._query));
            onStart();
            query.onSnapshot(querySnapshot => {
                var newArray = []
                querySnapshot.docs.map((doc) => {
                    newArray.push(doc.data());
                });
                resolve(newArray);
            }, err => {
                console.log(`Encountered error: ${err}`);
                reject(err)
            });
        }
    }
}
export function manterSincronizacaoColectionReducer(colection) {
    return function ({ onStart, resolve, reject }) {
        if (pathSincronizando.indexOf(colection.path) == -1) {
            pathSincronizando.push(colection.path);
            onStart();
            colection.onSnapshot(querySnapshot => {
                var newArray = []
                querySnapshot.docs.map((doc) => {
                    newArray.push(doc.data());
                });
                resolve(newArray);
            }, err => {
                console.log(`Encountered error: ${err}`);
                reject(err)
            });
        }
    }
}
export function manterSincronizacaoObjetoReducer(docItem) {
    return function ({ onStart, resolve, reject }) {
        if (pathSincronizando.indexOf(docItem.path) == -1) {
            pathSincronizando.push(docItem.path);
            onStart();
            docItem.onSnapshot(function (doc) {
                if (!doc.exists) {
                    resolve(undefined);
                } else {
                    resolve(doc.data());
                }
            }, err => {
                console.log(`Encountered error: ${err}`);
                reject(err)
            });
        }
    }
}

//#endregion