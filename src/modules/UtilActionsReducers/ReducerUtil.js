
import produce from "immer"
import { TypeSucesso, TypeErro } from './index'

export function reducerCrud(typeBase) {
    return function (state = {}, action) {
        const nextState = produce(state, draft => {
            switch (action.type) {
                case typeBase:
                    draft.crudloading = true
                    draft.crudError = ''
                    draft.crudMensagem = ''
                    break

                case TypeSucesso(typeBase):
                    draft.crudloading = false
                    draft.crudError = ''
                    draft.crudMensagem = 'Sucesso!'
                    break

                case TypeErro(typeBase):
                    draft.crudloading = false
                    draft.crudError = action.payload
                    draft.crudMensagem = ''
                    break
            }
        })
        return nextState


    }
}

export function reducerObj(typeBase) {
    return function (state = { loading: true, obj: null }, action) {

        const nextState = produce(state, draft => {
            switch (action.type) {
                case typeBase:
                    draft.loading = true
                    draft.error = ''
                    draft.obj = null
                    break

                case TypeSucesso(typeBase):
                    draft.loading = false
                    draft.error = ''
                    draft.obj = action.payload
                    break

                case TypeErro(typeBase):
                    draft.loading = false
                    draft.error = action.payload
                    break
            }
        })
        return nextState
    }
}

export function reducerList(typeBase, typeLazyLoad, usaNomeLista) {
    return reducerListMulti([typeBase], typeLazyLoad, usaNomeLista)
}
export function reducerListMulti(typesBases, typeLazyLoad, usaNomeLista) {
    return function (state = { loading: true, lista: [] }, action) {
        const nextState = produce(state, draft => {
            let controleListaAtualizar = draft;
            let listaRecebida = action.payload
            let erroRecebido = action.payload

            if (usaNomeLista) {
                if (action.payload) {
                    controleListaAtualizar = draft[action.payload.nomeLista];
                    listaRecebida = action.payload.lista
                    erroRecebido = action.payload.error;
                }
                if (!controleListaAtualizar) {
                    controleListaAtualizar = {};
                    if (action.payload && action.payload.nomeLista) {
                        draft[action.payload.nomeLista] = controleListaAtualizar;
                    }
                }
            }

            typesBases.forEach(typeBase => {
                switch (action.type) {
                    case typeBase:
                        controleListaAtualizar.loading = true;
                        controleListaAtualizar.error = "";
                        if (!controleListaAtualizar.lista || typesBases.length === 1) {
                            controleListaAtualizar.lista = [];
                        }
                        if (!controleListaAtualizar.controlaDelete) {
                            controleListaAtualizar.controlaDelete = {}
                        }
                        controleListaAtualizar.controlaDelete[typeBase] = []
                        break
                    case TypeSucesso(typeBase):
                        controleListaAtualizar.loading = false;
                        controleListaAtualizar.error = "";
                        mesclaListaUpdateAdd(controleListaAtualizar.lista, listaRecebida)

                        removeDeletados(controleListaAtualizar.controlaDelete[typeBase], listaRecebida.map(item => item.uid), controleListaAtualizar.lista)
                        controleListaAtualizar.controlaDelete[typeBase] = listaRecebida.map(item => item.uid)

                        break
                    case TypeErro(typeBase):
                        controleListaAtualizar.loading = false;
                        controleListaAtualizar.error = erroRecebido;
                        break
                }

            })
            if (typeLazyLoad) {
                switch (action.type) {
                    case typeLazyLoad:
                        controleListaAtualizar.lazyLoadItem_loading = true
                        controleListaAtualizar.lazyLoadItem_error = ""
                        break
                    case TypeSucesso(typeLazyLoad):
                        controleListaAtualizar.lazyLoadItem_loading = false
                        controleListaAtualizar.lazyLoadItem_error = ""
                        updateAddItemLista(listaRecebida, controleListaAtualizar.lista);
                        break
                    case TypeErro(typeLazyLoad):
                        controleListaAtualizar.lazyLoadItem_loading = false
                        controleListaAtualizar.lazyLoadItem_error = erroRecebido
                        break
                }
            }

        })
        return nextState
    }
}

//#region Util

export function removeDeletados(controlaDeleteOriginal, controlaDeleteNovo, lista) {
    controlaDeleteOriginal.map(item => {
        if (!controlaDeleteNovo.includes(item)) {
            let indexRemover = null;
            lista.forEach((it, index) => {
                if (it.uid === item) {
                    indexRemover = index;
                }
            })
            if (indexRemover !== null) {
                lista.splice(indexRemover, 1);
            }
        }
    })
}



export function mesclaListaUpdateAdd(listaOriginal, listaNovo) {
    listaNovo.forEach(itemAdd => {
        updateAddItemLista(itemAdd, listaOriginal)
    })
}

export function updateAddItemLista(item, lista) {
    let indexUpdade = null;
    lista.forEach((it, index) => {
        if (it.uid === item.uid) {
            indexUpdade = index;
        }
    })
    if (indexUpdade != null) {
        lista[indexUpdade] = { ...lista[indexUpdade], ...item }
    } else {
        lista.push(item)
    }

}

//#endregion
