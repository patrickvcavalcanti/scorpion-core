export * from './ReducerUtil';
export * from './ApiFirebaseUtil';
export * from './ActionsUtil';

export function TypeSucesso(type) {
    return "SUCESSO_" + type;
}
export function TypeErro(type) {
    return "ERRO_" + type;
}

export function tratarErroFiretore(erro) {
    return erro;
}

